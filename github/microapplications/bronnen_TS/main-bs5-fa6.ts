/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 02-10-20 13:19
 * Filename: main
 */
// require('./main-bs5-fa6.scss');
import './main-bs5-fa6.scss'
const $ = require('jquery');
import 'jquery-ui';
import 'bootstrap5';
import 'fontawesome6/js/all';
import 'filterizr';
import {WindowLoader} from "../generic/WindowLoader";
import {SpendtimeTracker} from "../generic/SpendtimeTracker";

const WL = new WindowLoader();
const SPT = new SpendtimeTracker(WL);

// This will extend the $.fn prototype with Filterizr
// Filterizr.installAsJQueryPlugin($);
