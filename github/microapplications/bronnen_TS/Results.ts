/*
 * FC-Sprint2 project => results.js
 *
 * The project "diglin.eu" is property of FC-Sprint2
 *
 * Created at:
 * 18-okt-2017 @ 9:02:15
 *
 * Created by:
 * Andries van Weeren
 * a.weeren@fcroc.nl
 *
 * @class new Results(param)
 * Bij het aanmaken van een nieuwe Resuls-instantie geef je een object mee met:
 * @param (object):
 * {
 *      title (string): [exercise title],
 *      application (string): [application name],
 *      hiddenAnswers (boolean): false
 * }
 *
 *
 * fn addRestult()
 * -------------------------------
 * Voor elke "vraag" die er is wordt de functie addResult gebruikt. Deze functie
 * heeft de (string) "vraag" als argument.
 *
 *
 * fn addEvent(param)
 * -------------------------------
 * Deze functie wordt aangeroepen voor elk event. Een event is ook het "starten"
 * en "afronden" van de oefening.
 * @param (object):
 * {
 *       event (string): 'click',
 *       action (string): 'playAUdio',
 *       answer (string): [url van de audio],
 *       info: {
 *           answerid (string): null,
 *           class (string): 'playWord',
 *           sound (string): null,
 *           word (string): [url van de audio]
 *       }
 *   }
 *
 *
 * fn addAnswer(param)
 * -------------------------------
 * @param (object):
 *
 * {
 * question (string): [bijbehorende vraag],
 * answer (string): [het gegeven antwoord],
 * correct (boolean|null): [true|false|null afhankelijk van of het antwoord correct, fout of timedout is],
 * timeout (boolean): [true|false afhankelijk van of er een timeout is]
 * }
 *
 * fn getResultsButton(target, additional, show)
 * -------------------------------
 * @target (string): [#id|.class]geeft het doel-element aan waarin de button geplaatst moet worden
 * @additional (boolean): [true|false] geeft aan of er aanvullende info in de resultaten moeten worden getoond
 * @show: nog niet in gebruik.
 *
 */

import bootbox from 'bootbox6';
import {parse} from "url";

export class Results {

    id: string;
    title: string;
    application: string;
    hiddenAnswers: boolean;
    exerciseStarted: boolean;
    exerciseFinished: boolean;
    time: number;
    timeInitiation: {
        'date': any,
        'time': any,
        'timestring': number|string
    };
    completed: boolean;
    timerLoaded: number = 0;
    lastTime;
    lastTimeCorrect;

    results: any[];
    correctAnswers: any[];
    timedAnswers: number = 5;
    events: any[];
    isMobile: boolean = false;

    constructor(param) {

        let self = this;
        let currentDate = new Date();
        let rDate = currentDate.toLocaleDateString();
        let rTime = currentDate.toLocaleTimeString();
        let rTimeString = currentDate.getTime();

        this.isMobile = false; //initiate as true
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) { 
            this.isMobile = true;
        }

        this.id = 'new';
        this.title = param.title || 'Unknown title';
        this.application = param.application || 'Unknown application';
        this.hiddenAnswers = param.hiddenAnswers || false;
        this.exerciseStarted = false;
        this.exerciseFinished = false;

        this.time = 0;
        this.timeInitiation = {
            'date': rDate,
            'time': rTime,
            'timestring': rTimeString
        };
        this.timerLoaded = 0;
        this.lastTime;
        this.lastTimeCorrect;

        setInterval(function () {
            self.timerLoaded++;
        }, 1000);

        this.results = [];
        this.correctAnswers = [];
        this.timedAnswers = 5;
        this.events = [];

        this.addEvent({
            'event': 'pageLoad',
            'action': 'pageLoad',
            'loaded': self.timeInitiation
        });
        this.completed = false;
    }

    setTimedAnswersAmount(amount) {
        this.timedAnswers = amount;
    }

    getTimedAnswersAmount() {
        return this.timedAnswers;
    }

    addCorrectAnswer(answer) {
        this.correctAnswers.push(answer);
    }

    getCorrectAnswers(): any {
        this.correctAnswers.sort(function (a, b) {
            if (a.correctanswertime < b.correctanswertime)
                return -1;
            if (a.correctanswertime > b.correctanswertime)
                return 1;
            return 0;
        });

        return this.correctAnswers.reverse();
    }

    getLastTime() {

        if(this.lastTime === undefined) {
            this.setAnswerTime();
        }

        return this.lastTime;
    }

    getLastTimeCorrect() {
        return this.lastTimeCorrect;
    }

    getExerciseStarted() {
        return this.exerciseStarted;
    }

    setExerciseStarted(param) {
        this.exerciseStarted = param;
    }

    getExerciseFinished() {
        return this.exerciseFinished;
    }

    setExerciseFinished(param) {
        this.exerciseFinished = param;
    }

    getDuration() {
        return '+' + this.timerLoaded;
    }

    setTime(time) {
        this.time = time;
    }

    addResult(param, paramOb = {'hidden': false}) {

        let resultObject = {
            'question': param,
            'answers': [],
            'timeout': false,
            'hidden': paramOb.hidden
        };
        this.results.push(resultObject);
    }

    setAnswerTime() {
        let currentDate = new Date();
        this.lastTime = currentDate.getTime();
    }

    getAnswerTime() {
        let currentDate = new Date();

        return currentDate.getTime() - this.getLastTime();
    }

    setCorrectAnswerTime() {
        let currentDate = new Date();
        this.lastTimeCorrect = currentDate.getTime();
    }

    getCorrectAnswerTime() {

        let currentDate = new Date();

        if (this.lastTimeCorrect === undefined || this.lastTimeCorrect === null) {

            return currentDate.getTime() - this.getLastTime();
        }

        return currentDate.getTime() - this.getLastTimeCorrect();
    }

    /**
     *
     * @param {object} param
     * @returns {undefined}
     */
    addAnswer(param)
    {
        let self = this;
        let i = 0;
        let currentDate = new Date();
        let answerObject = {
            'question': param.question,
            'answer': param.answer,
            'correct': param.correct,
            'time': self.getDuration(),
            'timestring': currentDate.getTime(),
            'timeout': param.timeout || false,
            'correctanswertime': ((param.correct) ? self.getCorrectAnswerTime() : null),
            'answertime': self.getAnswerTime(),
            'groupBy': ((param.groupBy !== undefined) ? param.groupBy : '')
        };

        //Bereken de tijd tussen de start en een correct antwoord of
        //Bereken de tijd tussen twee correcte antwoorden
        if (param.correct) {
            self.addCorrectAnswer(answerObject);
            self.setCorrectAnswerTime();
        }

        //Bereken de tijd tussen de antwoord-pogingen ongeacht correct of fout
        self.setAnswerTime();

        if (param.id !== undefined && param.id !== null && param.id !== '') {
            i = param.id;
        }
        else {
            var found = -1;

            while (i < self.results.length && found === -1) {

                if (self.results[i].question === param.question) {
                    found = i;
                }
                else {
                    i++;
                }
            }
        }

        this.results[i]['hidden'] = (param.hidden !== undefined) ? param.hidden : false;

        this.results[i].answers.push(answerObject);
    }

    getResults() {
        let self = this;
        return {
            appliaction: self.application,
            title: self.title,
            time: self.time,
            results: self.results,
            events: self.events
        };
    }

    addEvent(param) {

        if (param.event === 'start') {

            let currentDate = new Date();
            this.lastTime = currentDate.getTime();

            this.setExerciseStarted(true);

            let i = 0;
            let occured = false;

            while (i < this.events.length && !occured) {
                if (this.events[i].event === 'start') {
                    occured = true;
                }
                i++;
            }

            if (!occured) {
                param['time'] = this.getDuration();
                this.events.push(param);
            }
        }
        else if (param.event === 'completed') {
            this.setExerciseFinished(true);
            let i = 0;
            let occured = false;
            while (i < this.events.length && !occured) {
                if (this.events[i].event === 'completed') {
                    occured = true;
                }
                i++;
            }
            if (!occured) {
                this.completed = true;
                param['time'] = this.getDuration();
                this.events.push(param);
            }
        }
        else {
            param['time'] = this.getDuration();
            this.events.push(param);
        }
    }

    getResultsButton(target, additional = false, show = []) {

        if ((this.completed || this.hiddenAnswers) && $('a#resultbutton').length === 0) {

            let self = this;

            let btn = $('<button/>', {
                'id': 'resultbutton',
                'class': 'btn btn-outline-dark',
                style:'font-size:' + (self.isMobile ? 1.05:1.05) + 'em !important;padding:' + (self.isMobile ? '.36rem 0.8rem':'') + '!important'
            }).on('click', function () {

                self.displayResults(additional);

                //show given id's
                if (show.length > 0) {

                }
            }).html('<i class="fas fa-check fa-fw text-success"></i> | <i class="fas fa-times fa-fw text-danger"></i>');

            $(target).append(btn);
        }
    }

    displayResults(additional) {
        let self = this;
        if($('#bb-style').length === 0) {
            $('head').append('<style>.modal-body {max-height: 80vh !important; overflow-y: auto;}</style>')
        }
        let bb = bootbox.alert({
            title: '<h4><i class="fas fa-check fa-fw text-success"></i> <span style="font-size: 1.0em;">|</span> <i class="fas fa-times fa-fw text-danger"></i></h4>',
            message: self.getModalBodyContent(additional),
            buttons: {
                ok: {
                    label: '<i class="fa-solid fa-fw fa-check"></i>',
                    className: 'btn-outline-primary'
                }
            }
        });
    }

    getIconDiv(faIcon) {
        let iconRow = $('<div/>', {
            'class': 'row'
        }).append($('<div/>', {
            'class': 'col-md-12 text-center'
        }).html(faIcon));

        return iconRow;
    }

    getPointerDiv(contents) {
        let addRow = $('<div/>', {
            'class': 'row'
        }).append($('<div/>', {
            'class': 'col-md-12'
        }).append(contents));

        return addRow;
    }

    getModalBodyContent(additional) {
        let self = this;

        let row = $('<div/>', {
            'class': 'row'
        });

        let col = $('<div/>', {
            'class': 'col-md-12'
        });

        if (additional) {

            let contents = self.getAdditionSoundsInfo();

            if (contents) {
                $(col).append(self.getIconDiv('<i class="far fa-hand-pointer"></i>'), self.getPointerDiv(contents));
            }
        }

        let list = $('<ul/>', {
            'class': 'list-group'
        });

        $.map(self.results, function (res, i) {

            let li = $('<li/>', {
                'class': 'list-group-item',
                'data-id': i
            });

            let strong = $('<strong/>').html(res.question + ': ');

            if (self.hiddenAnswers && res.hidden !== undefined && res.hidden) {
                $(strong).hide();
            }

            $(li).append(strong);

            if (additional) {
                $(li).append(self.getAdditionalInfo(i));
            }

            let groupBy = false;

            $.map(res.answers, function (ans, j: number) {
                $(li).append(ans.answer + ' ' + ((ans.correct) ? '<i class="fas fa-check text-success"></i>' : '<i class="fas fa-times text-danger"></i>'));

                if (j < (res.answers.length - 1)) {
                    $(li).append(' | ');
                }

                if (ans.groupBy !== '') {
                    groupBy = ans.groupBy
                }
            });

            if (res.timeout) {
                $(li).append(' | <i class="far fa-clock text-danger"></i>');
            }

            if (groupBy && self.completed) {

                let sp = $('<span/>', {
                    class: 'badge'
                }).html(groupBy);

                $(li).append(sp);
            }

            $(list).append(li);
        });

        $(col).append(self.getIconDiv('<i class="fas fa-check text-success"></i> | <i class="fas fa-times text-danger"></i>'), self.getPointerDiv(list));


        let slowAnsers = self.getHtmlTimedAnswers();
        $(col).append(self.getIconDiv('<i class="far fa-clock"></i>'), self.getPointerDiv(slowAnsers));

        $(row).append(col);

        return row;
    }

    getHtmlTimedAnswers() {
        let self = this;

        let list = $('<ul/>', {
            'class': 'list-group'
        });

        let answers = self.getCorrectAnswers();

        if (answers.length > 0) {
            let i = 0;

            while (i < answers.length && i < self.getTimedAnswersAmount()) {
                let answer = answers[i];

                let li = $('<li/>', {
                    'class': 'list-group-item',
                    'data-id': i
                });

                let questionCol = $('<div/>', {
                    'class': 'col-sm-6 col-md-4'
                }).append($('<button/>', {
                    'class': 'btn btn-primary btn-sm'
                }).html(answer.question));

                let answerCol = $('<div/>', {
                    'class': 'col-sm-6 col-md-8'
                }).append($('<span/>', {
                    'class': ''
                }).html(((answer.answer === answer.question) ? '' : answer.answer + ' ') + ' <i class="fas fa-check text-success"></i> <span class="">' + (parseFloat(answer.correctanswertime)/ 1000).toFixed(2) + 's</span>'));


                $(li).append($('<div/>', {
                    'class': 'row'
                }).append(questionCol, answerCol));

                $(list).append(li);

                i++;
            }
        }

        return list;
    }

    getAdditionalInfo(id) {

        id = parseInt(id);

        let self = this;
        let eventResult = [];

        $.map(self.events, function (event, i) {

            if (event.info !== undefined && event.info.answerid !== undefined && parseInt(event.info.answerid) === id && event.action === 'playAudio') {
                eventResult.push(id);
            }
        });

        if (eventResult.length > 0) {
            return eventResult.length + 'x <i class="fas fa-volume-up"></i> | ';
        }

        return '';
    }

    getAdditionSoundsInfo() {

        let self = this;
        let list = $('<ul/>', {
            'class': 'list-group'
        });

        let amounts: any = {};
        let imageAmounts: any = {};

        $.map(self.events, function (event, i) {

            if (event.info !== undefined && event.info.class !== undefined && event.info.class === 'playWord') {
                if (event.info.word !== undefined && event.info.word !== '') {

                    if (amounts[event.info.word] === undefined) {
                        amounts[event.info.word] = {
                            'audio': 0,
                            'image': 0,
                            'id': (event.info.answerid !== undefined) ? event.info.answerid : null
                        };
                    }
                    amounts[event.info.word].audio = amounts[event.info.word].audio + 1;

                }
            }
            if (event.info !== undefined && event.info.class !== undefined && event.info.class === 'showWord') {
                if (event.info.word !== undefined && event.info.word !== '') {

                    if (amounts[event.info.word] === undefined) {
                        amounts[event.info.word] = {
                            'audio': 0,
                            'image': 0,
                            'id': (event.info.answerid !== undefined) ? event.info.answerid : null
                        };
                    }
                    amounts[event.info.word].image = amounts[event.info.word].image + 1;

                }
            }
        });

        $.map(amounts, function (amount, key) {
            if (amount.audio !== 0 || amount.image !== 0) {
                let html = '';

                let li = $('<li/>', {
                    'class': 'list-group-item',
                    'data-id': amount.id
                });

                let strong = $('<strong/>').html(key.toString() + ': ');
                if (self.hiddenAnswers && self.results[amount.id].hidden !== undefined && self.results[amount.id].hidden) {
                    $(strong).hide();
                }
                //html += '<strong>' + key + ':</strong> ';

                html += (amount.audio !== 0) ? amount.audio + 'x <i class="fas fa-volume-up"></i>' : '';
                html += (amount.audio !== 0 && amount.image !== 0) ? ' | ' : '';
                html += (amount.image !== 0) ? amount.image + 'x <i class="fas fa-camera"></i>' : '';

                $(list).append($(li).append(strong, html));
            }
        });

        if ($(list).children().length > 0) {
            return list;
        }

        return false;
    }

    getTimedResults() {

    }
}