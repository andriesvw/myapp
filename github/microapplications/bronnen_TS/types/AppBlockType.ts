export type AppBlockType = {
    content: number|string,
    audio?: string,
    image?: string
};