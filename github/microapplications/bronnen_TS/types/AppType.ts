export type AppType = {
    uuid?: string,
    id: number|string,
    exercisetitle: string,
    language: string,
    application: string,
    applicationname?: string,
    main_object: any
    createdAt?: string,
    updatedAt?: string,
};