export type MediaType = {
    audio: {
        path: string,
        files: string[],
        lowercase: string[]
    },
    image: {
        path: string,
        files: string[],
        lowercase: string[]
    },
    soundbar: {
        path: string,
        files: string[],
        lowercase: string[]
    },
    language: string,
    domain: string,
    amounts: {
        audio: number,
        img: number,
        soundbar: number,
        total: number
    }
};