/**
 * !!!Keep in mind, the structure of the dataset might be quite different then the data-structure required the exercise!!!
 * @param {string} language required parameter for the available datasets
 * @param {string} appliaction required for setting the correct application
 * @param {jQuery} [targetArea] optional clears the targetEarea
 * @param {array} [targetArray] optional can also be handled in the onLoaded-parameter
 * @param {function} [onBefore] optional executed just before fetching the selected dataset
 * @param {function} onLoaded required executed right after fetching the selected dataset
 */
type optionsType = {
    language: string,
    application: string,
    targetArea: any,
    targetArray: any[],
    onBefore: any,
    onLoaded: any|null
};

export class Datasets {

    private _options: optionsType;
    private _inputEl: JQuery<HTMLElement>;
    private _datasets: any[] = [];
    private _dataset: any = {};

    /**
     *
     * @param {optionsType} options
     */
    constructor(options: optionsType) {

        options = this.checkOptions(options);

        this.options = options || {
            language: 'nl_NL',
            targetArea: {},
            application: '',
            targetArray: [],
            onBefore: (response): void =>{ },
            onLoaded: (response): void => { }
        };


        this.init();
    }

    get options(): any {
        return this._options;
    }

    set options(value: any) {
        this._options = value;
    }

    get inputEl(): JQuery {
        return this._inputEl;
    }

    set inputEl(value: JQuery) {
        this._inputEl = value;
    }

    get datasets(): any[] {
        return this._datasets;
    }

    set datasets(value: any[]) {
        this._datasets = value;
    }

    get dataset(): any {
        return this._dataset;
    }

    set dataset(value: any) {
        this._dataset = value;
    }

    /**
     * Initiate the class
     */
    init() {

        let s = this;
        s.setInput();
        s.fetchDatasets();
    }

    checkOptions(options) {

        let s = this;

        if(options['application'] === undefined && options['applicationname'] !== undefined) {

            options['application'] = options['applicationname'];
        }
        else if(options['application'] === undefined && options['applicationname'] === undefined)
        {
            options['application'] = s.setApplication();
        }

        let requiredParams = [
            'language',
            'application',
            'onLoaded'
        ];

        requiredParams.map(function(param, i) {

            if(options[param] === undefined) {

                console.error(param + ' is required')
            }
        });

        if(options['onBefore'] === undefined) {

            options['onBefore'] = function(){ };
        }

        return options;
    }

    setApplication(): string {

        let URLarray: string[] = window.location.href.toString().split(/\//);
        let appId: number = URLarray.indexOf('applications');

        if (appId === -1)
        {
            appId = URLarray.indexOf('application');
        }

        return URLarray[(appId + 1)];
    }

    /**
     *
     * @param {object} response
     * @returns {object}
     */
    setResponse(response) {

        let s = this;

        response.application = s.getOption('application');
        response['datasetid'] = response.id;

        return response;
    }

    /**
     *
     * @param {string} key
     * @param {mixed} value
     */
    setOption(key, value) {
        let s = this;


        if(key === 'language') {
            s.setLanguage(value);
            s.fetchDatasets();
        }
        else {
            s.options[key] = value;
        }
    }

    /**
     *
     * @param {string} key
     * @returns {*}
     */
    getOption(key) {

        let s = this;
        return s.options[key];
    }

    /**
     *
     * @param {string} language
     */
    setLanguage(language) {
        let s = this;

        s.options.language = language;

        $(s.inputEl).addClass('disabled');
        s.fetchDatasets();
    }

    /**
     *
     * @param {Object} dataset
     */
    setDataset(dataset) {

        this.dataset = dataset;
    }

    /**
     *
     * @returns {Object}
     */
    getDataset() {

        return this.dataset;
    }

    /**
     *
     * @param {array} datasets
     */
    setDatasets(datasets) {

        this.datasets = datasets;
    }

    /**
     *
     * @returns {array}
     */
    getDatasets() {

        return this.datasets;
    }

    /**
     * Returns additional styling for the autocomplete
     * @returns {jQuery}
     */
    getUiStyle(){

        return $('<style/>', {
            id: 'autocmpltstyle'
        }).text('  .ui-autocomplete {\n' +
            '    max-height: 600px;\n' +
            '    overflow-y: auto;\n' +
            '    /* prevent horizontal scrollbar */\n' +
            '    overflow-x: hidden;\n' +
            '  }\n' +
            '  /* IE 6 doesn\'t support max-height\n' +
            '   * we use height instead, but this forces the menu to always be this tall\n' +
            '   */\n' +
            '  * html .ui-autocomplete {\n' +
            '    height: 600px;\n' +
            '  }');
    }

    /**
     * Set the input field for the dataset
     */
    setInput(){

        let s = this;

        if($('#autocmpltstyle').length === 0) {

            $('head').append(s.getUiStyle());
        }


        s.inputEl = $('<input/>', {
            class: 'form-control',
            placeholder: 'Select Dataset'
        });
    }

    /**
     * Sets the autocomplete functionality to input-field
     * Destroys the autocomplete if it exists
     */
    setAutoComplete() {

        let s = this;

        if(s.inputEl.hasClass('ui-autocomplete-input')) {

            s.inputEl.autocomplete('destroy');
        }

        $(s.inputEl).autocomplete({
            source: function (request, response) {
                let filteredArray = $.map(s.getDatasets(), function (item) {

                    if (item.value.toLowerCase().trim().startsWith(request.term.toLowerCase()) || item.value.toLowerCase().startsWith(request.term.toLowerCase())) {

                        return item;
                    }
                    else {
                        return null;
                    }
                });
                let results = $.ui.autocomplete.filter(filteredArray, request.term);
                response(results.slice(0, 100));
            },
            select: function (e, ui) {

                s.fetchDataset(ui.item.id)

            },
            autoFocus:true,
            minLength: 0
        });
    }

    /**
     * Gets the input-field
     * @returns {void|jQuery|HTMLElement}
     */
    getInput(){

        let s = this;

        return s.inputEl;
    }

    /**
     * Fetches the available datasets for the current @options.language value
     */
    fetchDatasets() {

        let s = this;

        $(s.inputEl).addClass('disabled').attr({
            disabled: 'disabled'
        });

        $.ajax({
            url: '/api/datasets/get/' + s.getOption('language'),
            method: 'get',
            dataType: 'json'
        }).done(function(response){
            s.setDatasets(response);

            s.setAutoComplete();
            $(s.inputEl).removeClass('disabled').removeAttr('disabled');

        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    }

    /**
     * Fetches the dataset based on the given id
     * @param {int} id
     */
    fetchDataset(id) {

        let s = this;

        s.getOption('onBefore')();

        $.ajax({
            url: '/api/dataset/get/' + id.toString(),
            method: 'get',
            dataType: 'json'
        }).done(function(response){

            response = s.setResponse(response);

            s.setDataset(response);
            s.getOption('onLoaded')(s.getDataset());

        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        })

    }
}