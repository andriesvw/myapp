/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 28/10/2022 10:36
 * Filename: BsNavTab
 */

import {BsFa6 as Bs} from "./BsFa6";
import {v4 as uuidv4} from 'uuid';

/**
 * @class BsNavTab
 */
export class BsAccordion {

    private _container: string;
    private _classes: string;
    private _item: JQuery;
    private _header: JQuery;
    private _accordion: JQuery;
    private _index: number = 0;

    /**
     * @param {string} container
     * @param {string} classes
     */
    public constructor(container: string, classes: string = 'border border-primary') {

        this.container = container;
        this.classes = classes;
        this.init();
    }

    private init(){

        let s = this;
        let uuid = s.uuid();
        s.item = Bs.div(`accordion-item mt-2 rounded ${s.classes}`);
        s.header = Bs.div('accordion-header rounded bg-danger', {
            id: `heading${uuid}`
        }).css({
            '--bs-bg-opacity': '0.5',
            '--bs-accordion-bg': 'auto'
        });

        let btn = Bs.jQEl('button', 'accordion-button rounded p-1', {
            'type': 'button',
            'data-bs-toggle': 'collapse',
            'data-bs-target': `#acc${uuid}`,
            'aria-expanded': 'false',
            'aria-controls': `acc${uuid}`
        });
        s._header.append(btn);

        s.accordion = Bs.div('accordion-collapse collapse p-2', {
            id: `acc${uuid}`,
            'aria-labelledby': `heading${uuid}`,
            'data-bs-parent': `#${s.container}`
        });

        s._item.append(s.header, s.accordion);
    }


    /**
     * @return {string}
     * @protected
     */
    protected get container(): string {
        return this._container;
    }

    /**
     * @param {string} value
     * @protected
     */
    protected set container(value: string) {
        this._container = value;
    }

    /**
     * @return {JQuery}
     */
    public get item(): JQuery {
        return this._item;
    }

    /**
     * @param {JQuery} item
     */
    public set item(item: JQuery) {
        this._item = item;
    }

    /**
     * @return {JQuery}
     */
    public get header(): JQuery {
        return this._header;
    }

    /**
     * @param {JQuery} value
     */
    public set header(value: JQuery) {
        this._header = value;
    }

    /**
     * @return {JQuery}
     */
    public get accordion(): JQuery {
        return this._accordion;
    }

    /**
     * @param {JQuery} value
     */
    public set accordion(value: JQuery) {
        this._accordion = value;
    }

    /**
     * @return {number}
     * @protected
     */
    protected get index(): number {
        return this._index;
    }

    /**
     * @param {number} value
     * @protected
     */
    protected set index(value: number) {
        this._index = value;
    }


    /**
     * @return {string}
     * @protected
     */
    protected get classes(){
        return this._classes;
    }

    /**
     * @param {string} classes
     * @protected
     */
    protected set classes(classes: string) {
        this._classes = classes;
    }

    /**
     * @return {string}
     * @protected
     */
    protected uuid(): string {
        return uuidv4().replace(/-/g, '');
    }

    /**
     * @param {number | string} time
     */
    public addTime(time: number|string) {

        let s = this;
        let col = Bs.col('col-2 text-right').append(Bs.far('fa-clock fa-sm mr-1'), time.toString());
        s._header.find('button').append(col);
    }

    public prependLink(link: JQuery): void {

        let s = this;
        s._header.find('button').prepend(link);
    }

    /**
     * @param {JQuery | string} title
     */
    public setHeader(title: JQuery|string) {

        let s = this;
        let col = Bs.col('col-9').append(title);
        s._header.find('button').append(col);
    }

    /**
     * @param {JQuery | string} item
     */
    public addBody(item: JQuery|string) {

        let s = this;
        s._accordion.append(item)
    }

}