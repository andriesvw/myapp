/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 23-06-21 12:08
 * Filename: Html
 */

/**
 * @class Bs
 */
export class Bs {

    /**
     * @description Create JQuery|
     * @param {string} tag
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static jQEl(tag: string, classes: string = '', options: any = {}): JQuery<HTMLElement> {

        let s = this;
        return $('<' + tag + '/>', s.options(classes, options));
    }

    /**
     * @description Returns div
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static div(classes: string = '', options: any = {}): JQuery<HTMLElement>{
        let s = this;

        return s.jQEl('div', classes, options);
    }

    /**
     * @description Returns textarea.form-control
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static textarea(classes: string = 'form-control', options: any = {}): JQuery<HTMLElement> {
        let s = this;
        classes = s.checkClass('form-control', classes);

        return s.jQEl('textarea', classes, options);
    }

    /**
     * @description Returns span.badge
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static badge(classes:string = 'badge', options: any = {}): JQuery<HTMLElement> {
        let s = this;
        classes = s.checkClass('badge', classes);

        return s.jQEl('span', classes, options);
    }

    /**
     * @description Returns div.alert
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static alert(classes: string = 'alert', options: any = {}): JQuery<HTMLElement> {

        let s = this;
        classes = s.checkClass('alert', classes);

        return s.jQEl('div', classes, options);
    }

    /**
     * @description Returns span
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static span(classes:string = '', options: any = {}): JQuery<HTMLElement> {

        let s = this;
        return s.jQEl('span', classes, options);
    }

    /**
     * @method options
     * @param {string} classes
     * @param {any} options
     * @return {Object}
     */
    static options(classes, options) {

        let ob = {
            class: classes
        };

        return Object.assign(ob, options);
    }

    /**
     * @description Returns div.container(-fluid)
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static ct(classes = 'container', options = {}): JQuery<HTMLElement>{

        let s = this;
        return s.jQEl('div', classes, options);
    }

    /**
     * @description Returns input.form-control
     * @param {string} classes
     * @param {any} options
     * @param {Object.<{event: string, callback: any}>} event
     * @return {JQuery<HTMLElement>}
     */
    static inp(classes: string = '', options = {}, event: {event: string, callback: any} = null): JQuery<HTMLElement> {
        let s = this;
        classes = s.checkClass('form-control', classes);

        let inp = s.jQEl('input', classes, options);

        if(event !== null) {

            inp.on(event.event, event.callback);
        }

        return inp;
    }

    /**
     * @description Returns select.custom-select
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static select(classes = '', options = {}): JQuery<HTMLElement> {
        let s = this;
        classes = s.checkClass('custom-select', classes);

        return s.jQEl('select', classes, options);
    }

    /**
     * @description Returns option
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static option(classes = '', options = {}): JQuery<HTMLElement> {
        let s = this;

        return s.jQEl('option', classes, options);
    }

    /**
     * @description Returns div.input-group
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static inpGrp(classes = '', options = {}): JQuery<HTMLElement> {
        let s = this;
        classes = s.checkClass('input-group ', classes);

        return s.jQEl('div', classes, options);
    }

    /**
     * @description Returns div.btn-group
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static btnGrp(classes = '', options = {}): JQuery<HTMLElement> {
        let s = this;
        classes = s.checkClass('btn-group ', classes);

        return s.jQEl('div', classes, options);
    }

    /**
     * @description Returns div.input-group-prepend
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static inpGrpPrep(classes = '', options = {}): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkClass('input-group-prepend', classes);

        return s.jQEl('div', classes, options);
    }

    /**
     * @description Returns div.input-group-append
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static inpGrpApp(classes = '', options = {}): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkClass('input-group-append', classes);

        return s.jQEl('div', classes, options);
    }
    /**
     * @description Returns button.btn
     * @param {string} classes
     * @param {any} options
     * @param {} event
     * @return {JQuery<HTMLElement>}
     */
    static btn(classes = '', options = {}, event: {event?: string, callback: any} = null): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkClass('btn ', classes);
        let btn = s.jQEl('button', classes, options);

        if(event !== null) {

            if(event.event === undefined) {
                event['event'] = 'click';
            }

            btn.on(event.event, event.callback);
        }

        return btn;
    }

    /**
     *
     * Returns div.row...
     * @param classes
     * @param options
     * @return {JQuery<HTMLElement>}
     */
    static row(classes = '', options = {}): JQuery<HTMLElement>{

        let s = this;
        classes = s.checkClass('row ', classes);
        return s.jQEl('div', classes, options);
    }

    /**
     * Returns div.col...
     * @param classes
     * @param options
     * @return {JQuery<HTMLElement>}
     */
    static col(classes: string = 'col', options = {}): JQuery<HTMLElement>{

        let s = this;
        // classes = s.checkClass('col ', classes);
        return s.jQEl('div', classes, options);
    }

    /**
     * Checks class for given string if not it prepends the given string
     * @param {string} str
     * @param {string} classes
     * @return {string}
     */
    static checkClass(str, classes: string) {

        return ((classes.indexOf(str) === -1) ? str + ' ' + classes : classes)
    }

    /**
     * Return div.card...
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static card(classes: string = '', options = {}): JQuery<HTMLElement>{

        let s = this;
        classes = s.checkClass('card', classes);
        return s.jQEl('div', classes, options);
    }

    /**
     * @param {string | number} headerNumber
     * @return {JQuery<HTMLElement>}
     */
    static h(headerNumber: string | number): JQuery<HTMLElement>{

        let s = this;

        if(typeof headerNumber === 'number') {
            headerNumber = headerNumber.toString();
        }

        return s.jQEl(`h${headerNumber}`);
    }

    /**
     * Return div.card-header...
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static header(classes: string = '', options = {}): JQuery<HTMLElement>{

        let s = this;
        classes = s.checkClass('card-header', classes);
        return s.jQEl('div', classes, options);
    }

    /**
     * Return div.card-body...
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static body(classes = '', options = {}): JQuery<HTMLElement>{

        let s = this;
        classes = s.checkClass('card-body', classes);
        return s.jQEl('div', classes, options);
    }

    /**
     * Return div.card-footer...
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static footer(classes = '', options = {}){

        let s = this;
        classes = s.checkClass('card-footer', classes);
        return s.jQEl('div', classes, options);
    }

    /**
     * Return i.fas...
     * @param {string} classes font-awesome icon-string
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static fab(classes, options = {}): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkFaClass('fab fa-', classes);
        return s.jQEl('i', classes, options);
    }

    /**
     * Return i.fas...
     * @param {string} classes font-awesome icon-string
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static fas(classes, options = {}): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkFaClass('fas fa-', classes);
        return s.jQEl('i', classes, options);
    }

    /**
     * Return i.far...
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static far(classes, options = {}): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkFaClass('far fa-', classes);
        return s.jQEl('i', classes, options);
    }

    /**
     * Checks class for given string if not it prepends the given string
     * @param {string} str
     * @param {string} classes
     * @return {string}
     */
    static checkFaClass(str, classes){

        if(classes.indexOf('fa-') === 0) {
            classes = classes.replace(/^fa-/, '');
        }

        return ((classes.indexOf(str) === -1) ? str + classes : classes)
    }

    /**
     * Return div.tab-content...
     * @param classes
     * @return {JQuery<HTMLElement>}
     */
    static tabContent(classes = ''): JQuery<HTMLElement> {

        let s = this
        classes = s.checkClass('tab-content', classes);
        return s.jQEl('div', classes, {});
    }

    /**
     * Return div.tab-pane...
     * @param classes
     * @param {string|JQuery} content,
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static tabPane(classes = '', content, options): JQuery<HTMLElement> {

        let s = this;
        options = options || {};
        classes = s.checkClass('tab-pane fade', classes);
        return s.jQEl('div', classes, options).html(content);
    }

    /**
     * Return div.nav nav-tabs...
     * @param classes
     * @return {JQuery<HTMLElement>}
     */
    static navTabUl(classes = ''): JQuery<HTMLElement> {

        let s = this
        classes = s.checkClass('nav nav-tabs', classes);
        return s.jQEl('ul', classes, {});
    }

    /**
     * Return li.nav-item... a.nav-link...
     * @param classes
     * @param {string|JQuery} content,
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static navTab(classes = '', content = '', options): JQuery<HTMLElement> {

        let s = this;
        options = options || {};
        classes = s.checkClass('nav-link', classes);
        let li = s.jQEl('li', 'nav-item ' + classes.replace('nav-link', ''));
        let a = s.jQEl('a', '' + classes, options).on('click', (e) => {

            e.preventDefault();
            // Fix for $(a).tab('show'); This is not working correct in a strict JS solution
            s.eventShowTab(a);
        }).html(content);
        li.append(a);

        return li;
    }

    /**
     * Fix for tab-show Bootstrap
     * @param {JQuery} a
     */
    static eventShowTab(a) {

        let curId = $(a).attr('id').replace('-tab', '');
        $(a).closest('ul').find('a.active').removeClass('active');
        $(a).addClass('active');
        $(a).closest('div').find('.tab-content:first > .tab-pane.active.show').removeClass('active show');
        $(a).closest('div').find('.tab-content:first > .tab-pane#' + curId).addClass('active show');
    }

    /**
     * @param {string} classes
     * @param {JQuery | string} content
     * @param options
     * @return {JQuery<HTMLElement>}
     */
    static accordion(classes = '', content: JQuery|string, options: any = {id: 'accordion'}): JQuery<HTMLElement> {

        let s = this
        classes = s.checkClass('', classes);
        return s.jQEl('div', classes, options);
    }

    /**
     *
     * @param {string} classes
     * @param {string} content
     * @param options
     * @return {JQuery<HTMLElement>}
     */
    static spanText(classes:string = '', content: string|JQuery = '', options: any = {}): JQuery<HTMLElement> {

        let s = this;
        classes = s.checkClass('input-group-text', classes);

        return s.jQEl('span', classes, options).append(content);
    }
}