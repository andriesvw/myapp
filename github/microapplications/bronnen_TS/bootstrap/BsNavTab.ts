/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 28/10/2022 10:36
 * Filename: BsNavTab
 */

import {BsFa6 as Bs} from "./BsFa6";
import {v4 as uuidv4} from 'uuid';
import Popover from 'bootstrap5/js/src/popover';

/**
 * @class BsNavTab
 */
export class BsNavTab {

    private _classes: string;
    private _tabClass: string = '';
    private _navTabs: JQuery;
    private _navPanes: JQuery;

    /**
     * @constructor
     * @param {string} classes
     */
    constructor(classes: string = '') {

        this.classes = classes;
        this.init();
    }

    /**
     * @private
     */
    private init(){

        let s = this;
        s.navTabs = Bs.jQEl('ul', `nav nav-tabs ${s.classes}`.trim());
        s.navPanes = Bs.div(`tab-content ${s.classes}`.trim());
    }

    /**
     * @return {string}
     */
    public get tabClass(): string {
        return this._tabClass;
    }

    /**
     * @param {string} classes
     */
    public set tabClass(classes: string) {
        this._tabClass = classes;
    }

    /**
     * @return {string}
     * @protected
     */
    protected get classes(){
        return this._classes;
    }

    /**
     * @param {string} classes
     * @protected
     */
    protected set classes(classes: string) {
        this._classes = classes;
    }

    /**
     * Hide navTabs ul-element
     */
    public hide(): void {
        this._navTabs.addClass('d-none');
    }

    /**
     * Hide navTabs ul-element
     */
    public show(): void {
        this._navTabs.removeClass('d-none');
    }

    /**
     * @return {JQuery}
     */
    public getTabs(): JQuery {
        return this._navTabs;
    }

    /**
     * @return {JQuery}
     */
    public getPanes(): JQuery {
        return this._navPanes;
    }

    /**
     * @return {JQuery}
     */
    public get navTabs(): JQuery {
        return this._navTabs;
    }

    /**
     * @param {JQuery} content
     * @protected
     */
    protected set navTabs(content: JQuery) {
        this._navTabs = content;
    }

    /**
     * @return {JQuery}
     */
    public get navPanes(): JQuery {
        return this._navPanes;
    }

    /**
     * @param {JQuery} content
     * @protected
     */
    protected set navPanes(content: JQuery) {
        this._navPanes = content;
    }

    /**
     * @return {string}
     * @protected
     */
    protected uuid(): string {
        return uuidv4().replace(/-/g, '');
    }

    /**
     * @param {JQuery | string} tab
     * @param {string} uuid
     * @param {boolean} active
     * @return {JQuery}
     */
    public getNavTab(tab: JQuery|string, uuid: string ,active: boolean = false): JQuery {

        let s = this;

        let navTab = Bs.jQEl('li', `nav-item`, {
            role: 'presentation'
        });
        let a = Bs.jQEl('button', `nav-link ${s.tabClass}`, {
            id: `${uuid}-tab`,
            'data-bs-toggle': 'tab',
            'data-bs-target': `#${uuid}`,
            // role: 'tab',
            type: 'button',
            'aria-controls': uuid,
            'aria-selected': 'false'
        }).append(tab);

        if(active) {
            a.addClass('active').removeClass('text-light');
            a.attr('aria-selected', 'true');
        }

        navTab.append(a);

        return navTab;
    }

    /**
     * @param {JQuery | string} pane
     * @param {string} uuid
     * @param {boolean} active
     * @return {JQuery}
     */
    public getNavPane(pane: JQuery|string, uuid: string ,active: boolean = false): JQuery {

        let navPane = Bs.div('tab-pane', {
            id: uuid,
            // role: 'tabpanel',
            'aria-labelledby': uuid + '-tab'
        }).append(pane);

        if(active) {
            navPane.addClass('show active');
        }

        return navPane;
    }

    /**
     * @param {string | JQuery} tab
     * @param {string | JQuery} pane
     * @param {boolean} active
     * @param {string} time
     * @public
     */
    public addNavTabPane(tab: string | JQuery, pane: string | JQuery, active: boolean = false, time: string = '') {

        let s = this;
        let uuid = `nav${s.uuid()}`;

        let navTab = s.getNavTab(tab, uuid, active);

        if(time !== ''){

            let div = Bs.div().append(Bs.far('fa-clock mr-1'), time.toString())
            new Popover(navTab, {
                content: div,
                placement: 'top',
                trigger: 'hover',
                html: true
            });
        }
        let navPane = s.getNavPane(pane, uuid, active);

        s._navTabs.append(navTab);
        s._navPanes.append(navPane);
    }
}