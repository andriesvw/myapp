/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 23-06-21 12:08
 * Filename: BsFa6.js
 */

import {Bs} from './Bs';

/**
 * @class BsFa6
 */
export class BsFa6 extends Bs {

    /**
     * Return i.fas...
     * @param {string} classes font-awesome icon-string
     * @param {Object} options
     * @param {boolean} fw
     * @return {JQuery<HTMLElement>}
     */
    static fab(classes: string, options = {}, fw: boolean = true): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkFaClass('fa-brands fa-', classes);
        return s.jQEl('i', classes, options);
    }

    /**
     * Return i.fas...
     * @param {string} classes font-awesome icon-string
     * @param {Object} options
     * @param {boolean} fw
     * @return {JQuery<HTMLElement>}
     */
    static fas(classes: string, options = {}, fw:boolean = true): JQuery<HTMLElement>{
        let s = this;
        if(fw) {

        }
        classes = s.checkFaClass('fa-solid fa-', classes);
        return s.jQEl('i', classes, options);
    }

    /**
     * Return i.far...
     * @param {string} classes
     * @param {Object} options
     * @param {boolean} fw
     * @return {JQuery<HTMLElement>}
     */
    static far(classes: string, options = {},  fw: boolean = true): JQuery<HTMLElement>{
        let s = this;
        classes = s.checkFaClass('fa-regular fa-', classes);
        return s.jQEl('i', classes, options);
    }

    /**
     * Checks class for given string if not it prepends the given string
     * @param {string} str
     * @param {string} classes
     * @param {boolean} fw
     * @return {string}
     */
    static checkFaClass(str: string, classes: string, fw: boolean = false): string{

        if(classes.indexOf('fa-') === 0) {
            classes = classes.replace(/^fa-/, '');
        }

        return ((classes.indexOf(str) === -1) ? str + classes : classes)
    }

    /**
     * @description Returns select.custom-select
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static select(classes: string = '', options = {}): JQuery<HTMLElement> {
        let s = this;
        classes = s.checkClass('form-select', classes);

        return s.jQEl('select', classes, options);
    }

    /**
     * @description Returns div.<classes>
     * @param {string} classes
     * @param {any} options
     * @return {JQuery<HTMLElement>}
     */
    static container(classes: string = 'container', options = {}): JQuery<HTMLElement> {
        let s = this;

        return s.jQEl('div', classes, options);
    }
}