import {AbstractApplication} from "./AbstractApplication";

export class ApplicationMain extends AbstractApplication {

    protected _app: any;

    public constructor() {
        super();

        this.initialize();
    }

    protected initialize(){

        let s = this;
        s.app = s.getInitialAppData();
        s.id = s.getUrlParameter('id');
        s.application = s.getCurrentApplication();
    }

    /**
     * @return {number|string}
     */
    public get id() {
        return this._app.id;
    }

    /**
     *
     * @param {number|string} id
     * @protected
     */
    protected  set id(id: number|string){
        this._app.id = id;
    }

    /**
     *
     * @return {string}
     */
    public get title() {

        return this._app.exercisetitle;
    }

    /**
     *
     * @param {string} title
     * @protected
     */
    protected set title(title: string) {

        document.title = this._app.exercisetitle = this._app.main_object.title = title.trim();
    }

    /**
     *
     * @param {string} key
     */
    public prop(key: string) {

        if(this._app.main_object[key] !== undefined) {
            return this._app.main_object[key];
        }

        return null;
    }

    /**
     *
     * @param {string} key
     * @param {*} value
     * @protected
     */
    protected setProp(key: string, value: any) {

        this._app.main_object[key] = value;
    }

    /**
     *
     * @return {string}
     */
    public get language() {

        return this._app.language;
    }

    /**
     *
     * @param {string} language
     * @protected
     */
    protected set language(language: string) {

        this._app.language = this._app.main_object.language = language;
    }

    /**
     *
     * @return {*}
     */
    public get app(){
        return this._app;
    }

    /**
     *
     * @param {*} response
     * @protected
     */
    protected set app(response: any) {

        this._app = response;
    }

    /**
     *
     * @return {string}
     */
    public get application(){

        return this._app.application;
    }

    /**
     *
     * @param {string} application
     * @protected
     */
    protected set application(application: string) {

        this._app.application = application;
    }
}