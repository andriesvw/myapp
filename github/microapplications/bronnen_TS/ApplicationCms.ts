import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/autocomplete';
import bootbox from 'bootbox5';

import type {AppType} from './types/AppType';
import {AbstractApplication} from "./AbstractApplication";
import {BsFa6 as Bs} from "./bootstrap/BsFa6";
import {AppBlock} from "./AppBlock";

export class ApplicationCms extends AbstractApplication {

    protected _target: string|JQuery = 'body';
    protected _app: AppType;
    protected _controlsRow: JQuery;
    protected _blocks: AppBlock[] = [];

    public constructor() {
        super();
        this.init();
    }

    /**
     * @return {JQuery}
     * @protected
     */
    protected get controlsRow(): JQuery {
        return this._controlsRow;
    }

    /**
     * @param {JQuery} controlsRow
     * @protected
     */
    protected set controlsRow(controlsRow: JQuery) {
        this._controlsRow = controlsRow;
    }

    /**
     * @protected
     */
    protected get audio(){
        if(this.prop('audio') === undefined || this.prop('audio') === null){
            this.setProp('audio', '');
        }
        return this.prop('audio');
    }

    /**
     * @param {string} value
     * @protected
     */
    protected set audio(value: string) {
        this.setProp('audio', value);
    }

    /**
     * @return {string}
     * @protected
     */
    protected get image(){

        if(this.prop('image') === undefined || this.prop('image') === null){
            this.setProp('image', '');
        }
        return this.prop('image');
    }

    /**
     * @param {string} value
     * @protected
     */
    protected set image(value: string) {
        this.setProp('image', value);
    }

    /**
     * @protected
     */
    protected init(){

        let s = this;
        s.editUrl = '/admin/appexercise/edit';
        s.mediaUrl = 'https://media.fcsprint2.nl'
        s.app = s.getInitialAppData();
        s.application = s.getCurrentApplication();
        s.id = s.getUrlParameter('id');
    }

    protected target(): JQuery {

        if(typeof this._target === 'string') {
            return $('body');
        }

        return this._target;
    }

    /**
     * @return {AppType}
     * @protected
     */
    protected get app(): AppType {
        return this._app;
    }

    /**
     * @param {AppType} app
     * @protected
     */
    protected set app(app: AppType) {
        this._app = app;
    }

    /**
     * @return {number | string}
     * @protected
     */
    protected get id(): number|string {
        return this._app.id;
    }

    /**
     * @param {number | string} id
     * @protected
     */
    protected set id(id: number|string) {
        this._app.id = id;
    }

    /**
     * @return {string}
     * @protected
     */
    protected get uuid(): string {
        return this._app.uuid;
    }

    /**
     * @param {string} uuid
     * @protected
     */
    protected set uuid(uuid: string) {
        this._app.uuid = uuid;
    }

    /**
     * @return {string}
     * @protected
     */
    protected get title(): string {
        return this._app.exercisetitle;
    }

    /**
     * @param {AppBlock} block
     * @return {void}
     * @protected
     */
    protected addBlock(block: AppBlock) {
        this._blocks.push(block);
    }

    /**
     * @param {number} index
     * @param {JQuery} target
     * @return {void}
     * @protected
     */
    protected removeBlock(index: number, target: JQuery) {

        // let index = this._blocks.map(e => e.content).indexOf(block.content);

        if(index > -1) {
            this._blocks.splice(index, 1);
            target.remove();
        }
    }

    /**
     * @return {void}
     * @protected
     */
    protected clearBlocks(): void {
        this._blocks = [];
    }

    /**
     * @param {string} title
     * @protected
     */
    protected set title(title: string) {

        this._app.exercisetitle = title;
        document.title = (this.title === '') ? this.application + ' | Exercise' : this.title;
    }

    /**
     * @return {string}
     * @protected
     */
    protected get application(): string {
        return this._app.application;
    }

    /**
     * @param {string} application
     * @protected
     */
    protected set application(application: string) {
        this._app.application = application;
    }

    /**
     * @return {string}
     * @protected
     */
    protected get language(): string {
        return this._app.language;
    }

    /**
     * @param {string} language
     * @protected
     */
    protected set language(language: string) {

        this._app.language = language;
        this.setProp('language', language);
    }

    /**
     * @return {string}
     * @protected
     */
    protected get createdAt(): string {
        return this._app.createdAt;
    }

    /**
     * @return {string}
     * @protected
     */
    protected get updatedAt(): string {
        return this._app.updatedAt;
    }

    /**
     * @param {string} key
     * @return {any}
     * @protected
     */
    protected prop(key: string): any {

        if(this._app.main_object[key] !== undefined) {
            return this._app.main_object[key];
        }

        return null;
    }

    protected get blocks(): AppBlock[] {
        return this._blocks;
    }

    protected set blocks(blocks: AppBlock[]) {
        this._blocks = blocks;
    }

    /**
     * @param {string} key
     * @param value
     * @protected
     */
    protected setProp(key: string, value: any) {
        this._app.main_object[key] = value;
    }

    /**
     * @return {JQuery}
     * @protected
     */
    protected getWorkAreaRow(): JQuery {
        return Bs.row('row-work-area')
    }

    /**
     * @return {JQuery}
     * @protected
     */
    protected inpGrpTitle(): JQuery {

        let s = this;
        let grp = Bs.inpGrp('input-group-title');
        let inp = Bs.inp('input-title', {
            placeholder: 'Title',
            value: s.title
        }, ({event: 'input', callback: (e: JQuery.KeyPressEvent) => {

                s.title = e.currentTarget.value.toString().trim()
                $('.card-header-bronnen').html((s.title === '') ? s.application + ' | Exercise' : s.title);
            }}));
        // let prep = Bs.inpGrpPrep();
        let span = Bs.spanText('', Bs.fas('fa-heading fa-fw'));

        // prep.append(span);
        grp.append(span, inp);
        return grp;
    }

    /**
     * @return {number}
     * @protected
     */
    protected getIndex(): number {

        return this._blocks.length;
    }

    /**
     * @param {number} index
     * @param {{content: "", audio?: "", image?: ""}} data
     * @return {JQuery}
     * @protected
     */
    protected getExerciseBlock(index: number, data: {content: string, audio?: string, image?: string} = {content: '', audio: '', image: ''}): JQuery {

        let s = this;
        let blck = new AppBlock(s, index, data);

        blck.initCmsGroups();
        s.addBlock(blck);

        let col = Bs.col('col-3 mt-3');
        let card = Bs.card('card border border-secondary bg-light');
        let header = Bs.header('card-header card-app-block py-1');
        let rowHeader = Bs.row();
        let colHeader = Bs.col('text-end');
        let removeBlckButton = Bs.btn('btn-outline-danger btn-sm').on('click', () => {
            s.removeBlock(index, col);

        }).on('mouseenter', () => {
            card.removeClass('bg-light border-secondary').addClass('alert alert-danger border border-danger');
        }).on('mouseleave', () => {
            card.addClass('bg-light border-secondary').removeClass('alert alert-danger border border-danger');
        }).append(Bs.fas('fa-trash fa-fw'));
        colHeader.append(removeBlckButton);
        rowHeader.append(colHeader);
        header.append(rowHeader);

        let body = Bs.body('py-1');
        let row = Bs.row();
        let col1 = Bs.col('col-12 mb-1').append(blck.inputGrpContent);
        let col2 = Bs.col('col-12 mb-1').append(blck.inputGrpAudio);
        let col3 = Bs.col('col-12 mb-1').append(blck.inputGrpImage);
        row.append(col1, col2, col3);
        body.append(row);
        card.append(header, body);
        col.append(card);

        return col;
    }

    /**
     * @param {boolean} disabled
     * @return {jQuery}
     * @protected
     */
    protected getAudioInput(disabled: boolean = false){

        let s = this;
        let col = Bs.col('col-md-6 mt-3');
        let grp = Bs.inpGrp();
        // let app = Bs.inpGrpApp();
        let btn = Bs.btn('btn-outline-secondary disabled').attr({
            disabled: 'disabled'
        }).append(Bs.fas('fa-play fa-fw'));
        // app.append(btn);
        let inp = Bs.inp('col-media-audio', {
            placeholder: 'Audio',
            value: s.audio
        });

        if(!disabled) {

            let au = new Audio();
            au.onended = () => {

                btn.empty().append(Bs.fas('fa-play fa-fw'));
            };

            let stopPlay = () => {
                /**
                 * @type {Audio} au
                 */
                au.pause();
                au.currentTime = 0;
                btn.empty().append(Bs.fas('fa-play fa-fw'));
                btn.off('click');
                btn.on('click', startPlay);
            }

            let startPlay = () => {
                au.play();
                btn.empty().append(Bs.fas('fa-pause fa-fw'));
                btn.off('click');
                btn.on('click', stopPlay);
            };

            let canPlay = () => {
                btn
                    .removeClass('btn-outline-secondary disabled')
                    .addClass('btn-outline-primary')
                    .removeAttr('disabled');
                btn.off('click');
                btn.on('click', startPlay);
            }

            if (s.audio !== '') {

                au.src = s.mediaUrl + '/media/' + s.language + '/audio/' + s.audio;

                canPlay();
            }

            inp.on('blur', (e: JQuery.BlurEvent) => {

                let value = e.currentTarget.value.toString().trim();
                if (value === '') {
                    s.audio = value;
                    btn.off('click').removeClass('btn-outline-primary').addClass('btn-outline-secondary disabled').attr({
                        disabled: 'disabled'
                    });
                } else {

                    s.audio = s.findAudio(value);
                    inp.val(s.audio);
                    au.src = s.mediaUrl + '/media/' + s.language + '/audio/' + s.audio;

                    canPlay()
                }
            });

            s.makeAutoComplete(inp, s.media.audio.files, (event, ui) => {

                s.audio = ui.item.value;
            });
        }
        else {
            grp.css({opacity: '0.3'});
            inp.attr({disabled: 'disabled'}).addClass('disabled');
        }

        grp.append(inp, btn);
        col.append(grp);

        return col;
    }

    /**
     * @param {boolean} disabled
     * @return {JQuery}
     * @protected
     */
    protected getImageInput(disabled = false): JQuery {

        let s = this;
        let col = Bs.col('col-md-6 mt-3');
        let grp = Bs.inpGrp();
        // let app = Bs.inpGrpApp();
        let im = new Image();
        let btn = Bs.btn('btn-outline-secondary disabled').attr({
            disabled: 'disabled'
        }).append(Bs.fas('fa-camera fa-fw'));
        // app.append(btn);

        let inp = Bs.inp('col-media-image', {
            placeholder: 'Image',
            value: s.image
        });

        if(!disabled) {

            if (s.image !== '') {

                im.src = s.mediaUrl + '/media/' + s.language + '/img/' + s.image;
                btn
                    .removeClass('btn-outline-secondary disabled')
                    .addClass('btn-outline-primary')
                    .removeAttr('disabled');
            }

            btn.on('click', () => {

                if (!btn.hasClass('disabled') && s.image !== '') {

                    im.src = s.mediaUrl + '/media/' + s.language + '/img/' + s.image;
                    s.previewImage(im);
                }
            });

            inp.on('blur', (e: JQuery.BlurEvent) => {

                let value = e.currentTarget.value.toString().trim();
                s.image = s.findImage(value);

                if (s.image === '') {
                    s.image = value;
                    btn
                        .addClass('btn-outline-secondary disabled')
                        .removeClass('btn-outline-primary')
                        .attr('disabled', 'disabled');
                } else {
                    inp.val(s.image);
                    btn
                        .removeClass('btn-outline-secondary disabled')
                        .addClass('btn-outline-primary')
                        .removeAttr('disabled');
                }
            });

            s.makeAutoComplete(inp, s.media.image.files, (event, ui) => {

                s.image = ui.item.value;
            });
        }
        else {
            grp.css({opacity: '0.3'});
            inp.attr({disabled: 'disabled'}).addClass('disabled');
        }

        grp.append(inp, btn);
        col.append(grp);

        return col;
    }


    /**
     * @param {HTMLImageElement} image
     * @protected
     */
    protected previewImage(image: HTMLImageElement): void {

        let row = Bs.row('row-bootbox-image');
        let col = Bs.col('col-bootbox-image text-center');
        col.append(image);
        row.append(col);

        bootbox.alert({
            size: 'large',
            title: '<h6>' + image.src + '</h6>',
            message: row,
            backdrop: true,
            onEscape: true
        });
    }

    /**
     * @return {*|JQuery}
     * @protected
     */
    protected getSaveButton(): JQuery {

        let s = this;
        let btn = Bs.btn('btn-outline-primary').append(Bs.fas('fa-save fa-fw'));

        (<any>btn).popover({
            trigger: 'hover',
            placement: 'top',
            content: 'Save'
        });

        btn.on('click', () => {
            let result = this._blocks.map((e) => e.data).filter((e) => {
                return e.content !== '';
            });

            s.setProp('exercises', result);
            s.setProp('title', s.title);
            s.sendData(s.app, true);
        });

        return btn;
    }

    /**
     * @return {*|JQuery}
     * @protected
     */
    protected getSaveReturnIndexButton(): JQuery{

        let s = this;

        let btn = Bs.btn(' btn-primary').append(Bs.fas('fa-save fa-fw'), Bs.fas('fa-reply fa-fw'));

        (<any>btn).popover({
            trigger: 'hover',
            placement: 'top',
            content: 'Save & Exit'
        });

        btn.on('click', () => {

            let result = this._blocks.map(e => e.data).filter((e) => {
                return e.content !== '';
            });
            s.setProp('exercises', result);
            s.setProp('title', s.title);
            // // console.log(s.app)
            s.sendData(s.app, false);
        });

        return btn;
    }

    /**
     * @param {JQuery} workArea
     * @return {JQuery}
     * @protected
     */
    getAddBlockButton(workArea: JQuery): JQuery {

        let s = this;
        return Bs.btn('btn-outline-dark').on('click', () => {
            workArea.append(s.getExerciseBlock(s.getIndex(), {content: '', audio: '', image: ''}));
        }).append(Bs.fas('fa-plus fa-fw'));
    }

    /**
     * @param {JQuery} workArea
     * @return {JQuery}
     * @protected
     */
    getAddBulkButton(workArea: JQuery): JQuery {

        let s = this;
        return Bs.btn('btn-outline-dark').on('click', () => {

            s.getBulkBox(workArea);
            // workArea.append(s.getExerciseBlock(s.getIndex(), {content: '', audio: '', image: ''}));
        }).append(Bs.fas('fa-database fa-fw'));
    }

    /**
     * @param {JQuery} workArea
     */
    getBulkBox(workArea: JQuery) {

        let s = this;
        let row = Bs.row('row-bootbox-image');
        let col = Bs.col('col-bootbox-image text-center');
        let text = (s.blocks.length > 0) ? s.blocks
            .map((e) => {
                return e.content
            })
            .filter((e) => {
                return e !== ''
            })
            .join('\n') : '';
        let textArea = Bs.textarea('', {
            rows: '20'
        }).text(text);
        col.append(textArea);
        row.append(col);

        bootbox.confirm({
            size: 'large',
            title: '<h6>' + s.application + ' | Bulk</h6>',
            message: row,
            callback: (result) => {
                if(result) {
                    let bulk = textArea.val().toString().trim().split(/\n/g).filter((e) => {
                        return e.trim() !== '';
                    });

                    s.handleBulk(workArea, bulk);
                }
            },
            backdrop: true,
            onEscape: true,
            buttons: {
                confirm: {
                    label: '<i class="fa-solid fa-check fa-fw"></i>',
                    className: 'btn-outline-primary'
                },
                cancel: {
                    label: '<i class="fa-solid fa-times fa-fw"></i>',
                    className: 'btn-outline-secondary'
                }
            },
        });
    }

    /**
     * @param {JQuery} workArea
     * @param {string[]} bulk
     * @protected
     */
    protected handleBulk(workArea: JQuery, bulk: string[]): void {

        let s = this;
        s.clearBlocks();
        workArea.empty();
        bulk.forEach((item, i) => {

            let data = {
                content: item,
                audio: s.findAudio(item),
                image: s.findImage(item)
            };

            workArea.append(s.getExerciseBlock(i, data));
        });
    }

    /**
     * @return {JQuery}
     * @protected
     */
    getControlButtons(): JQuery{

        let s = this;
        let col = Bs.col('col-4 text-end');
        let grp = $('<div/>', {
            class: 'btn-group'
        });

        grp.append(
            s.getSaveButton(),
            s.getSaveReturnIndexButton()
        );

        col.append(grp);

        return col;
    }
}
