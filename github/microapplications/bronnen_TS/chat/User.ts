/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 07/03/2024 09:24
 * Filename: User
 */

type userType = {
    id: string | number,
    uuid?: string,
    name?: string,
    email: string,
    avatar?: string,
    roles?: string[],
    progress?: boolean
};
type userOptionsType = {
    hidden?: boolean
}
import {v4 as uuidv4} from 'uuid';
import {Chat} from "./Chat";
import {Message, messageDataType} from "./Message";
import {FontAwesome as Fa} from "../FontAwesome";

/**
 * @class User
 */
export class User {

    private _chat: Chat;
    private _data: userType;
    private _options: userOptionsType;
    private _callback: any = null;

    constructor(data: userType = null, chat: Chat = null, options: userOptionsType = null) {

        if(data === null){
            data = this.createDummy();
        }

        this.data = data;
        this.chat = chat;
        this.options = options;

        console.log(this.data);

    }

    public createDummy(): userType {
        let uuid = uuidv4();
        return {
            id: uuid,
            uuid: uuid,
            name: 'User',
            email: '',
            avatar: 'fa-user',
            roles: ['ROLE_USER']
        };
    }

    public get data(): userType {
        return this._data;
    }

    public set data(value: userType) {
        this._data = value;
    }

    public option(key: string) {
        return (this.options[key] !== undefined) ? this.options[key] : null;
    }

    public get options(): userOptionsType {
        return this._options;
    }

    public set options(value: userOptionsType) {
        this._options = value;
    }

    public get id(): string | number{

        return this.data.id;
    }

    public set id(value: string | number) {
        this.data.id = value;
    }

    public get name(): string {

        if(this.data.name === undefined || this.data.name === ''){
            return this.email;
        }

        return this.data.name;
    }

    public set name(value: string) {
        this.data.name = value;
    }

    public get uuid(): string {

        if(this.data.uuid === undefined) {
            this.data.uuid = uuidv4();
        }

        return this.data.uuid;
    }

    public set uuid(value: string) {
        this.data.email = value;
    }

    public get email(): string {
        return this.data.email;
    }

    public set email(value: string) {
        this.data.email = value;
    }

    public get progress(): boolean {

        if(this.data.progress === undefined) {
            this.data.progress = false;
        }
        return this.data.progress;
    }

    public set progress(value: boolean) {
        this.data.progress = value;
    }

    public get icon(): JQuery<HTMLElement> | null {

        const s = this;

        if(s.avatar === '') {

            if(s.isApplication()) {
                return null;
            }
            else if(s.isSystem()) {
                return Fa.solid('fa-gear')
            }
            else if(this.isAssistent()) {
                return Fa.regular('fa-user')
            }
            else if(this.isModerator()) {
                return Fa.solid('fa-user-tie')
            }
            else if(this.isAdmin()) {
                return Fa.solid('fa-user-shield')
            }
            else if(this.isSuperAdmin()) {
                return Fa.solid('fa-user-gear')
            }
            else {
                return Fa.solid('fa-user')
            }
        }

        return Fa.solid(s.avatar)
    }

    public get avatar(): string {

        if(this.data.avatar === undefined){
            return ''
        }

        return this.data.avatar;
    }

    public set avatar(value: string) {
        this.data.avatar = value;
    }

    public isSuperAdmin(): boolean {
        return (this.data.roles.indexOf('ROLE_SUPER_ADMIN') > -1);
    }

    public isApplication(): boolean {
        return (this.data.roles.indexOf('ROLE_APPLICATION') > -1);
    }

    public isUser(): boolean {
        return (this.data.roles.indexOf('ROLE_USER') > -1);
    }

    public isAdmin(): boolean {
        return (this.data.roles.indexOf('ROLE_ADMIN') > -1);
    }

    public isModerator(): boolean {
        return (this.data.roles.indexOf('ROLE_MODERATOR') > -1);
    }

    public isSystem(): boolean {
        return (this.data.roles.indexOf('ROLE_SYSTEM') > -1);
    }

    public isAssistent(): boolean {

        return (this.data.roles.indexOf('ROLE_ASSISTENT') > -1);
    }

    public get roles(): string[] {
        return this.data.roles;
    }

    public set roles(value: string[]) {
        this.data.roles = value;
    }

    public get hidden(): boolean {
        return this.options.hidden;
    }

    public set hidden(value: boolean) {
        this.options.hidden = value;
    }

    public get chat(): Chat {
        return this._chat;
    }

    public set chat(value: Chat) {
        this._chat = value;
    }

    public get callback(): any {
        return this._callback;
    }

    public set callback(value: any) {
        this._callback = value;
    }

    public getMessages(): messageDataType[] {
        const s = this;

        return s.chat.messages
            .filter((message: Message): boolean=> {
                return message.user === s;
            })
            .map((message: Message) => {
                return message.data;
            });
    }
}