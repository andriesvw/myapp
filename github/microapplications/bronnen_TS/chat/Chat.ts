/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 07/03/2024 09:24
 * Filename: Chat
 */
import '../main-bs5-fa6.scss';
import 'fontawesome6/js/all';
import './css/chat.scss';

import {User} from "./User";
import {v4 as uuidv4} from 'uuid';
import {BsFa6} from "../bootstrap/BsFa6";
import {FontAwesome as Fa} from "../FontAwesome";
import {Message, messageDataType} from "./Message";
import {ChatTimer} from "./ChatTimer";
import {ChatScore} from "./ChatScore";
import $ from "jquery";
import {Header} from "./Header";
import {Body} from "./Body";
import {Footer} from "./Footer";
import {Card} from "./Card";

export type chatOptionsType = {
    timer?: boolean,
    score?: boolean,
    anchor?: boolean,
    ai?: boolean
};
export type message = {
    user: User,
    content: any
}
const defaultOptions: chatOptionsType = {
    timer: true,
    score: true,
    anchor: true,
    ai: false
};

/**
 * @class Chat
 */
export class Chat {

    private _lastMessage: Message = null;
    private _hideInputField: boolean = false;

    constructor(target: string | JQuery<HTMLElement> = null, options: chatOptionsType = defaultOptions) {

        const s = this;
        s.checkExistingMode();
        s.target = s.checkTarget(target);
        s.id = uuidv4();
        s.options = options;

        let uuidSystem = uuidv4();
        let uuidAssistent = uuidv4();
        s.system = new User({
            id: uuidSystem,
            uuid: uuidSystem,
            name: 'SYSTEM',
            email: '',
            roles: ['ROLE_SYSTEM'],
            avatar: ''
        }, s, {hidden: true});
        s.assistent = new User({
            id: uuidAssistent,
            uuid: uuidAssistent,
            name: 'ASSISTENT',
            email: '',
            roles: ['ROLE_ASSISTENT'],
            avatar: ''
        }, s);

        s.score = new ChatScore(s);
        s.timer = new ChatTimer(s);
        // s.timer.start();
        //
        // setTimeout(() => {
        //     s.timer.stop();
        // }, 240000)
        // this._appTimer = new AppTimer();

        s.init();
    }

    private _darkMode: boolean = false;

    public get darkMode(): boolean {
        return this._darkMode;
    }

    public set darkMode(value: boolean) {

        const s = this;
        s._darkMode = value;

        window.localStorage.setItem('darkMode', ((s.darkMode) ? 'true' : 'false'));

        if (s.darkMode) {
            $('html').attr('data-bs-theme', 'dark');
        }
        else {
            $('html').removeAttr('data-bs-theme')
        }

    }

    private _id: string;

    public get id(): string {
        return this._id;
    }

    public set id(value: string) {
        this._id = value;
    }

    private _options: chatOptionsType;

    public get options(): chatOptionsType {
        return this._options;
    }

    public set options(value: chatOptionsType) {
        this._options = value;
    }

    private _target: JQuery<HTMLElement>;

    public get target(): JQuery<HTMLElement> {

        return this._target;
    }

    public set target(value: JQuery<HTMLElement>) {
        this._target = value;
    }

    private _card: Card;

    public get card(): Card {
        return this._card;
    }

    public set card(value: Card) {
        this._card = value;
    }

    private _messagesBody: JQuery<HTMLElement>;

    public get messagesBody(): JQuery<HTMLElement> {
        return this._messagesBody;
    }

    public set messagesBody(value: JQuery<HTMLElement>) {
        this._messagesBody = value;
    }

    private _inputBody: JQuery<HTMLElement>;

    public get inputBody(): JQuery<HTMLElement> {
        return this._inputBody;
    }

    public set inputBody(value: JQuery<HTMLElement>) {
        this._inputBody = value;
    }

    private _input: JQuery<HTMLElement>;

    public get input(): JQuery<HTMLElement> {
        return this._input;
    }

    public set input(value: JQuery<HTMLElement>) {
        this._input = value;
    }

    private _timer: ChatTimer;

    public get timer(): ChatTimer {
        return this._timer;
    }

    public set timer(value: ChatTimer) {
        this._timer = value;
    }

    private _score: ChatScore;

    public get score(): ChatScore {
        return this._score;
    }

    public set score(value: ChatScore) {
        this._score = value;
    }

    // Users
    private _system: User;

    public get system(): User {
        return this._system;
    }

    public set system(value: User) {
        this._system = value;
    }

    private _assistent: User;

    public get assistent(): User {
        return this._assistent;
    }

    public set assistent(value: User) {
        this._assistent = value;
    }

    private _application: User;

    public get application(): User {
        return this._application;
    }

    public set application(value: User) {
        this._application = value;
    }

    private _user:  User;

    public get user(): User {
        return this._user;
    }

    public set user(value: User) {
        this._user = value;
    }

    private _users: User[] = [];

    public get users(): User[] {
        return this._users;
    }

    public set users(value: User[]) {
        this._users = value;
    }

    private _messages: Message[] = [];

    public get messages(): Message[] {
        return this._messages;
    }

    public set messages(value: Message[]) {
        this._messages = value;
    }

    private _rowLastMessage: JQuery<HTMLElement>;

    public get rowLastMessage(): JQuery<HTMLElement> {
        return this._rowLastMessage;
    }

    public set rowLastMessage(value: JQuery<HTMLElement>) {
        this._rowLastMessage = value;
    }

    public aiEnabled(): boolean {
        return this.options.ai;
    }

    public addUser(user: User): void {

        this.users.push(user);
    }

    public addMessage(message: Message, callback = null): void {

        const s = this;

        message.chat = s;

        if(s.users.length === 0) {
            message.icon = false;
        }
        s.messages.push(message);

        if(message.user === s.user) {

            message.options['align'] = 'end';
        }

        if(message.user === s.user || message.user === s.assistent || message.user.isApplication()) {

            s.card.cardBody.appendMessage(message);

            // setTimeout(() => {
            //
            //     const s = this;
            //     let data: messageDataType = {
            //         user: s.assistent,
            //         content: `Hello this is your assistent ${s.messages.length}`
            //     };
            //
            //     let message: Message = new Message(s, data);
            //
            //     s.addMessage(message);
            // }, 750)
        }

        if(message.user === s.user &&  s.user.callback !== null) {

            s.user.callback(message);
        }
    }

    //
    public init(): void {

        const s = this;
        s.determineUser();

        s.card = new Card(s);

        s.target.append(s.card.html);

        // s.body = BsFa6.card('chat').css({
        //     height: '100vh'
        // });
        //
        // s.cardHeader = BsFa6.div('card-header').append(s.getCardHeaderContent());
        //
        //
        // s.cardBody = BsFa6.div('card-body').css({
        //     'overflow-y': 'auto'
        // });
        //
        // s.setScroll();
        //
        // s.messagesBody = BsFa6.row();
        // s.cardBody.append(s.messagesBody)
        //
        // s.inputBody = s.getCardFooter();
        // s.body.append(s.cardHeader, s.cardBody, s.inputBody);
    }

    protected checkExistingMode() {

        if(window.localStorage.getItem('darkMode') === null) {
            window.localStorage.setItem('darkMode', 'false')
        }

        this.darkMode = (window.localStorage.getItem('darkMode') === 'true');
    }

    protected checkTarget(target: string | JQuery<HTMLElement> = null): JQuery<HTMLElement> {

        if(target !== null && typeof target === 'string') {

            return $(target);
        }
        else if(target !== null && typeof target !== 'string') {

            return target;
        }

        return $('body');
    }

    protected determineUser(): void {
        const s = this;
        let meta: JQuery<HTMLElement> = $('#bronnendata');

        if(meta.length > 0) {

            let property = JSON.parse(window.atob(meta.data('property')));

            s.user = new User(property, s);
        }
        else {
            s.user = new User(null, s)
        }
    }

    //
    // protected eventSendMessage(): void {
    //
    //     const s = this;
    //
    //     let data = {
    //         user: s.user,
    //         content: s.input.val().toString()
    //     };
    //
    //     if(s.lastMessage !== null) {
    //
    //         data['question'] = s.lastMessage;
    //     }
    //
    //     let message: Message = new Message(data, {
    //         html: true
    //     });
    //
    //     s.addMessage(message);
    //     s.input.val('');
    //
    //     // Dummy response
    //     // s.dummyResponse();
    // }
}