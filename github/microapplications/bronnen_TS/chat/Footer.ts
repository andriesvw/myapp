/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 10/03/2024 22:24
 * Filename: Footer
 */

import {Message, messageDataType} from "./Message";
import {BsFa6 as BS} from "../bootstrap/BsFa6";
import {FontAwesome as Fa} from "../FontAwesome";
import {Card} from "./Card";
import {Chat} from "./Chat";
import {data} from "jquery";

/**
 * @class Footer
 */
export class Footer {

    private _card: Card;
    private _html: JQuery<HTMLElement>;
    private _stickyMessage: Message;
    private _stickyMessageCol: JQuery<HTMLElement>;
    private _textArea: JQuery<HTMLElement>;

    constructor(card: Card) {

        this.card = card;
        this.html = BS.footer('card-footer pt-1');

        this.init();
    }

    public get card(): Card {
        return this._card;
    }

    public set card(value: Card) {
        this._card = value;
    }

    public get chat(): Chat {
        return this.card.chat;
    }

    public get html(): JQuery<HTMLElement> {
        return this._html;
    }

    public set html(value: JQuery<HTMLElement>) {
        this._html = value;
    }

    public get stickyMessageCol(): JQuery<HTMLElement> {
        return this._stickyMessageCol;
    }

    public set stickyMessageCol(value: JQuery<HTMLElement>) {
        this._stickyMessageCol = value;
    }

    protected init(): void {

        const s = this;

        let row: JQuery<HTMLElement> = BS.row();
        row.append(s.getColInputArea());

        let rowSticky: JQuery<HTMLElement> = s.getStickyRow();

        s.html.append(rowSticky, row);
    }

    public getStickyRow(): JQuery<HTMLElement> {

        const s = this;
        let row: JQuery<HTMLElement> = BS.row('row row-sticky');
        s.stickyMessageCol = BS.col('col-12');

        row.append(s.stickyMessageCol);

        return row;
    }

    public setStickyMessage(): void {

        const s = this;
        let flexSticky: JQuery<HTMLElement> = BS.div('d-flex flex-row');
        if(s.stickyMessage.user === s.chat.assistent) {
            let p1: JQuery<HTMLElement> = BS.div('p-1')
            let icon = Fa.solid('fa-angles-down fa-beat')
                .css({
                    '--fa-animation-duration': '1.5s'
                })
            let btnScrollBottom = BS.span('border border-light p-1 rounded')
                .on('click', () => {
                    s.card.cardBody.html.animate({ scrollTop: s.card.cardBody.html.prop("scrollHeight")}, 500);
                })
                .append(
                    icon
                );

            setTimeout(() => {
                btnScrollBottom.empty().append(Fa.solid('fa-angles-down'));
            }, 4300)

            p1.append(btnScrollBottom)
            flexSticky.append(p1);
        }

        let content: JQuery<HTMLElement> = BS.div('p-1').append(s.stickyMessage.content);
        flexSticky.append(content);

        s.stickyMessageCol.empty().append(flexSticky);
    }

    public stickyShow(): void {
        this.stickyMessageCol.show(200);
    }

    public stickyHide(): void {
        this.stickyMessageCol.hide(200);
    }

    public get stickyMessage(): Message {
        return this._stickyMessage;
    }

    public set stickyMessage(message: Message) {

        const s = this;
        if(message.user === s.chat.assistent && message.type === 'question') {
            s._stickyMessage = message;
            s.setStickyMessage();
        }
    }

    protected get textArea(): JQuery<HTMLElement> {
        return this._textArea;
    }

    protected set textArea(value: JQuery<HTMLElement>) {
        this._textArea = value;
    }

    protected getColInputArea(): JQuery<HTMLElement> {

        const s = this;
        let maxHeight: number = 90;
        let minHeight: number = 30;
        let col: JQuery<HTMLElement> = BS.col('col-12');
        let flex: JQuery<HTMLElement> = BS.div('d-flex flex-row');
        let pText: JQuery<HTMLElement> = BS.div('pt-1 pb-2 flex-grow-1')

        s.textArea = BS
            .textarea('textarea-chat d-block')
            .on('input', (e): void => {

                s.chat.timer.start();
                if(s.textArea.val().toString().trim() === '') {

                    s.textArea
                        .css({height: `${minHeight}px`});
                }
                else {

                    let lines: number = s.textArea.val().toString().split(/\n/g).length;
                    let height: number = 30;

                    if(lines > 1 && s.textArea[0].scrollHeight >= minHeight && s.textArea[0].scrollHeight <= maxHeight) {
                        height = s.textArea[0].scrollHeight
                    }
                    else if(lines > 1 && s.textArea[0].scrollHeight > maxHeight) {
                        height = maxHeight;
                    }

                    s.textArea.css({
                        height: `${height.toString()}px`
                    });
                }
            })
            .on('keyup', (e: JQuery.KeyUpEvent<HTMLElement>): void => {

                if(s.isSubmit(e)) {

                    let val: string = s.textArea.val().toString();

                    s.resetTextArea(minHeight);
                    // s.textArea.val(s.textArea.val().toString().trim())

                    if(val.trim() !== '') {
                        s.sendMessage(val);
                    }


                }
            });

        let btn: JQuery<HTMLElement> = BS
            .span('btn border border-primary xbg-primary text-primary rounded span-chat-submit')
            .on('click', (): void => {

                let val: string = s.textArea.val().toString();

                s.resetTextArea(minHeight);

                if(val.trim() !== '') {
                    s.sendMessage(val);
                }
            })
            .append(Fa.solid('fa-arrow-right'));

        pText.append(s.textArea);
        // pBtn.append(btn);
        flex.append(pText, btn);
        col.append(flex);

        return col;
    }

    public resetTextArea(minHeight: number) {

        const s = this;
        s.textArea
            .val('')
            .css({height: `${minHeight.toString()}px`})
            .trigger('focus');
    }

    /**
     * Check key is Enter on different originalEvent-properties
     * @param {JQuery} e
     * @return {boolean}
     * @protected
     */
    protected isSubmit(e): boolean {

        return (
            !e.originalEvent.shiftKey
            && (
                e.originalEvent.code === 'Enter'
                || e.originalEvent.key === 'Enter'
                || e.originalEvent.keyCode === 13
                || e.originalEvent.which === 13
            )
        );
    }

    protected sendMessage(value: string):  void {

        const s = this;
        let data: messageDataType = {
            user: s.chat.user,
            content: value
        };

        let message: Message = new Message(data);

        s.chat.addMessage(message);
    }
}