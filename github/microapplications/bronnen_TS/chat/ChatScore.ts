/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 12/03/2024 09:20
 * Filename: ChatScore
 */

import {Chat} from "./Chat";
import {BsFa6 as BS, BsFa6} from "../bootstrap/BsFa6";
import {FontAwesome as Fa} from "../FontAwesome";

/**
 * @class ChatScore
 */
export class ChatScore {

    private _correct: number = 0;
    private _wrong: number = 0;
    private _total: number = 0;
    private _chat: Chat;
    private _options: any;
    private _spanCorrect: JQuery<HTMLElement>;
    private _spanWrong: JQuery<HTMLElement>;
    private _spanTotal: JQuery<HTMLElement>;
    private _html: JQuery<HTMLElement>;

    public constructor(chat: Chat, total: number = 0, options: any = {}) {

        this.spanCorrect = BsFa6.span().html('0');
        this.spanWrong = BsFa6.span().html('0');
        this.spanTotal = BsFa6.span().html('0');
        this.chat = chat;
        this.total = total;

        this.init();
    }

    public addCorrect(): void {
        const s = this;
        this.correct++;
        s.spanCorrect.html(s.correct.toString())
    }

    public addWrong(): void {
        const s = this;
        s.wrong++;
        s.spanWrong.html(s.wrong.toString())
    }

    protected get correct(): number {
        return this._correct;
    }

    protected set correct(value: number) {
        this._correct = value;
    }

    protected get wrong(): number {
        return this._wrong;
    }

    protected set wrong(value: number) {
        this._wrong = value;
    }

    public get total(): number {
        return this._total;
    }

    public set total(value: number) {

        const s = this;
        let separator2: JQuery<HTMLElement> = Fa.solid('fa-ellipsis-vertical');

        s._total = value;
        s.spanTotal.html(s.total.toString());

        // if(s.total > 0) {
        //
        //     s.html.append(
        //         separator2,
        //         s.getSpanTotal()
        //     )
        // }
    }

    protected get chat(): Chat {
        return this._chat;
    }

    protected set chat(value: Chat) {
        this._chat = value;
    }

    protected o(key) {
        if(this.options[key] === undefined) {
            return null;
        }

        return this.options[key];
    }

    protected get options(): any {
        return this._options;
    }

    protected set options(value: any) {
        this._options = value;
    }

    public get spanCorrect(): JQuery<HTMLElement> {
        return this._spanCorrect;
    }

    protected set spanCorrect(value: JQuery<HTMLElement>) {
        this._spanCorrect = value;
    }

    public get spanWrong(): JQuery<HTMLElement> {
        return this._spanWrong;
    }

    protected set spanWrong(value: JQuery<HTMLElement>) {
        this._spanWrong = value;
    }

    protected get spanTotal(): JQuery<HTMLElement> {
        return this._spanTotal;
    }

    protected set spanTotal(value: JQuery<HTMLElement>) {
        this._spanTotal = value;
    }

    protected getSpanCorrect(): JQuery<HTMLElement> {

        const s = this;
        let container: JQuery<HTMLElement> = BsFa6.span('text-success');
        let icon: JQuery<HTMLElement> = Fa.solid('fa-check');
        s.spanCorrect = BsFa6.span().html(s.correct.toString());

        container.append(icon, s.spanCorrect);

        return container;
    }

    protected getSpanWrong(): JQuery<HTMLElement> {

        const s = this;
        let container: JQuery<HTMLElement> = BsFa6.span('text-danger');
        let icon: JQuery<HTMLElement> = Fa.solid('fa-times');
        s.spanWrong = BsFa6.span().html(s.wrong.toString());

        container.append(s.spanWrong, icon);

        return container;
    }

    protected getSpanTotal(): JQuery<HTMLElement> {

        const s = this;
        let container: JQuery<HTMLElement> = BsFa6.span();
        // let icon: JQuery<HTMLElement> = Fa.solid('f');

        s.spanTotal = BsFa6.span('border border-light rounded px-1').html(s.total.toString());

        container.append(s.spanTotal);

        return container;
    }

    protected init(): void{
        const s = this;
        s.html = BsFa6.span();
        let separator1: JQuery<HTMLElement> = Fa.solid('fa-ellipsis-vertical');
        let separator2: JQuery<HTMLElement> = Fa.solid('fa-ellipsis-vertical');

        s.html.append(
            s.getSpanCorrect(),
            separator1,
            s.getSpanWrong()
        );

        // if(s.total > 0) {
        //
        //     s.html.append(
        //         separator2,
        //         s.getSpanTotal()
        //     )
        // }
    }

    public get html(): JQuery<HTMLElement> {
        return this._html;
    }

    public set html(value: JQuery<HTMLElement>) {
        this._html = value;
    }
}