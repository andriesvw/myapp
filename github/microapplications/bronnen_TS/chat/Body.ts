/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 10/03/2024 22:24
 * Filename: Body
 */

import {BsFa6 as BS} from "../bootstrap/BsFa6";
import {FontAwesome as Fa} from "../FontAwesome";
import {Card} from "./Card";
import {Chat} from "./Chat";
import {Message} from "./Message";

/**
 * @class Body
 */
export class Body {

    private _card: Card;
    private _html: JQuery<HTMLElement>;
    private _waiting: JQuery<HTMLElement>;
    private _atBottom: boolean = false;

    constructor(card: Card) {

        this.card = card;
        this.html = BS.body('card-body card-body-chat');

        this.init();
    }

    protected init(): void {

        const s = this;
        s.waiting = BS.div('d-flex flex-row justify-content-center');
        s.waiting.append(s.card.waiting);

        s.html.append(s.waiting);
        s.setScroll();
    }

    public get card(): Card {
        return this._card;
    }

    public set card(value: Card) {
        this._card = value;
    }

    public get chat(): Chat {
        return this.card.chat;
    }

    public get html(): JQuery<HTMLElement> {
        return this._html;
    }

    public set html(value: JQuery<HTMLElement>) {
        this._html = value;
    }

    public get waiting(): JQuery<HTMLElement> {
        return this._waiting;
    }

    public set waiting(value: JQuery<HTMLElement>) {
        this._waiting = value;
    }

    public appendMessage(message: Message): void {

        const s = this;
        s.waiting.remove();
        s.html.append(message.html)

        s.card.cardFooter.stickyMessage = message;

        if(s.atBottom) {

            s.html[0].scrollTop = s.html[0].scrollHeight;
        }
        else {

            if(message.user === s.chat.user) {

                s.card.cardBody.html.animate({ scrollTop: s.card.cardBody.html.prop("scrollHeight")}, 250);
            }
        }
    }
    public get atBottom(): boolean {
            return this._atBottom;
        }

    public set atBottom(value: boolean) {
        this._atBottom = value;

        if(this.atBottom) {
            this.card.cardFooter.stickyHide()
        }
        else {
            this.card.cardFooter.stickyShow()
        }
    }

    protected setScroll(): void {

        const s = this;
        s.html.on('scroll', () => {

            if (s.html[0].scrollHeight - s.html.scrollTop() < (s.html.outerHeight() + 55)) {

                s.atBottom = true;
            }
            else {
                s.atBottom = false;
            }

            // s.setLastMessage();
        });

        $(window).on('resize', (): void => {

            if (s.html[0].scrollHeight - s.html.scrollTop() < (s.html.outerHeight() + 55)) {

                s.atBottom = true;
            }
            else {
                s.atBottom = false;
            }
            console.log(s.atBottom)
            // s.setLastMessage();
        })
    }

    // public get lastMessage(): Message {
    //     return this._lastMessage;
    // }
    //
    // public showLastMessage(): void {
    //     this.rowLastMessage.show();
    // }
    //
    // public hideLastMessage(): void {
    //     this.rowLastMessage.hide();
    // }
    //
    // public setLastMessage(): void {
    //     const s = this;
    //
    //     if(s.atBottom) {
    //         s.hideLastMessage()
    //     }
    //     else {
    //         s.showLastMessage()
    //     }
    // }
}