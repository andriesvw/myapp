/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 11/03/2024 22:03
 * Filename: AppTimer
 */

import {BsFa6} from "../bootstrap/BsFa6";
import {Chat} from "./Chat";

/**
 * @class AppTimer
 */
export class ChatTimer {

    private _startTime: number;
    private _stopTime: number;
    private _spanMinutes: JQuery<HTMLElement>;
    private _spanSeconds: JQuery<HTMLElement>;
    private _interval: NodeJS.Timer
    private _started: boolean = false;
    private _stopped: boolean = true;
    private _chat: Chat;

    public constructor(chat: Chat) {

        this.spanMinutes = BsFa6.span().html('00');
        this.spanSeconds = BsFa6.span().html('00');
        this.chat = chat;
    }

    public get html(): JQuery<HTMLElement> {

        const s = this;
        let separator: JQuery<HTMLElement> = BsFa6.span().html(':');
        let container: JQuery<HTMLElement> = BsFa6.span();

        container.append(s.spanMinutes, separator, s.spanSeconds);
        return container;
    }

    public start(): void {

        const s = this;

        if(!s.started) {

            s.startTime = Date.now();
            s.interval = setInterval((): void => {

                s.update();
            }, 1000);

            s.started = true;
            s.stopped = false;
        }
    }

    public stop() {

        const s = this;
        s.stopTime = Date.now();
        s.stopped = true;
    }

    protected get startTime(): number {
        return this._startTime;
    }

    protected set startTime(value: number) {
        this._startTime = value;
    }

    protected get stopTime(): number {
        return this._stopTime;
    }

    protected set stopTime(value: number) {
        this._stopTime = value;
    }

    protected update(): void {

        const s = this;
        let sec: number = Math.round(Math.floor((Date.now() - s.startTime) / 1000));

        if(s.stopped) {

            sec = Math.round(Math.floor((s.stopTime - s.startTime) / 1000));
        }

        let minutes: number = Math.round(Math.floor(sec / 60));
        let seconds: number = Math.round(Math.floor(sec % 60));

        s.setMin((minutes < 10) ? '0' + minutes.toString() : minutes.toString());
        s.setSec((seconds < 10) ? '0' + seconds.toString() : seconds.toString());
    }

    protected setMin(value: string):void {

        this.spanMinutes.html(value);

    }

    protected setSec(value: string): void {

        this.spanSeconds.html(value);
    }

    protected get spanMinutes(): JQuery<HTMLElement> {
        return this._spanMinutes;
    }

    protected set spanMinutes(value: JQuery<HTMLElement>) {
        this._spanMinutes = value;
    }

    protected get spanSeconds(): JQuery<HTMLElement> {
        return this._spanSeconds;
    }

    protected set spanSeconds(value: JQuery<HTMLElement>) {
        this._spanSeconds = value;
    }

    protected get interval(): NodeJS.Timer {
        return this._interval;
    }

    protected set interval(value: NodeJS.Timer) {
        this._interval = value;
    }

    private get started(): boolean {
        return this._started;
    }

    private set started(value: boolean) {
        this._started = value;
    }

    private get stopped(): boolean {
        return this._stopped;
    }

    private set stopped(value: boolean) {
        this._stopped = value;
    }

    protected get chat(): Chat {
        return this._chat;
    }

    protected set chat(value: Chat) {
        this._chat = value;
    }
}