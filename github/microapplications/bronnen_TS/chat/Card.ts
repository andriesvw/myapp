/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 10/03/2024 22:30
 * Filename: Card
 */

import { BsFa6 as BS} from "../bootstrap/BsFa6";
import {Header} from "./Header";
import {Body} from "./Body";
import {Footer} from "./Footer";
import {Chat} from "./Chat";
import {FontAwesome as Fa} from "../FontAwesome";

/**
 * @class Card
 */
export class Card {

    private _chat: Chat;
    private _html: JQuery<HTMLElement>;
    private _cardHeader: Header;
    private _cardBody: Body;
    private _cardFooter: Footer;

    constructor(chat: Chat) {

        const s = this;
        this.chat = chat;
        this.html = BS.card('card card-chat');
        this.cardHeader = new Header(s)
        this.cardBody = new Body(s);
        this.cardFooter = new Footer(s);

        this.init();
    }

    public get chat(): Chat {
        return this._chat;
    }

    public set chat(value: Chat) {
        this._chat = value;
    }

    public get html(): JQuery<HTMLElement> {
        return this._html;
    }

    public set html(value: JQuery<HTMLElement>) {
        this._html = value;
    }

    public get cardHeader(): Header {
        return this._cardHeader;
    }

    public set cardHeader(value: Header) {
        this._cardHeader = value;
    }

    public get cardBody(): Body {
        return this._cardBody;
    }

    public set cardBody(value: Body) {
        this._cardBody = value;
    }

    public get cardFooter(): Footer {
        return this._cardFooter;
    }

    public set cardFooter(value: Footer) {
        this._cardFooter = value;
    }

    protected init(): void {

        const s = this;

        s.html.append(
            s.cardHeader.html,
            s.cardBody.html,
            s.cardFooter.html
        );
    }

    public waiting(): JQuery<HTMLElement> {

        let span: JQuery<HTMLElement> = BS.span('p-2')
        span.append(
            Fa.solid('fa-circle fa-2xs fa-beat me-1')
        );
        setTimeout(()=>{
            span.append(Fa.regular('fa-circle fa-2xs fa-beat me-1'))
        }, ((1/6)*1000))
        setTimeout(()=>{
            span.append(Fa.solid('fa-circle fa-2xs fa-beat'))
        }, ((2/6)*1000));

        return span;
    }
}