/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 10/03/2024 22:23
 * Filename: Header
 */

import { BsFa6 as BS } from "../bootstrap/BsFa6";
import {FontAwesome as Fa} from "../FontAwesome";
import {Card} from "./Card";
import {Chat} from "./Chat";
import $ from "jquery";

/**
 * @class Header
 */
export class Header {

    private _card: Card;
    private _html: JQuery<HTMLElement>;
    private _row: JQuery<HTMLElement>;

    private _btnAnchor: JQuery<HTMLElement>;
    private _btnMode: JQuery<HTMLElement>;
    private _btnBack: JQuery<HTMLElement>;
    private _btnReload: JQuery<HTMLElement>;

    constructor(card: Card) {

        this.card = card;
        this.html = BS.header();

        this.init();
    }

    public get card(): Card {
        return this._card;
    }

    public set card(value: Card) {
        this._card = value;
    }

    public get chat(): Chat {
        return this.card.chat;
    }

    public get html(): JQuery<HTMLElement> {
        return this._html;
    }

    public set html(value: JQuery<HTMLElement>) {
        this._html = value;
    }

    public get row(): JQuery<HTMLElement> {
        return this._row;
    }

    public set row(value: JQuery<HTMLElement>) {
        this._row = value;
    }

    public get btnAnchor(): JQuery<HTMLElement> {
        return this._btnAnchor;
    }

    public set btnAnchor(value: JQuery<HTMLElement>) {
        this._btnAnchor = value;
    }

    public get btnMode(): JQuery<HTMLElement> {
        return this._btnMode;
    }

    public set btnMode(value: JQuery<HTMLElement>) {
        this._btnMode = value;
    }

    public get btnBack(): JQuery<HTMLElement> {
        return this._btnBack;
    }

    public set btnBack(value: JQuery<HTMLElement>) {
        this._btnBack = value;
    }

    public get btnReload(): JQuery<HTMLElement> {
        return this._btnReload;
    }

    public set btnReload(value: JQuery<HTMLElement>) {
        this._btnReload = value;
    }

    public init(){

        const s = this;

        s.row = BS.row();
        let colAppControls: JQuery<HTMLElement> = s.getColAppControls();
        let colScore: JQuery<HTMLElement> = s.getColScore();
        let colTimer: JQuery<HTMLElement> = s.getColTimer();
        let colNavigation: JQuery<HTMLElement> = s.getColNavigation();
        let colCenter: JQuery<HTMLElement> = s.getCenterCol(colAppControls, colNavigation);

        s.row.append(colAppControls, colScore, colCenter, colTimer, colNavigation);
        s.html.append(s.row);
    }

    public getColAppControls(): JQuery<HTMLElement> {

        const s = this;
        let order: string = 'order-1 order-xs-1 order-sm-1 order-md-1 order-lg-1';
        let col: JQuery<HTMLElement> = BS.col(`col-6 col-xs-6 col-sm-2 col-md-2 col-lg-2 order-1 ${order}`);
        col.append(s.getBtnsGroupAppControls());
        return col;
    }

    public getColScore(): JQuery<HTMLElement> {

        const s = this;
        let order: string = 'order-3 order-xs-3 order-sm-2 order-md-2 order-lg-2';
        let col: JQuery<HTMLElement> = BS.col(`col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 order-2 order-md-3 ${order}`);
        s.chat.score.total = 11;
        col.append(s.chat.score.html);
        return col;
    }

    public getCenterCol(colAppControls, colNavigation): JQuery<HTMLElement> {

        const s = this;
        let order: string = 'order-4 order-xs-4 order-sm-3 order-md-3 order-lg-3';
        let col: JQuery<HTMLElement> = BS.col(`col-4 col-xs-4 col-sm-4 col-md-4 col-lg-2 text-center d-sm-none ${order}`);
        col.append(s.getBtnsCenterGroup(colAppControls, colNavigation));
        return col;
    }

    public getBtnsCenterGroup(colAppControls, colNavigation): JQuery<HTMLElement> {

        const s = this;

        let group: JQuery<HTMLElement> = BS.btnGrp('btn-group-sm');
        let show = true;
        let btn: JQuery<HTMLElement> = BS
            .span('btn border-0 rounded px-1')
            .on('click', (): void => {
                show = !show;

                if(!show) {
                    colAppControls.hide();
                    colNavigation.hide();
                    btn.empty().append(Fa.solid('fa-chevron-down fa-fw'));
                }
                else {
                    colAppControls.show();
                    colNavigation.show();
                    btn.empty().append(Fa.solid('fa-chevron-up fa-fw'));
                }
            })
            .append(Fa.solid('fa-chevron-up fa-fw'));


        group.append(btn);

        return group;
    }

    public getColNavigation(): JQuery<HTMLElement> {

        const s = this;
        let order: string = 'order-2 order-xs-2 order-sm-5 order-md-5 order-lg-5';
        let col: JQuery<HTMLElement> = BS.col(`col-6 col-xs-6 col-sm-2 col-md-2 col-lg-2 text-end ${order}`);
        col.append(s.getBtnsGroupNavigation());
        return col;
    }

    public getColTimer(): JQuery<HTMLElement> {

        const s = this;
        let order: string = 'order-5 order-xs-5 order-sm-4 order-md-4 order-lg-4';
        let col: JQuery<HTMLElement> = BS.col(`col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 text-end ${order}`);
        col.append(s.chat.timer.html);
        return col;
    }

    public getBtnsGroupAppControls(): JQuery<HTMLElement> {

        const s = this;


        let group: JQuery<HTMLElement> = BS.btnGrp('btn-group-sm');

        s.btnAnchor = BS.span('btn border-0 rounded rounded-end-0 px-1')
            // .on('click', (): void => {
            //     let an: JQuery<HTMLElement> = $(`[name="${anchor}"]`);
            //     an[0].scrollIntoView({
            //         behavior: "smooth", // or "auto" or "instant"
            //         block: "start" // or "end"
            //     });
            // })
            .append(Fa.solid('fa-anchor fa-fw'));

        s.anchorDisable();
        s.btnMode = BS.span('btn border-0 rounded rounded-start-0 px-1')
            .on('click', (): void => {

                s.chat.darkMode = !s.chat.darkMode;
            })
            .append(Fa.solid('fa-circle-half-stroke fa-fw'));

        group.append(s.btnAnchor, s.btnMode);

        return group;
    }

    public getBtnsGroupNavigation(): JQuery<HTMLElement> {

        const s = this;

        let group: JQuery<HTMLElement> = BS.btnGrp('btn-group-sm');

        s.btnBack = BS.span('btn border-0 rounded rounded-end-0 px-1')
            .on('click', (): void => {
                history.go(-1);
            })
            .append(Fa.solid('fa-arrow-left fa-fw'));

        s.btnReload = BS.span('btn border-0 rounded rounded-start-0 px-1')
            .on('click', (): void => {

                window.location.reload();
            })
            .append(Fa.solid('fa-rotate-right fa-fw'));

        group.append(s.btnBack, s.btnReload);

        return group;
    }

    public anchorEnable() {

        const s = this;
        let anchor: string = 'mainAnchor';
        s.btnAnchor
            .removeClass('disabled opacity-25')
            .removeAttr('disabled style')
            .on('click', (): void => {

                let an: JQuery<HTMLElement> = $(`[name="${anchor}"]`);
                an[0].scrollIntoView({
                    behavior: "smooth", // or "auto" or "instant"
                    block: "start" // or "end"
                });
            });
    }

    public anchorDisable() {

        const s = this;
        s.btnAnchor
            .addClass('disabled opacity-25')
            .attr('disabled', 'disabled')
            .off('click');
    }

}