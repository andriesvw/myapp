/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 07/03/2024 12:04
 * Filename: Message
 */

import {User} from "./User";
import {BsFa6 as BS} from "../bootstrap/BsFa6";
require('./css/message.css');
import {FontAwesome as Fa} from "../FontAwesome";
import {Chat} from "./Chat";
import $ from "jquery";
import {v4 as uuidv4} from 'uuid';
import Popover from 'bootstrap5/js/src/popover';


export type messageDataType = {
    user: User,
    type?: string,
    content: string | JQuery<HTMLElement>,
    question?: Message,
    correct?: boolean,
    uuid?: string,
    time?: number,
    callback?: any;
};
export type messageOptionsType = {
    width?: string,
    html?: boolean,
    align?: string,
    anchor?: string,
    color?: string,
    text?: string,
    size?: string,
    header?: string,
    icon?: boolean,
    iconType?: string | JQuery<HTMLElement>,
    offset?: string,
    alert?: string,
    lastMessage?: boolean
};
/**
 * @class Message
 */
export class Message {

    private _chat: Chat;
    private _data: messageDataType;
    private _options: messageOptionsType;
    private _flex: JQuery<HTMLElement>;
    private _alert: JQuery<HTMLElement>;
    private _alerts: JQuery<HTMLElement>[] = [];

    constructor(data: messageDataType, options: messageOptionsType = {}) {

        this.data = data;
        this.time = Date.now();
        this.uuid = uuidv4();
        this.options = options;

        this.color = 'primary text-dark';

        if(this.user.isAssistent() || this.user.isApplication()) {
            this.color = 'light';
        }


    }

    protected get uuid(): string {
        return this.data.uuid
    }

    protected set uuid(value: string) {
        this.data.uuid = value
    }

    protected get time(): number {
        return this.data.time
    }

    protected set time(value: number) {
        this.data.time = value
    }

    public get chat(): Chat {
        return this._chat;
    }

    public set chat(value: Chat) {
        this._chat = value;
    }

    protected o(key): any {
        return this.options[key];
    }

    public get data(): messageDataType {
        return this._data;
    }

    public set data(value: messageDataType) {
        this._data = value;
    }

    public get type(): string {

        if(this.data.type === undefined) {
            this.data.type = 'message';
        }

        return this.data.type;
    }

    public set type(value: string) {
        this.data.type = value;
    }

    public get user(): User {
        return this.data.user;
    }

    public set user(value: User) {
        this.data.user = value;
    }

    public get question(): Message {
        return this.data.question;
    }

    public set question(value: Message) {
        this.data.question = value;
    }

    public get content(): string | JQuery<HTMLElement> {
        return this.data.content;
    }

    public set content(value: string | JQuery<HTMLElement>) {
        this.data.content = value;
    }

    public get options(): messageOptionsType {
        return this._options;
    }

    public set options(value: messageOptionsType) {
        this._options = value;
    }

    public get lastMessage(): boolean {

        if(this.options.lastMessage === undefined) {
            this.options['lastMessage'] = false;
        }

        return this.options.lastMessage;
    }

    public get correct(): boolean {
        return this.data.correct;
    }

    public set correct(value: boolean) {

        const s = this;
        s.data.correct = value;

        let alert: JQuery<HTMLElement> = BS.span('alert py-1 px-1 mb-0 flex-message shadow-sm').append(Fa.solid('fa-check'));
        s.addAlert(alert);

        if(s.correct) {
            s.setAlert('success');
        }
        else {
            alert.empty().append(Fa.solid('fa-times'));
            s.setAlert('danger');
        }

        s.flex.append(alert);
    }

    public set lastMessage(value: boolean) {

        this.options.lastMessage = value;
    }

    public get color(): string {

        if(this.options.color === undefined) {
            this.options['color'] = 'light';
        }

        return this.options.color;
    }

    public set color(value: string) {
        this.options.color = value;
    }

    public get text(): string {

        if(this.options.text === undefined) {
            this.options['text'] = '';
        }

        return this.options.text;
    }

    public set text(value: string) {
        this.options.text = value;
    }

    public get flex(): JQuery<HTMLElement> {
        return this._flex;
    }

    public set flex(value: JQuery<HTMLElement>) {
        this._flex = value;
    }

    public get alert(): JQuery<HTMLElement> {
        return this._alert;
    }

    public set alert(value: JQuery<HTMLElement>) {
        this._alert = value;
    }

    public get alerts(): JQuery<HTMLElement>[] {
        return this._alerts;
    }

    public set alerts(value: JQuery<HTMLElement>[]) {
        this._alerts = value;
    }

    public get anchor(): string {

        if(this.options.anchor === undefined) {
            this.options['anchor'] = '';
        }

        return this.options.anchor;
    }

    public set anchor(value: string) {
        this.options.anchor = value;
    }

    public get offset(): string {

        if(this.options.offset === undefined) {
            this.options['offset'] = '';
        }

        return this.options.offset;
    }

    public set offset(value: string) {
        this.options.offset = value;
    }

    public get width(): string {

        if(this.options.width === undefined) {
            this.options['width'] = 'col-xs-12 col-sm-10 col-md-10';
        }

        return this.options.width;
    }

    public set width(value: string) {
        this.options.width = value;
    }

    public get header(): string {

        if(this.options.header === undefined) {
            this.options['header'] = '';
        }

        return this.options.header;
    }

    public set header(value: string) {
        this.options.header = value;
    }

    public get size(): string {

        if(this.options.size === undefined) {
            this.options['size'] = '1.0rem';
        }

        return this.options.size;
    }

    public set size(value: string) {
        this.options.size = value;
    }

    public get icon(): boolean {

        if(this.options.icon === undefined) {
            this.options['icon'] = true;
        }

        return this.options.icon;
    }

    public set icon(value: boolean) {
        this.options.icon = value;
    }

    public get iconType(): string | JQuery<HTMLElement> {

        if(this.options.iconType === undefined) {
            this.options['iconType'] = this.user.avatar;
        }

        return this.options.iconType;
    }

    public set iconType(value: string | JQuery<HTMLElement>) {
        this.options.iconType = value;
    }

    protected addAlert(alert: JQuery<HTMLElement>): void {

        const s = this;
        s.alerts.push(alert);

        let len: number = s.alerts.length;
        for(let i = 0; i < len; i++) {

            if(len > 1 && i === 0) {
                s.alerts[i].addClass('rounded-end-0');
            }
            else if(len > 1 && i === (len - 1)) {
                s.alerts[i].addClass('rounded-start-0');
            }
            else if (len > 1) {
                s.alerts[i].addClass('rounded-0');
            }
        }
    }

    protected getRowAnchor() {

        const s = this;
        if(s.anchor === '') {
            return null;
        }

        let row: JQuery<HTMLElement> = BS.row();
        let col: JQuery<HTMLElement> = BS.col('col-12 text-end');
        let span: JQuery<HTMLElement> = BS.span('', {name: s.anchor}).append(Fa.solid('fa-anchor fa-2xs'));

        col.append(span);
        row.append(col);

        s.chat.card.cardHeader.anchorEnable();

        return row;
    }

    public getContent(): JQuery<HTMLElement> {

        const s = this;
        // let color: string = 'primary text-dark';
        //
        // if(s.user.isAssistent() || s.user.isApplication()) {
        //     color = 'light';
        // }

        s.alert = BS.span(`alert alert-${s.color} py-1 px-2 mb-0 flex-message shadow-sm`);

        s.alert.append(s.getRowAnchor());

        if(s.type === 'title') {

            s.alert
                .addClass('flex-fill bg-primary text-white h3 py-2')
                .removeClass('py-1');
        }

        s.addAlert(s.alert);

        if(typeof s.content === 'string') {

            let row: JQuery<HTMLElement> = BS.row();
            let content: string[] = s.content.split(/\n/g);
            content.forEach((line: string) => {

                let col: JQuery<HTMLElement> = BS.col('col-12').append(line);
                row.append(col);
            });
            s.alert.append(row);
        }
        else {

            s.alert.append(s.content);
        }

        let po = new Popover(s.alert, {
            content: s.user.name,
            placement: 'bottom',
            trigger: 'hover'
        });

        return s.alert;
    }

    public removeAlerts(alerts: string[]): void {

        this.alerts.forEach((alert: JQuery<HTMLElement>) => {
            alert.removeClass(`alert-${alerts.join(' alert-')}`)
        });
    }

    public setAlert(color: string = ''): void {

        const s = this;
        let alerts: string[] = [
            'light',
            'dark',
            'primary',
            'danger',
            'success',
            'warning',
            'info',
            'secondary'
        ];

        if(alerts.indexOf(color) > -1 && color.indexOf('alert-') === -1) {
            color = `alert-${color}`
        }

        s.removeAlerts(alerts);

        s.alerts.forEach((alert: JQuery<HTMLElement>) => {
            alert.addClass(color);
        })
    }

    public get html(): JQuery<HTMLElement> {

        const s = this;
        
        s.flex = BS.div('d-flex flex-row mb-2');
        let spanIcon: JQuery<HTMLElement> = BS.span('py-1');

        if(s.o('align') === 'end') {
            spanIcon.addClass('flex-grow-1')
        }

        if(s.icon) {

            spanIcon
                .addClass('px-1')
                .append(s.user.icon);
        }

        s.flex.append(spanIcon, s.getContent());

        return s.flex;
    }
}