/*
 * FC-Sprint2 project => score.js
 *
 * The project "diglin.eu" is property of FC-Sprint2
 *
 * Created at:
 * 30-nov-2016 @ 9:49:36
 *
 * Created by:
 * Andries van Weeren
 * a.weeren@fcroc.nl
 */


/**
 *  let s = new SCORE([ID]/[CLASS]);
 *  (object) target is the element on which the score will be displayed
 */

export interface ScoreType {

    get right(): number;
    set right(amount: number);
    get wrong(): number;
    set wrong(amount: number);
    addRight();
    addWrong();
    reset(right:number, wrong: number);
    playRight();
    playWrong();
}
