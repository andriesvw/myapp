import {MediaType} from "../types/MediaType";

export interface Application {

    start(): void;
}