/**
 *  let t = new TIMER([ID]/[CLASS]);
 *  (object) target is the element on which the timer will be displayed
 */
import {Progress} from '../../progress/Progress'

export interface TimerType {

    /**
     * @return {null|Progress}
     */
    get progress(): Progress

    /**
     * @param {Progress} progress
     */
    set progress(progress: Progress)

    get timer()

    set timer(timer)

    /**
     *
     * @return {number}
     */
    get sec(): number

    /**
     *
     * @param {number} sec
     */
    set sec(sec: number)

    /**
     *
     * @return {boolean}
     */
    get isstopped(): boolean

    /**
     *
     * @param {boolean} stopped
     */
    set isstopped(stopped: boolean)

    /**
     *
     * @return {boolean}
     */
    get isMobile(): boolean

    /**
     *
     * @param {boolean} isMobile
     */
    set isMobile(isMobile: boolean)

    start(): void

    stop(): void

    /**
     *  Stops TIMER and reinitiates it
     */
    reset(): void

    /**
     * returns (string) with current min and sec
     */
    getTime(): string
}
