/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 22/09/2023 14:15
 * Filename: BulkInput
 */

// import bootbox from "bootbox5";
import bootbox, {confirm} from "bootbox";
import $ from "jquery";


/**
 * @class BulkInput
 */
export class BulkInput {

    private _bb: JQuery<HTMLElement>;
    private _data: string[]|number[] = [];
    private _splitter: string = '\n';
    private _flags: string = 'ig';
    private _textarea: JQuery<HTMLElement>;

    /**
     * @return {JQuery<HTMLElement>}
     */
    public get bb(): JQuery<HTMLElement> {
        return this._bb;
    }

    /**
     * @param {JQuery<HTMLElement>} value
     */
    public set bb(value: JQuery<HTMLElement>) {
        this._bb = value;
    }

    /**
     * @return {string[] | number[]}
     */
    public get data(): string[] | number[] {
        return this._data;
    }

    /**
     * @param {string[] | number[]} value
     */
    public set data(value: string[] | number[]) {
        this._data = value;
    }

    /**
     * @return {string}
     */
    public get splitter(): string {
        return this._splitter;
    }

    /**
     * @param {string} value
     */
    public set splitter(value: string) {
        this._splitter = value;
    }

    /**
     * @return {string}
     */
    public get flags(): string {
        return this._flags;
    }

    /**
     * @param {string} value
     */
    public set flags(value: string) {
        this._flags = value;
    }

    /**
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected get textarea(): JQuery<HTMLElement> {
        return this._textarea;
    }

    /**
     * @param {JQuery<HTMLElement>} value
     * @protected
     */
    protected set textarea(value: JQuery<HTMLElement>) {
        this._textarea = value;
    }

    /**
     * @param {() => void} handleBulk
     */
    public init(handleBulk = () => { }): void {

        let s = this;
        s.textarea = $('<textarea/>', {
            class: 'form-control',
            cols: '10',
            rows: '20'
        }).text(s.data.join(s.splitter));
        let re: RegExp = new RegExp(s.splitter, s.flags);
        let bb: JQuery<HTMLElement> = bootbox.confirm({
            title: '<h3><i class="fa-solid fa-database"></i>',
            message: s.textarea,
            size: 'large',
            buttons: {
                confirm: {
                    label: '<i class="fa-solid fa-fw fa-check"></i>',
                    className: 'btn-outline-primary'
                },
                cancel: {
                    label: '<i class="fa-solid fa-fw fa-ban"></i>',
                    className: 'btn-outline-secondary'
                }
            },
            callback: (confirm: boolean): void => {

                if(confirm) {

                    s.data = s.textarea.val().toString().trim().split(re)
                    handleBulk();
                }
            }
        });
    }
}