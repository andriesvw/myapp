// import type {AppBlockType} from "./types/AppBlockType";
import {BsFa6 as Bs} from './bootstrap/BsFa6';
import bootbox from 'bootbox5';

/**
 * @class AppBlock
 */
export class AppBlock {

    /*
     * Class Properties
     */
    private _index: number;
    private _data: any;
    private _content: number|string;
    private _audio: string;
    private _image: string;
    private _parent: any;

    /*
     * Class JQuery Properties
     */
    private _inputGrpContent: JQuery;
    private _inputGrpAudio: JQuery;
    private _inputGrpImage: JQuery;

    /**
     *
     * @param {any} data
     * @param {number} index
     * @param {any} parent
     */
    constructor(parent, index: number, data: any) {

        this.parent = parent;
        this.index = index;
        this.data = data;

        this.init();
    }

    public get data(){

        let s = this;

        return s._data;
    }

    public set data(data:any) {

        this._data = data;
    }

    /**
     *
     * @param {dataType} data
     * @return {void}
     * @protected
     */
    protected init(): void {

        let s = this;

        if(s.data.content !== undefined) {
            s.content = s.data.content;
        }
        if(s.data.audio !== undefined) {
            s.audio = ((s.data.audio.toLowerCase().indexOf('https') === 0) ? s.parent.stripUrl(s.data.audio) : s.data.audio);
        }
        if(s.data.image !== undefined) {
            s.image = ((s.data.image.toLowerCase().indexOf('https') === 0) ? s.parent.stripUrl(s.data.image) : s.data.image);
        }
    }

    /**
     * @public
     */
    public initCmsGroups(): void {

        let s = this;

        s.initContentGroup();
        s.initAudioGroup();
        s.initImageGroup();
    }

    /**
     * @return {number}
     */
    public get index():number {
        return this._index;
    }

    /**
     * @param {number} index
     */
    public set index(index: number) {
        this._index = index;
    }

    public setIndex(index: number) {
        this._index = index;
    }

    /**
     *
     * @return {any}
     * @protected
     */
    public get parent(): any{
        return this._parent;
    }

    /**
     *
     * @param {any} parent
     * @protected
     */
    public set parent(parent: any) {
        this._parent = parent;
    }

    /**
     *
     * @return {number | string}
     * @public
     */
    public get content(): number | string {
        return this._content;
    }

    /**
     *
     * @param {number | string} content
     * @public
     */
    public set content(content: number | string) {
        this._content = content;
    }

    /**
     *
     * @return {string}
     * @public
     */
    public get audio(): string {
        return this._audio;
    }

    /**
     *
     * @param {string} value
     * @public
     */
    public set audio(value: string) {
        this._audio = value;
    }

    /**
     *
     * @return {string}
     * @public
     */
    public get image(): string {
        return this._image;
    }

    /**
     *
     * @param {string} value
     * @public
     */
    public set image(value: string) {
        this._image = value;
    }

    /**
     *
     * @return {JQuery}
     */
    public get inputGrpContent(): JQuery {
        return this._inputGrpContent;
    }

    /**
     *
     * @param {JQuery} grp
     */
    public set inputGrpContent(grp: JQuery) {
        this._inputGrpContent = grp;
    }

    /**
     *
     * @return {JQuery}
     */
    public get inputGrpAudio(): JQuery {
        return this._inputGrpAudio;
    }

    /**
     *
     * @param {JQuery} grp
     */
    public set inputGrpAudio(grp: JQuery) {
        this._inputGrpAudio = grp;
    }

    /**
     *
     * @return {JQuery}
     */
    public get inputGrpImage(): JQuery {
        return this._inputGrpImage;
    }

    /**
     *
     * @param {JQuery} grp
     */
    public set inputGrpImage(grp: JQuery) {
        this._inputGrpImage = grp;
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    public initContentGroup(): JQuery {

        let s = this;
        s.inputGrpContent = Bs.inpGrp('input-group-content');
        let input = Bs.inp('input-content', {
            placeholder: 'Content',
            value: s.content
        }, {event: 'blur', callback: (e) => {

                let value = e.target.value.trim();

                if(value !== '') {

                    s.content = value;
                    s.audio = s.parent.findAudio(value);
                    s.image = s.parent.findImage(value);
                    s.inputGrpAudio.find('input.input-audio').val(s.audio);
                    s.inputGrpImage.find('input.input-image').val(s.image);
                }

                s.toggleActiveStataButton(s.inputGrpAudio.find('.btn-audio'), s.audio);
                s.toggleActiveStataButton(s.inputGrpImage.find('.btn-image'), s.image);
            }});

        s.inputGrpContent.append(input);

        return s.inputGrpContent;
    }

    /**
     *
     * @return {string}
     * @protected
     */
    protected getImageUrl(): string {

        let s = this;
        return s.parent.mediaUrl + '/media/' + s.parent.language + '/img/' + s.image;
    }

    /**
     *
     * @return {string}
     * @protected
     */
    protected getAudioUrl(): string {

        let s = this;
        return s.parent.mediaUrl + '/media/' + s.parent.language + '/audio/' + s.audio;
    }

    /**
     *
     * @return {void}
     */
    protected initAudioGroup(): void {

        let s = this;
        s.inputGrpAudio = Bs.inpGrp('input-group-audio');
        let button = Bs.btn('btn-outline-secondary disabled btn-audio', {disabled: 'disabled'}, {event: 'click', callback: (e) => {

            if(!$(e.target).hasClass('disabled') && s.audio !== '') {

                /**
                 * @type {HTMLAudioElement} audio
                 */
                let audio = new Audio();
                audio.src = s.getAudioUrl();
                audio.play();
            }
        }}).append(Bs.fas('fa-play fa-fw'));
        let grpAppend = Bs.inpGrpApp().append(button);
        let input = Bs.inp('input-audio', {
            placeholder: 'Audio',
            value: s.audio
        }, {event: 'blur', callback: (e) => {

            let value = e.target.value.trim();

            if(value !== '') {

                s.audio = e.target.value = s.parent.findAudio(value);
            }
            else {
                s.audio = value;
            }

            s.toggleActiveStataButton(button, s.audio);
        }});

        s.parent.makeAutoComplete(input, s.parent.media.audio.files, (event, ui) => {

            s.audio = ui.item.value;
        });

        s.toggleActiveStataButton(button, s.audio);

        s.inputGrpAudio.append(input, button);
    }

    /**
     *
     * @param {HTMLImageElement} image
     * @protected
     */
    protected previewImage(image: HTMLImageElement): void{

        let row = Bs.row('row-bootbox-image');
        let col = Bs.col('col text-center col-bootbox-image');
        col.append(image);
        row.append(col);

        bootbox.alert({
            size: 'large',
            title: '<h6>' + image.src + '</h6>',
            message: row,
            backdrop: true,
            onEscape: true,
            buttons: {
                ok: {
                    label: '<i class="fa-solid fa-check fa-fw"></i>',
                    className: 'btn-outline-primary'
                }
            }
        });
    }

    /**
     *
     * @return {void}
     */
    protected initImageGroup(): void {

        let s = this;
        s.inputGrpImage = Bs.inpGrp('input-group-image');
        let button = Bs.btn('btn-outline-secondary disabled btn-image', {disabled: 'disabled'}, {event: 'click', callback: (e) => {
                if(!$(e.target).hasClass('disabled') && s.image !== '') {

                    /**
                     * @type {HTMLImageElement} image
                     */
                    let image = new Image();
                    image.src = s.getImageUrl();
                    s.previewImage(image);
                }
            }}).append(Bs.fas('fa-camera fa-fw'));
        // let grpAppend = Bs.inpGrpApp().append(button);
        let input = Bs.inp('input-image', {
            placeholder: 'Image',
            value: s.image
        }, {event: 'blur', callback: (e) => {

            let value = e.target.value.trim();

            if(value !== '') {

                s.image = e.target.value = s.parent.findImage(value);
            }
            else {
                s.image = value;
            }

            s.toggleActiveStataButton(button, s.image);
        }});

        s.parent.makeAutoComplete(input, s.parent.media.image.files, (event, ui) => {

            s.image = ui.item.value;
        });

        s.toggleActiveStataButton(button, s.image);

        s.inputGrpImage.append(input, button)
    }

    /**
     *
     * @param {JQuery} button
     * @param {string} value
     * @protected
     */
    protected toggleActiveStataButton(button: JQuery, value: string): void {

        if(value !== '') {
            button.removeClass('btn-outline-secondary disabled').removeAttr('disabled').addClass('btn-outline-primary');
        }
        else {

            button.addClass('btn-outline-secondary disabled').attr({'disabled': 'disabled'}).removeClass('btn-outline-primary');
        }
    }

    /**
     * @description (Re)Builds the URL of the media-file
     * @method setMediaUrl
     * @param {string} url
     * @return {string}
     * @protected
     */
    public setMediaUrl(url: string): string {

        let s = this;
        return s.parent.setMediaUrl(url);
    }
}