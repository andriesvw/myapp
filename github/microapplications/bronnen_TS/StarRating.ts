/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 06/09/2022 09:28
 * Filename: Rating
 */
import {
    evaluate, parse
} from 'mathjs';
import {Bs} from '../genericfe-bs4/bs/Bs';
import {ScoreType} from "./interfaces/ScoreType";
import {TimerType} from "./interfaces/TimerType";

const math = {
    eval: evaluate,
    parse: parse
};

/**
 * @param {any} data : Exercise-data
 * @param {TIMER} timer : TIMER-object
 * @param {SCORE} score : SCORE-object
 * @param {number} amount | Amount of exercises to determine relative score
 * @param {string} classes | string of classes to be added to the Rating-container
 *
 */
export class StarRating {

    data: any;
    timer: TimerType;
    score: ScoreType;
    answerrecords: any[];
    timerecords: any[];
    maxScore: number;
    stars: number;
    target: any;
    classes: string;
    amount: number;
    fraction: number;
    fetched: boolean;
    _ratingUrl: string;

    constructor(data: any, timer: TimerType, score: ScoreType, amount, classes: string = 'col-2') {

        if(timer === undefined) {
            console.error('timer is not defined. Check if timer is initialized.')
        }
        if(score === undefined) {
            console.error('score is not defined. Check if score is initialized.')
        }

        this.target = $('.container');
        this.data = data;
        this.timer = timer;
        this.score = score;
        this.answerrecords = [];
        this.timerecords = [];
        this.maxScore = 300;
        this.stars = 3;
        this.classes = classes;
        this.amount = amount;
        this.fetched = false;
        this.fraction = 1;
        this.ratingUrl = '/api/rating/save';

        this.init();
    }

    protected set ratingUrl(url: string) {
        this._ratingUrl = url;
    }

    protected get ratingUrl(): string {
        return this._ratingUrl;
    }

    public init(){

        let s = this;
        if(!s.checkExistingRatingInExerciseData()) {

            s.fetchRating();
        }
        else {

            s.handleRating(s.data.rating);
        }
    }

    public setTarget(target) {

        let s = this;
        s.target = target;
    }

    /**
     *
     * @param {string} classes
     */
    public setClasses(classes: string) {

        let s = this;
        s.classes = classes;
    }

    protected fetchRating(){

        let s = this;

        $.ajax({
            url: '/api/rating/get',
            data: {
                appname: s.data.applicationname,
                language: s.data.language,
                exerciseid: s.data.id
            },
            method: 'GET',
            dataType: 'JSON'
        }).done((response) => {

            if(response.length > 0) {

                s.handleRating(response[0]);
            }
        }).fail((jqXHR, textStatus, errorThrown) => {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    }

    protected handleRating(rating){

        let s = this;
        s.fetched = true;
        s.fraction =  s.amount / rating.amount;

        s.answerrecords = rating.ratinglines.filter((item, i) => {
            return item.type === 'answers';
        });

        s.timerecords = rating.ratinglines.filter((item, i) => {
            return item.type === 'time';
        });
    }

    /**
     *
     * @param key
     * @return {any}
     */
    getValue(key) {

    }

    /**
     * @return boolean
     */
    public checkExistingRatingInExerciseData() {

        let s = this;

        if(s.data.rating === undefined || s.data.rating.length === 0){

            return false;
        }

        return true;
    }

    /**
     *
     * @param {any|null} exec | example:
     * function(cc) {
     *
     *     $('#results').removeClass('col-md-4').addClass('col-md-2');
     *     $(cc).insertAfter($('#results'));
     * });
     */
    public completed(exec = null){

        let s = this;

        if(s.fetched) {

            s.getRating(exec);
        }
    }

    /**
     *
     * @param {number} res
     * @return []
     */
    protected getStarsArray(res: number) {

        let s = this;
        let starsArray = [];

        if(res < 0.5) {
            // 0
            starsArray.push(
                Bs.far('star text-warning fa-2x'),
                Bs.far('star text-warning fa-2x'),
                Bs.far('star text-warning fa-2x')
            );
        } else if(res < 1) {
            // 0.5
            starsArray.push(
                Bs.fas('star-half-alt text-warning fa-2x'),
                Bs.far('star text-warning fa-2x'),
                Bs.far('star text-warning fa-2x')
            );
        } else if(res < 1.5) {
            // 1
            starsArray.push(
                Bs.fas('star text-warning fa-2x'),
                Bs.far('star text-warning fa-2x'),
                Bs.far('star text-warning fa-2x')
            );
        } else if(res < 2) {
            // 1.5
            starsArray.push(
                Bs.fas('star text-warning fa-2x'),
                Bs.fas('star-half-alt text-warning fa-2x'),
                Bs.far('star text-warning fa-2x')
            );
        }
        else if (res < 2.5) {
            // 2
            starsArray.push(
                Bs.fas('star text-warning fa-2x'),
                Bs.fas('star text-warning fa-2x'),
                Bs.far('star text-warning fa-2x')
            );
        } else if(res < 3) {
            // 2.5
            starsArray.push(
                Bs.fas('star text-warning fa-2x'),
                Bs.fas('star text-warning fa-2x'),
                Bs.fas('star-half-alt text-warning fa-2x')
            );
        }
        else if(res === 3) {
            // 3
            starsArray.push(
                Bs.fas('star text-warning fa-2x'),
                Bs.fas('star text-warning fa-2x'),
                Bs.fas('star text-warning fa-2x')
            );
        }

        return starsArray;
    }

    /*
     * From protected to public?
     * What to do with scoreRed and timeRed
     */
    protected getRating(exec = null){

        let s = this;
        let cc = Bs.col('col ' + s.classes);
        let scoreRed = s.getScoreReduction();
        let timeRed = s.getTimeReduction();
        let score = s.maxScore + scoreRed + timeRed;
        let frac = score/s.maxScore;
        let res = frac*s.stars
        let starsArray = s.getStarsArray(res);
        let pathId = s.canSaveRating();

        if(pathId !== false) {

            s.saveRating(parseInt(pathId), res, cc);
        }

        starsArray.forEach((star, j) => {
            cc.append(star);
        });

        if(exec !== null) {
            exec($(cc));
        }
        else {
            $(s.target).append(cc);
        }
    }

    protected canSaveRating() {

        if($('#pathID').length > 0){

            return $('#pathID').data('value');
        }

        return false;
    }

    protected mixData(data){

        let s = this;
        let dt = new Date();
        let a =  + 1;
        let b = dt.getMinutes() + 1;
        let c = dt.getUTCSeconds() + 1;
        let d = Math.floor((Math.random() * 9999) + 1);

        let t = (((data.rating * a)*d) + c) * b;

        let x = {
            b: (a > 0) ? a : 1,
            c: t,
            d: c,
            e: d,
            f: b,
            g: data.pathid,
            v: 1.1
        };

        return x;

    }

    protected saveRating(pathId: number, result: number, target: any) {

        let s = this;

        $.ajax({
            url: s.ratingUrl,
            data: s.mixData({
                    rating: result,
                    pathid: pathId
                }),
            method: 'POST',
            dataType: 'JSON'
        }).done((response) => {

            if(response['bestscore'] !== undefined && response['bestscore'] === true) {
                $(target).append('<i class="fas fa-trophy fa-fw text-success mb-3"></i>');
            }
        }).fail((jqXHR, textStatus, errorThrown) => {

            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });

    }

    /**
     * @return {number}
     */
    protected getScoreReduction(){

        let s = this;
        let reduction = 0;

        if(s.answerrecords.length === 1) {

            reduction = s.score.wrong * s.answerrecords[0].condition * s.answerrecords[0].formula;
        }
        else if(s.answerrecords.length > 1) {

            let found = false;
            let i = 0;

            while(i < s.answerrecords.length && !found) {
                if(parseInt(s.answerrecords[i].condition) === s.score.wrong || (i === (s.answerrecords.length-1) && parseInt(s.answerrecords[i].condition) < s.score.wrong)) {

                    reduction = s.answerrecords[i].formula;
                    found = true;
                }

                i++;
            }

            if(!found) {
                // fallback on first answer-record
                reduction = s.score.wrong * s.answerrecords[0].condition * s.answerrecords[0].formula;
            }
        }

        return parseInt(reduction.toString());
    }

    /**
     * @return {number}
     */
    protected getTimeReduction(){

        let s = this;
        let reduction = 0;
        let i = 0;
        let found = false;
        let calcSeconds = s.timer.sec * (1/s.fraction);

        while(i < s.timerecords.length && !found) {

            let rec = s.timerecords[i];
            let parser = math.parse(rec.condition);
            let node = parser.compile();

            if(node.evaluate({x: calcSeconds})) {

                reduction = rec.formula;
                found = true;
            }

            i++;
        }

        return parseInt(reduction.toString());
    }
}
