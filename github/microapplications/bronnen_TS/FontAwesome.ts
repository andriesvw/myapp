/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 11/11/2023 11:17
 * Filename: FontAwesome
 */

import $ from "jquery";

/**
 * @class FontAwesome
 */
export class FontAwesome {

    static _fixedWidth: boolean = true;
    private static _multiplier: string = '';

    static get fixedWidth(): boolean {
        return this._fixedWidth;
    }

    static set fixedWidth(value: boolean) {
        this._fixedWidth = value;
    }

    static get multiplier(): string {
        return this._multiplier;
    }

    static set multiplier(value: string) {
        this._multiplier = value;
    }

    static get fw(): string {
        const s = this;
        return (s.fixedWidth) ? ' fa-fw' : '';
    }

    /**
     * @param {string} icon
     * @return {JQuery<HTMLElement>}
     */
    static s(icon: string): JQuery<HTMLElement> {
        return this.solid(icon)
    }

    /**
     * @param {string} icon
     * @return {JQuery<HTMLElement>}
     */
    static r(icon: string): JQuery<HTMLElement> {
        return this.regular(icon)
    }

    /**
     * @param {string} icon
     * @return {JQuery<HTMLElement>}
     */
    static b(icon: string): JQuery<HTMLElement> {
        return this.brand(icon)
    }

    /**
     * @param {string} icon
     * @return {JQuery<HTMLElement>}
     */
    static solid(icon: string): JQuery<HTMLElement> {
        const s = this;

        if(icon.indexOf('fa-') === -1) {
            icon = 'fa-' + icon;
        }
        return $('<i/>', {
            class: 'fa-solid' + s.fw + ' ' + s.multiplier + ' ' + icon
        });
    }

    /**
     * @param {string} icon
     * @return {JQuery<HTMLElement>}
     */
    static regular(icon: string): JQuery<HTMLElement> {
        const s = this;
        return $('<i/>', {
            class: 'fa-regular' + s.fw + ' ' + s.multiplier + ' ' + icon
        });
    }

    /**
     * @param {string} icon
     * @return {JQuery<HTMLElement>}
     */
    static brand(icon: string): JQuery<HTMLElement> {
        const s = this;
        return $('<i/>', {
            class: 'fa-brand' + s.fw + ' ' + s.multiplier + ' ' + icon
        });
    }
}