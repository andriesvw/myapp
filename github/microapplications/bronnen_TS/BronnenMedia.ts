/*
 * Author: Floris Vrij
 * E-mail: 243651@edu.rocfriesepoort.nl
 * Project: fcsprint2
 * Created: 06/03/2024 13:23
 * Filename: BronnenMedia
 */

require('jquery-ui/themes/base/all.css');
import 'jquery-ui/ui/core';

export class BronnenMedia {

    protected _language: string;

    constructor(language: string = "nl_NL") {

        this.language = language;
    }

    public get language(): string {
        return this._language;
    }

    public set language(value: string) {
        this._language = value;
    }

    public get mediaDomain(): string {
        return "https://media.fcsprint2.nl/media/"
    }


    public getAudioLink(file) {
        const s = this;
        return (s.mediaDomain + s.language + "/audio/" + file);
    }
    
    public getImageLink(file) {
        const s = this;
        return (s.mediaDomain + s.language + "/img/" + file);
    }


    public setMediaUrl(url: string): string {

        let s = this;

        if (url === null) {
            return '';
        }

        url = url.replace('../../', '').replace('https://test.diglin.eu/', '').replace('https://media.fcsprint2.nl/', '').replace(location.protocol + '//' + location.host + '/', '');

        if (url[0] === '/') {
            url = url.substr(1);
            url.slice(1);
        }

        let urlArray = url.split('\/');

        let mediaId = urlArray.indexOf('media');

        if (urlArray.indexOf('soundbars') > -1) {
            let lang = s.getLang(urlArray);

            url = url.replace('\/\/', '\/').replace('media/soundbars/' + lang, 'media/' + lang + '/soundbar');
        }

        if (urlArray.indexOf('audio') > -1 || urlArray.indexOf('img') > -1) {

            //OLD PATH
            let lang = 'nl_NL';

            if(url.indexOf('media/audio/') > -1 || url.indexOf('media/img/') > -1) {
                lang = urlArray[(mediaId+2)];
                url = url.replace('media/audio/' + lang, 'media/' + lang + '/audio').replace('media/img/' + lang, 'media/' + lang + '/img');
            }

            lang = urlArray[(mediaId+1)];

            let targetLang = s.getLang(url.split('\/'));

            if(lang !== targetLang) {
                url = url.replace('media/' + lang + '/audio', 'media/' + targetLang + '/audio').replace('media/' + lang + '/img', 'media/' + targetLang + '/img');
            }
        }

        url = s.getMenuLanguage(url);

        if (s.checkMediaLocation()) {

            return location.protocol + '//' + location.host + '/' + url;
        }

        return 'https://media.fcsprint2.nl/' + url;
    }

    /**
     * @description Deermines if media-location is local or external
     * @method checkMediaLocation
     * @return {boolean}
     * @protected
     */
    protected checkMediaLocation(): boolean {

        let s = this;
        let ip = location.host;
        let locations = [
            'acc.diglinplus.nl',
            'app.diglinplus.nl',
            'app2.diglinplus.nl',
            'lcl.diglin-plus.eu'
        ];

        return (locations.indexOf(ip) > -1);
    }

    /**
     *
     * @param {string} url
     * @return {string}
     * @protected
     */
    protected getMenuLanguage(url:string): string {

        let s = this;

        if(s.getUrlParameter('menulang') !== undefined && s.getUrlParameter('menulang') !== '') {

            let urlArrayMedia = url.split('\/');
            let ID = urlArrayMedia.indexOf('media');
            urlArrayMedia[(ID+1)] = s.getUrlParameter('menulang');

            return urlArrayMedia.join('/');
        }

        return url
    }

    /**
     *
     * @param {any[]} urlArray
     * @return {string}
     * @protected
     */
    protected getLang(urlArray: any[]): string{

        let s = this;
        let mediaId = urlArray.indexOf('media');
        let lang = urlArray[(mediaId + 1)];
        if(s.getUrlParameter('menulang') !== undefined) {
            lang = s.getUrlParameter('menulang');
        }

        return lang;
    }

    /**
     * @description Returns the value of the given URL-parameter
     * @method getUrlParameter
     * @param {string|number} sParam
     * @return {*}
     * @protected
     */
    public getUrlParameter(sParam: string|number): any {

        let s = this;

        let sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }


}