/*
 * FC-Sprint2 project => navigation.js
 *
 * The project "diglin.eu" is property of FC-Sprint2
 *
 * Created at:
 * 8-dec-2016 @ 9:06:35
 *
 * Created by:
 * Andries van Weeren
 * a.weeren@fcroc.nl
 */

interface Navigator {
    app: {
        exitApp: () => any; // Or whatever is the type of the exitApp function
    }
}

export class Navigation {

    private _target: JQuery;
    private _brand: string;

    constructor(target: string|JQuery, brand = '') {

        this.target = target;
        this.brand = brand;

        this.init();
    }

    get target(): JQuery {
        return this._target;
    }

    set target(target: string|JQuery) {
        if(typeof target === 'string') {
            this._target = $(target);
        }
        else {
            this._target = target;
        }
    }

    get brand(): string {
        return this._brand;
    }

    set brand(brand: string) {
        this._brand = brand;
    }

    init() {
        
        let s = this;

        let style = '<style type="text/css">.navbar{margin-bottom: 0px;} .navbar-control{position: fixed;display: block;top: 0.3em;right: 1.0em;z-index: 1000;} .menuright{margin-right: 3.0em;}.list-unstyled li{margin:2px 0px}.navbar-logo{height:35px;margin:0px auto;margin-top:5px;}</style>';
        let navbar = '<div class="navbar-control"> \
                <ul class="list-unstyled"> \
                    <li><button class="btn btn-sm btn-primary" id="btnReload"><i class="fas fa-sync-alt fa-fw"></i></button></li> \
                    <li><button class="btn btn-sm btn-primary" id="btnBack" onclick="javascript:history.go(-1)" data-rel="back"><i class="fas fa-arrow-left fa-fw"></i></button></li> \
                </ul> \
            </div>';

        $('head').append(style);
        $(s.target).empty().append(navbar);
        s.events();
    }
    
    events() {
        //    if (device.platform === "iOS" && parseInt(device.version) === 9) {
        //        $.mobile.hashListeningEnabled = false;
        //    }

        function goBack() {

            // let userAgent = navigator.userAgent || navigator.vendor || window.opera;
            let userAgent = navigator.userAgent || navigator.vendor;
            if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
                // IOS DEVICE
                history.go(-1);
            }
            else if (userAgent.match(/Android/i)) {
                // ANDROID DEVICE

                // navigator.app.backHistory();
                history.back();
            }
            else {
                // EVERY OTHER DEVICE
                history.go(-1);
                // navigator.app.backHistory();
            }
        }

        $('#btnBack').on('click', function () {

            goBack();
        });

        $(document).on('click', '#btnReload', function () {
            location.reload();
        });
    }
}
