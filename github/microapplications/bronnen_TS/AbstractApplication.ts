/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 17-11-20 14:38
 * Filename: AbstractApplication
 */

/**
 * Baseclass for every application CMS and Frontend
 * @abstract AbstractApplication
 */

import type {MediaType} from './types/MediaType'
import {AppType} from "./types/AppType";
import 'jquery-ui/ui/core';
import 'jquery-ui/ui/widgets/autocomplete';
import {atob} from "buffer";
import $, {data, ui} from "jquery";

export abstract class AbstractApplication {

    private _editUrl: string = '/admin/appexercise/edit';
    private _mediaUrl: string = 'https://media.fcsprint2.nl';
    private _media: MediaType;

    /**
     *
     * @return {MediaType}
     * @protected
     */
    public get media(): MediaType{

        return this._media;
    }

    /**
     *
     * @param {MediaType} media
     * @protected
     */
    public set media(media: MediaType) {

        this._media = media;
    }

    /**
     *
     * @return {string}
     * @protected
     */
    protected get editUrl(): string{
        return this._editUrl;
    }
    /**
     *
     * @param {string} url
     */
    protected set editUrl(url: string) {

        this._editUrl = url;
    }

    /**
     *
     * @return {string}
     * @protected
     */
    protected getEditUrl(): string{

        let s = this;
        return s.editUrl;
    }

    /**
     *
     * @return {string}
     * @protected
     */
    protected get mediaUrl(): string{
        return this._mediaUrl;
    }

    /**
     *
     * @param {string} url
     */
    protected set mediaUrl(url: string) {
        this._mediaUrl = url;
    }

    /**
     * @description Returns unhashed response if it's hashed or just returns response is it's not hashed
     * @method getResponse
     * @param {Object} response
     * @return {any}
     */
    protected getResponse(response){

        return ((response.notitia !== undefined) ? JSON.parse(window.atob(response.notitia)) : response);
    }

    /**
     * @description Creates a copy of an array
     * @method getCopyOfArray
     * @param {any[]} array
     * @return {any[]}
     */
    protected getCopyOfArray(array: any[]): any[]{

        let copy = [];

        array.forEach(function(item, i){
            copy.push(item);
        });

        return copy;
    }

    /**
     * @description Shuffles all values in given array
     * @method shuffle
     * @param {any} array
     * @return {any[]}
     */
    public shuffle(array: any[]): any[] {

        const s = this;

        let currentIndex: number = array.length,
            temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex)
        {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    /**
     * @description Set the given element sticky to top on scroll
     * @method setAffix
     * @param {jQuery} elmnt
     * @return {void}
     * @protected
     */
    protected setAffix(elmnt): void {

        let s = this;
        let scrollStart = 0;
        let style = $('<style/>').text('.affixed {\n' +
            '    position: fixed;\n' +
            '    z-index: 1000;\n' +
            '    background: #fff;\n' +
            '    top: 0px;\n' +
            '    left: 0px;\n' +
            '    right: 0px;\n' +
            '    width: 100%;\n' +
            '    padding-top: 15px;\n' +
            '    padding-bottom: 15px;\n' +
            '    border-bottom: solid 1px #6c757d;\n' +
            '    margin-right: 0px;\n' +
            '    margin-left: 0px;\n' +
            '}');
        $('head').append(style);

        $(window).on('scroll', function (event) {

            if (scrollStart === 0) {

                scrollStart = $(elmnt).offset().top;
            }

            if ($(window).scrollTop() >= $(elmnt).offset().top && $(window).scrollTop() >= scrollStart) {

                $(elmnt).addClass('affixed');
            }
            else if ($(window).scrollTop() < scrollStart) {

                $(elmnt).removeClass('affixed');
                $(elmnt).removeAttr('style');
            }
        });
    }

    /**
     * @description Determines the application-index URL
     * @method getIndexUrl
     * @return {string}
     * @protected
     */
    protected getIndexUrl(): string{

        let s = this;

        let urlArray = window.location.pathname.split(/\//g);
        let index = urlArray.indexOf('applications');
        let application = urlArray[(index+1)];

        return '/admin/index/applications/' + application;
    }

    /**
     * @description Deermines if media-location is local or external
     * @method checkMediaLocation
     * @return {boolean}
     * @protected
     */
    protected checkMediaLocation(): boolean {

        let s = this;
        let ip = location.host;
        let locations = [
            'acc.diglinplus.nl',
            'app.diglinplus.nl',
            'app2.diglinplus.nl',
            'lcl.diglin-plus.eu'
        ];

        return (locations.indexOf(ip) > -1);
    }

    /**
     * @description Determines what the location is
     * @method checkLocation
     * @return {boolean}
     * @protected
     */
    protected checkLocation(): boolean {

        let s = this;
        let ip = location.host;
        let locations = [
            '10.35.92.70',
            '10.35.92.71',
            '37.46.138.72',
            'www.diglinplus.nl',
            'lcl.diglin-plus.eu',
            'ld.fcsprint2.nl'
        ];

        return (locations.indexOf(ip) > -1);
    }

    /**
     * @description Determines the application-name
     * @method getAppData
     * @return {{app: *, id: (boolean|string)}}
     */
    protected getAppData(): {id: (number|string), app: string, application: string} {

        let s = this;
        let host = window.location.href.split('\/');
        let foundID = host.indexOf('applications');

        return {
            'id': ((s.getUrlParameter('id') !== undefined) ? s.getUrlParameter('id') : s.getUrlParameter('Id')),
            'app': s.getCurrentApplication(),
            'application': s.getCurrentApplication()
        };
    }

    /**
     * @description Returns the current application
     * @method getCurrentApplication
     * @return {*}
     * @protected
     */
    protected getCurrentApplication(): any{

        let host = window.location.href.split('\/');
        let foundID = host.indexOf('applications');
        let app = '';
        if (foundID > -1) {
            app = host[foundID + 1];
        }
        else {
            foundID = host.indexOf('application');
            app = host[foundID + 1];
        }

        return app;
    }

    /**
     * @description Sets the URL of the JSON-URI local or external
     * @method setJsonUrl
     * @param {string} app
     * @return {string}
     * @protected
     */
    protected setJsonUrl(app: string): string {

        let s = this;
        let host = window.location.href.split('\/');
        let foundID = host.indexOf('applications');

        app = app || null;

        if (foundID > -1) {
            app = host[foundID + 1];
        }
        else {
            foundID = host.indexOf('application');
            app = host[foundID + 1];

        }

        if (window.location.hostname.indexOf('nt2school') > -1 || window.location.hostname.indexOf('diglin-plus') > -1 || s.checkLocation()) {
            return 'https://test.diglin.eu/applications/' + app + '/';
        }

        return '';
    }

    /**
     * @description (Re)Builds the URL of the media-file
     * @method setMediaUrl
     * @param {string} url
     * @return {string}
     * @protected
     */
    public setMediaUrl(url: string): string {

        let s = this;

        if (url === null) {
            return '';
        }

        url = url.replace('../../', '').replace('https://test.diglin.eu/', '').replace('https://media.fcsprint2.nl/', '').replace(location.protocol + '//' + location.host + '/', '');

        if (url[0] === '/') {
            url = url.substr(1);
        }

        let urlArray = url.split('\/');

        let mediaId = urlArray.indexOf('media');

        if (urlArray.indexOf('soundbars') > -1) {
            let lang = s.getLang(urlArray);

            url = url.replace('\/\/', '\/').replace('media/soundbars/' + lang, 'media/' + lang + '/soundbar');
        }

        if (urlArray.indexOf('audio') > -1 || urlArray.indexOf('img') > -1) {

            //OLD PATH
            let lang = 'nl_NL';

            if(url.indexOf('media/audio/') > -1 || url.indexOf('media/img/') > -1) {
                lang = urlArray[(mediaId+2)];
                url = url.replace('media/audio/' + lang, 'media/' + lang + '/audio').replace('media/img/' + lang, 'media/' + lang + '/img');
            }

            lang = urlArray[(mediaId+1)];

            let targetLang = s.getLang(url.split('\/'));

            if(lang !== targetLang) {
                url = url.replace('media/' + lang + '/audio', 'media/' + targetLang + '/audio').replace('media/' + lang + '/img', 'media/' + targetLang + '/img');
            }
        }

        url = s.getMenuLanguage(url);

        if (s.checkMediaLocation()) {

            return location.protocol + '//' + location.host + '/' + url;
        }

        return 'https://media.fcsprint2.nl/' + url;
    }

    /**
     *
     * @param {string} url
     * @return {string}
     * @protected
     */
    protected getMenuLanguage(url:string): string {

        let s = this;

        if(s.getUrlParameter('menulang') !== undefined && s.getUrlParameter('menulang') !== '') {

            let urlArrayMedia = url.split('\/');
            let ID = urlArrayMedia.indexOf('media');
            urlArrayMedia[(ID+1)] = s.getUrlParameter('menulang');

            return urlArrayMedia.join('/');
        }

        return url
    }

    /**
     *
     * @param {any[]} urlArray
     * @return {string}
     * @protected
     */
    protected getLang(urlArray: any[]): string{

        let s = this;
        let mediaId = urlArray.indexOf('media');
        let lang = urlArray[(mediaId + 1)];
        if(s.getUrlParameter('menulang') !== undefined) {
            lang = s.getUrlParameter('menulang');
        }

        return lang;
    }

    /**
     * @description Sets the URL of de JSON-data
     * @method setJsonApiUrl
     * @return {string}
     * @protected
     */
    protected setJsonApiUrl(): string {

        let s = this;

        if (window.location.hostname.indexOf('nt2school') > -1 || window.location.hostname.indexOf('diglin-plus') > -1 || s.checkLocation()) {

            return 'https://test.diglin.eu';
        }

        return '';
    }

    /**
     * @description Returns the value of the given URL-parameter
     * @method getUrlParameter
     * @param {string|number} sParam
     * @return {*}
     * @protected
     */
    public getUrlParameter(sParam: string|number): any {

        let s = this;

        let sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    /**
     * @description Sets all elements to same width
     * @method setSameWidth
     * @param {jQuery} target
     * @return {void}
     * @protected
     */
    protected setSameWidth(target): void {

        let s = this;
        let width = 0;

        $(target).each(function(i, el){

            if($(el).outerWidth() > width) {

                width = $(el).outerWidth();
            }
        });

        $(target).css({width: width.toString() + 'px'});
    }

    /**
     * @description Sets all elements to same height
     * @method setSameHeight
     * @param {jQuery} target
     */
    protected setSameHeight(target) {

        let s = this;

        let height = 0;

        $(target).each(function (i, el) {

            let currentHeight = $(el).outerHeight();

            if (currentHeight > height) {
                height = currentHeight;
            }
        });

        $(target).css({height: height.toString() + 'px'})
    }

    /**
     * @description Gets the JSON-data and executes the given function
     * @method getJson
     * @param callback
     * @return {void}
     * @protected
     */
    protected getJson(callback = null): void {

        let s = this;
        let appData = s.getAppData();
        let id = s.getUrlParameter('id');

        $.ajax({
            url: s.setJsonApiUrl() + '/api/appexercise/get/' + id,
            data: {
                _: new Date().getTime(),
                'id': id,
                'application': appData.application,
            },
            method: 'post',
            dataType: 'json'
        }).done(function (response) {

            response = s.getResponse(response);

            callback(response);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    }

    /**
     *
     * @param {any} object
     * @return {void}
     * @protected
     */
    protected ajaxCall(object: any): void {

        $.ajax({
            url: object.url,
            async: object.async || true,
            type: object.type,
            data: object.data || {},
            dataType: object.dataType,
            beforeSend: object.beforeSend || function () {}
        })
            .done(object.success || function () {})
            .fail(object.error || function () {})
            .always(object.complete || function () {});
    }

    /**
     * @description Checks userinput for "possible on the way" to create a diacritic character. When returns true, the current answer should
     * not be (yet) be taken as wrong answer.
     * @method checkDiacriticsMacOS
     * @param {string} answer - the correct answer
     * @param {string} userinput - the current userinput to be verified against answer
     * @return {boolean}
     */
    public checkDiacriticsMacOS(answer, userinput): boolean{

        let s = this;
        answer = answer.toString();
        // Escape userinput string
        // $& means the whole matched string
        userinput = userinput.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

        let patt = new RegExp('^' + userinput, 'u');
        let answerSplitted = answer.split('');
        /* Escape only last (diacritic) character of userinput-string in the answer-string */
        let lastPos = (userinput.length - 1);

        if(lastPos >= answer.length){
            return false;
        }
        
        let lastChar = answer[lastPos];
        let lastCharEscaped = lastChar.normalize("NFD").replace(/([\u0300-\u036f])/ug, "");

        answerSplitted[lastPos] = lastCharEscaped;
        let answerEscaped = answerSplitted.join('');

        if(patt.test(answer) || patt.test(answerEscaped) || (lastChar !== lastCharEscaped && ['~','´','¨','`','^'].indexOf(userinput[lastPos]) > -1)){

            return true;
        }

        return false;
    }

    /**
     * Sends data-object to backend
     * @param {Object} data
     * @param {boolean} returnCurrent
     * @param {boolean} numericcheck
     * @return {void}
     */
    protected sendData(data, returnCurrent = false, numericcheck = true) {

        let s = this;

        $.ajax({
            url: s.getEditUrl(),
            type: 'post',
            data: {
                data: data,
                numericcheck: numericcheck
            },
            dataType: 'json'
        }).done(function (response)
        {

            if(returnCurrent) {
                // return to current fetch ID from response if @param id is "new"
                window.location.href = window.location.pathname + '?id=' + response.id.toString();
            }
            else {

                window.location.href = s.getIndexUrl();
            }
        }).fail(function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    }

    /**
     *
     * @param value
     * @return {string}
     */
    public findAudio(value: string){

        let s = this;
        if(!/\.mp3$/.test(value)) {
            value += '.mp3';
        }

        let foundIndex: number = s.media.audio.lowercase.indexOf(value.toLowerCase());

        if(foundIndex === -1 && value.indexOf('_') > -1) {

            foundIndex = s.media.audio.lowercase.indexOf(value.toLowerCase().replace('_', ' '));
        }

        if(foundIndex > -1) {

            return s.media.audio.files[foundIndex];
        }

        return '';
    }

    /**
     *
     * @param value
     * @return {string}
     */
    public findImage(value: string){

        let s = this;
        if(!/\.jpg$/.test(value)) {
            value += '.jpg';
        }

        let foundIndex: number = s.media.image.lowercase.indexOf(value.toLowerCase());

        if(foundIndex === -1 && value.indexOf('_') > -1) {

            foundIndex = s.media.image.lowercase.indexOf(value.toLowerCase().replace('_', ' '));
        }

        if(foundIndex > -1) {

            return s.media.image.files[foundIndex];
        }

        return '';
    }

    public makeAutoComplete(elmt, list, select = (event, ui) => { console.log(ui)}) {

        let s = this;

        (<any>$(elmt)).autocomplete({
            source: function (request, response) {
                let filteredArray = $.map(list, function (item) {

                    if (item.toLowerCase().trim().startsWith(request.term.toLowerCase()) || item.toLowerCase().startsWith(request.term.toLowerCase())) {
                        return item;
                    }
                    else {
                        return null;
                    }
                });
                let results = (<any>$).ui.autocomplete.filter(filteredArray, request.term);
                response(results.slice(0, 10));
            },
            select: select,
            minLength: 2
        });

        return elmt;
    }

    /**
     *
     * @param {string} url
     */
    public stripUrl(url: string): string {

        url = url.trim();
        if(/soundcloud/.test(url)) {
            return url;
        }

        if (url === '') {

            return url;
        }

        let urlArray: string[] = url.split(/\//);
        let lastId: number = urlArray.length - 1;

        if(urlArray[lastId] === '.jpg' || urlArray[lastId] === '.mp3') {
            return '';
        }

        return urlArray[lastId];
    }

    /**
     * Returns col with spinner-icons
     * @returns {JQuery}
     */
    protected getSpinner(): JQuery {

        let col: JQuery<HTMLElement> = $('<div/>', {
            id: 'loadindicator',
            class: 'col col-md-12 col-lg-12 text-center'
        }).html('<i class="fas fa-spinner fa-fw fa-pulse fa-4x"></i>');

        return col;
    }

    /**
     *
     * @param {jQuery} target
     * @param {any} appData
     */
    protected getSelectLanguage(target: JQuery, appData, callback: any): void {

        let select: JQuery<HTMLElement> = $('<select/>', {
            class: 'form-select'
        }).on('change', (e: JQuery.ChangeEvent<HTMLElement>): void => {

            callback(e);
        }).html('<option disabled="disabled">Loading</option>');

        $(target).append(select);

        $.ajax({
            url: "/api/languages/unique",
            method: "GET",
            dataType: 'json'
        }).done((response) => {

            select.empty();
            response.forEach((lang, i): void =>
            {

                let option: JQuery<HTMLElement> = $('<option/>', {
                    value: lang.isoCode
                }).html(lang.fullname);

                if(lang.isoCode === appData.language) {
                    option.attr({
                        selected: 'selected'
                    });
                }

                select.append(option);
            });
            let e = {target:{value: appData.language}, currentTarget:{value: appData.language}}
            callback(e);

            // s.getFileArray(true);
        }).fail((jqXHR: JQuery.jqXHR<any>, textStatus: JQuery.Ajax.ErrorTextStatus, errorThrown: string): void =>
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    }

    /**
     * This method creates an array with all the filenames belonging to a certain language
     * from this method the #ready method is fired with boolean as parameter@param language
     * @param {string} language
     * @param {(response) => {}} callback
     * @return {void}
     * @protected
     */
    protected getMediaArray(language: string, callback: any = null): void {
        let s = this;

        $.ajax({
            url: s.mediaUrl + "/api/media/fileList",
            type: "POST",
            data: {language: language},
            dataType: 'json'
        }).done((response): void => {

            if(callback !== null) {
                callback(response);
            }
            else {
                s.media = response;
            }
        }).fail((jqXHR: JQuery.jqXHR<any>, textStatus: JQuery.Ajax.ErrorTextStatus, errorThrown: string): void => {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    }

    /**
     * @return {{id: "new", exercisetitle: "", language: "nl_NL", application: "", main_object: {title: "", language: "nl_NL"}}}
     * @protected
     */
    protected getInitialAppData(): AppType {

        const s = this;

        return {
            id: 'new',
            exercisetitle: '',
            language: 'nl_NL',
            application: s.getCurrentApplication(),
            main_object:
                {
                    title: '',
                    language: 'nl_NL',
                    image: '',
                    audio: ''
                }
        }
    }
}
