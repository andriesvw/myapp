/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 10/07/2023 09:42
 * Filename: main_app.ts
 */

require('../generic/main.scss');
const $ = require('jquery');
import 'jquery-ui';
import 'bootstrap';
import '@fortawesome/fontawesome-free/js/all';
import {WindowLoader} from "../generic/WindowLoader";
import {SpendtimeTracker} from "../generic/SpendtimeTracker";

const WL = new WindowLoader();
const SPT = new SpendtimeTracker(WL);