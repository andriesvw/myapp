/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 05/03/2024 14:18
 * Filename: BronnenShadowBox
 */

import { BsFa6 as Bs} from "./bootstrap/BsFa6";
import {FontAwesome as Fa} from "./FontAwesome";

/**
 * @class BronnenShadowBox
 */
export class BronnenShadowBox {

    private _parent: any;
    private _card: JQuery<HTMLElement>;
    private _header: JQuery<HTMLElement>;
    private _body: JQuery<HTMLElement>;
    private _footer: JQuery<HTMLElement>;

    get parent(): any {
        return this._parent;
    }

    set parent(value: any) {
        this._parent = value;
    }

    get card(): JQuery<HTMLElement> {
        return this._card;
    }

    set card(value: JQuery<HTMLElement>) {
        this._card = value;
    }

    get header(): JQuery<HTMLElement> {
        return this._header;
    }

    set header(value: JQuery<HTMLElement>) {
        this._header = value;
    }

    get body(): JQuery<HTMLElement> {
        return this._body;
    }

    set body(value: JQuery<HTMLElement>) {
        this._body = value;
    }

    get footer(): JQuery<HTMLElement> {
        return this._footer;
    }

    set footer(value: JQuery<HTMLElement>) {
        this._footer = value;
    }

    constructor(parent: any = null) {

        this.parent = parent;
        this.init();
    }

    protected init(): void {

        const s = this;
        s.card = Bs.card('border shadow');
        s.body = Bs.body();
        s.card.append(s.body);
    }

    public addImage(url: string): void {

        const s = this;
        let row: JQuery<HTMLElement> = Bs.row();
        let col: JQuery<HTMLElement> = Bs.col('col-12 text-center mb-3');
        let im: HTMLImageElement = new Image();
        im.src = url;
        $(im).addClass('img-fluid');
        col.append(im);
        row.append(col);
        s.body.append(row);
    }

    public addAudio(url: string): void {

        const s = this;
        let row: JQuery<HTMLElement> = Bs.row();
        let col: JQuery<HTMLElement> = Bs.col('col-12 text-center');
        let au: HTMLAudioElement = new Audio();
        au.src = url;
        au.controls = true;
        $(au).addClass('w-100 rounded-pill');
        col.append(au);
        row.append(col);
        s.body.append(row);
    }


}