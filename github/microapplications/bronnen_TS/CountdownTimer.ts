/**
 * Created by PhpStorm.
 * Project: FC-Sprint2
 * User: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Date: 12-02-20 | 09:09
 * Filename: CountdownTimer
 */

/**
 * Requires a target and an URL @param {number} time
 */
export class CountdownTimer {

    private _seconds: number = 0;
    private _initialSeconds: number = 0;
    private _target: JQuery;
    private interval;
    private _started: boolean = false;
    private _finished: boolean = false;
    private _int: number = 1000;
    private minDiv: JQuery = null;
    private secDiv: JQuery = null;
    private requestCallback;
    /**
     *
     * @param {jQuery|HTMLElement} target
     * @param {int} startSeconds
     * @callback requestCallback
     */
    constructor(target: string|JQuery, startSeconds: number, requestCallback) {

        this.seconds = startSeconds || 0;
        this.initialSeconds = this.seconds;
        this.target = target;
        this.interval = {};
        this.started = false;
        this.finished = false;
        this.int = 1000;
        this.requestCallback = requestCallback || function() {
            console.log('requestCallback is NOT set');
        };
        this.init();
    }

    /**
     * @return {number}
     */
    get initialSeconds(): number {
        return this._initialSeconds;
    }

    /**
     * @param {number} seconds
     */
    set initialSeconds(seconds: number) {
        this._initialSeconds = seconds;
    }

    get seconds(): number {
        return this._seconds;
    }

    set seconds(seconds: number) {
        this._seconds = seconds;
    }

    get target(): JQuery {
        return this._target;
    }

    set target(target: string|JQuery) {
        if(typeof target === 'string') {
            this._target = $(target);
        }
        else {
            this._target = target;
        }
    }

    get started(): boolean {
        return this._started;
    }

    set started(started: boolean) {
        this._started = started;
    }

    get finished(): boolean {
        return this._finished;
    }

    set finished(finished: boolean) {
        this._finished = finished;
    }

    get int(): number {
        return this._int;
    }

    set int(mseconds: number) {
        this._int = mseconds;
    }

    /**
     * Initates the CountdownTimer
     */
    init(): void {

        let s = this;

        if(!s.started) {

            s.initHtml();
        }
    }

    reset(): void {

        let s = this;
        s.requestCallback = () => {};
        s.stop();
        clearInterval(s.interval);
        s.seconds = s.initialSeconds;
    }

    /**
     *
     * @returns {string} remaining minutes based on remaining seconds
     */
    getMinutes(): string {

        let s = this;
        let minutes = Math.floor(s.seconds / 60);

        return ((minutes < 10) ? '0' + minutes.toString() : minutes.toString());
    }

    /**
     *
     * @returns {string} remaining seconds based on remaining seconds and subtracted minutes
     */
    getMinuteSeconds(): string {

        let s = this;
        let seconds = s.seconds % 60;

        return ((seconds < 10) ? '0' + seconds.toString() : seconds.toString());
    }

    /**
     * Sets the minutes and seconds timer to the target HTMLElement
     *
     * @returns {void}
     */
    initHtml(): void {

        let s = this;

        s.minDiv = $('<div/>', {
            class: 'd-inline-block'
        }).html(s.getMinutes());

        s.secDiv = $('<div/>', {
            class: 'd-inline-block'
        }).html(s.getMinuteSeconds());
        $(s.target).append('<i class="fa-regular fa-hourglass-half"></i> ',s.minDiv, '<span class="">:</span>', s.secDiv);
    }

    /**
     * Reduces the seconds by 1 and updates the HTMLElement
     * Call stop() when seconds equals or is less then 0
     *
     * @returns {void}
     */
    reduceSeconds(e) {

        let s = this;
        s.seconds--;

        $(s.minDiv).html(s.getMinutes());
        $(s.secDiv).html(s.getMinuteSeconds());

        if(s.seconds <= 0) {

            s.stop();
            s.requestCallback(e);
        }
    }

    /**
     * Starts the countdown interval
     * @returns {void}
     */
    start(): void {

        let s = this;
        if(!s.started && !s.finished) {
            s.started = true;
            s.interval = setInterval(function (e) {

                s.reduceSeconds(e);

            }, s.int);
        }
    }

    /**
     * Clears the countdown interval
     * @returns {void}
     */
    stop(): void {

        let s = this;
        if(s.started) {
            clearInterval(s.interval);
            s.started = false;
            s.finished = true;
        }
    }

    /**
     *
     * @param {string|number} param
     * @return {*}
     */
    getUrlParameter(param): any {

        let sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === param) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }
}