/*
 * FC-Sprint2 project => score.js
 *
 * The project "diglin.eu" is property of FC-Sprint2
 *
 * Created at:
 * 30-nov-2016 @ 9:49:36
 *
 * Created by:
 * Andries van Weeren
 * a.weeren@fcroc.nl
 */


import {ScoreType} from "./interfaces/ScoreType";
import $ from "jquery";
import { BsFa6 as BS } from "./bootstrap/BsFa6";
import { FontAwesome as Fa } from "./FontAwesome";

/**
 *  let s = new SCORE([ID]/[CLASS]);
 *  (object) target is the element on which the score will be displayed
 */

export class Score implements ScoreType{

    private _target: JQuery;
    private _html: JQuery<HTMLElement>;
    private _isMobile: boolean;
    private _right: number = 0;
    private _wrong: number = 0;
    private _audio: {right: HTMLAudioElement, wrong: HTMLAudioElement};
    private _enabled: boolean = true;
    private _options: {header?: boolean};

    constructor(target: string|JQuery<HTMLElement> = null, options: {header?: boolean} = {}) {

        this.options = options;
        this.target = target;
        this.isMobile = false; //initiate as true
        // device detection
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
            this.isMobile = true;
        }
        
        if($('#scoretimer').length === 0) {

            $('<style/>', {id: 'scoretimer'}).text('.inline{ display: inline-block; margin-right: 0.5rem}').appendTo($('head'));
        }
        this.right = 0;
        this.wrong = 0;
        this.audio = {
            right: new Audio('/audio/good.mp3'),
            wrong: new Audio('/audio/false.mp3')
        };

        this.init();
    }

    public get options(): { header?: boolean } {
        return this._options;
    }

    public set options(value: { header?: boolean }) {
        this._options = value;
    }

    /**
     *
     * @return {boolean}
     */
    public get header(): boolean {

        if(this.options.header === undefined) {
            this.options.header = true;
        }
        return this.options.header;
    }

    /**
     *
     * @param {boolean} value
     */
    public set header(value: boolean) {
        this.options.header = value;
    }

    public get enabled(): boolean{
        return this._enabled;
    }

    public set enabled(enabled: boolean) {
        this._enabled = enabled;
    }

    public get target(): JQuery {
        return this._target;
    }

    protected set target(target: string|JQuery) {
        if(typeof target === 'string') {
            this._target = $(target);
        }
        else {
            this._target = target;
        }
    }

    protected get isMobile(): boolean {
        return this._isMobile;
    }

    protected set isMobile(isMobile: boolean) {
        this._isMobile = isMobile;
    }

    public get right(): number {
        return this._right;
    }

    protected set right(amount: number) {
        this._right = amount;
    }

    public get wrong(): number {
        return this._wrong;
    }

    public set wrong(amount: number) {
        this._wrong = amount;
    }

    public get audio(): {right: HTMLAudioElement, wrong: HTMLAudioElement} {
        return this._audio;
    }

    public set audio(audio: {right: HTMLAudioElement, wrong: HTMLAudioElement}) {
        this._audio = audio;
    }

    public get html(): JQuery<HTMLElement> {
        return this._html;
    }

    public set html(value: JQuery<HTMLElement>) {
        this._html = value;
    }

    protected init() {
        
        let s = this;

        if (s.target !== null)
        {
            let style = $('<style/>').attr('type', 'text/css').html(s.target + ' .inline{display: inline-block;}' + s.target + ' .symbol{margin: 0px ' + (s.isMobile ? 0:10) + 'px;}');
            $('head').append(style);

            $(s.target).html('');

            let rightInner = $('<div/>',{
                class:'rightInner d-inline-block mr-2',
                style:'margin-right: .' + (s.isMobile ? 2:5) + 'rem !important;'
            }).html(s.right.toString());

            let iCheck = $('<i/>').addClass('fas fa-check');

            let rightOuter = $('<div/>',{
                class:'symbol d-inline-block'
            }).append(iCheck);

            let wrongInner = $('<div/>',{
                class:`wrongInner d-inline-block mr-2 ${((s.header) ? 'h4' : '')}`,
                style:'margin-right: .' + (s.isMobile ? 2:5) + 'rem !important;'
            }).html(s.wrong.toString());

            let wrontOuter = $('<div/>',{
                class:`symbol d-inline-block ${((s.header) ? 'h4' : '')}`
            }).append($('<i/>').addClass('fas fa-times'));

            let right = $('<div/>',{
                class:`d-inline-block mr-2 text-success ${((s.header) ? 'h4' : '')}`,
                id:'right',
                style:'margin-right: .' + (s.isMobile ? 2:5) + 'rem !important;'
            }).append(rightInner, rightOuter);

            let wrong = $('<div/>',{
                class:`d-inline-block mr-2 text-danger ${((s.header) ? 'h4' : '')}`,
                id:'wrong',
                style:'margin-left: .' + (s.isMobile ? 2:5) + 'rem !important; margin-right: .' + (s.isMobile ? 0:5) + 'rem !important;'
            }).append(wrongInner, wrontOuter);

            $(s.target).append(right, '<div class="d-inline-block">|<div/>', wrong)

            setInterval(function () {

                $('.rightInner').html(s.right.toString());
                $('.wrongInner').html(s.wrong.toString());
            }, 2000);
        }
    }

    private _spanRight: JQuery<HTMLElement>;
    private _spanWrong: JQuery<HTMLElement>;

    public get spanRight(): JQuery<HTMLElement> {
        return this._spanRight;
    }

    public set spanRight(value: JQuery<HTMLElement>) {
        this._spanRight = value;
    }

    public get spanWrong(): JQuery<HTMLElement> {
        return this._spanWrong;
    }

    public set spanWrong(value: JQuery<HTMLElement>) {
        this._spanWrong = value;
    }

    public getHtmlRight(): JQuery<HTMLElement> {

        const s = this;
        let divCorrect: JQuery<HTMLElement> = BS.div('d-inline-block text-success');
        let spanIconRight: JQuery<HTMLElement> = BS.span().append(Fa.solid('fa-check'))
        s.spanRight = BS.span().html(s.right.toString());
        divCorrect.append(spanIconRight, s.spanRight);

        let divWrong: JQuery<HTMLElement> = BS.div('d-inline-block text-danger');
        let spanIconWrong: JQuery<HTMLElement> = BS.span().append(Fa.solid('fa-times'))
        s.spanWrong = BS.span().html(s.wrong.toString());
        divWrong.append(spanIconWrong, s.spanWrong);

        return divCorrect;
    }

    public getHtmlWrong(): JQuery<HTMLElement> {

        const s = this;

        let divWrong: JQuery<HTMLElement> = BS.div('d-inline-block text-danger');
        let spanIconWrong: JQuery<HTMLElement> = BS.span().append(Fa.solid('fa-times'))
        s.spanWrong = BS.span().html(s.wrong.toString());
        divWrong.append(s.spanWrong, spanIconWrong);

        return divWrong;
    }

    /**
     *  Adds 1 to s.right
     */
    public addRight() {

        const s = this;

        s.right++;
        $('.rightInner').html(s.right.toString());

        if(s.spanRight !== undefined) {
            s.spanRight.html(s.right.toString())
        }

        s.playRight();
    };

    /**
     *  Adds 1 to s.wrong
     */
    public addWrong() {

        let s = this;
        s.wrong++;
        $('.wrongInner').html(s.wrong.toString());

        if(s.spanWrong !== undefined) {
            s.spanWrong.html(s.wrong.toString())
        }
        // s.playWrong();
    };

    /**
     * Resets SCORE
     * @param {number} right
     * @param {number} wrong
     */
    public reset(right:number = 0, wrong: number = 0) {

        let s = this;
        s.right = right;
        s.wrong = wrong;
        s.init();
    };

    /**
     * Plays good.mp3
     */
    public playRight() {

        let s = this;
        s.audio.right.play();
    };

    /**
     * Plays false.mp3
     */
    public playWrong() {

        let s = this;
        s.audio.wrong.play();
    }
}
