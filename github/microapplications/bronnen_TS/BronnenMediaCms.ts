import { BronnenMedia } from "./BronnenMedia";
import {BsFa6 as Bs} from "./bootstrap/BsFa6";
import bootbox from 'bootbox6'

export class BronnenMediaCms extends BronnenMedia{
    private _callback: any;

    private _mediaObj: any;
    private _fetching: boolean = false;
    private _ready: boolean = false;

    private _bb: any;
    
    constructor(language: string = "nl_NL", callback: any = () => { }){
        super(language);

        this.callback = callback;
        this.mediaObj = this.getMediaArray();
    }

    public changeLanguage(language: string) {
        const s = this;
        s.language = language;

        // refresh data
        s.mediaObj = s.getMediaArray();
    }

    public get callback(): any {
        return this._callback;
    }

    public set callback(value: any) {
        this._callback = value;
    }

    public get mediaObj(): any {
        return this._mediaObj;
    }

    public set mediaObj(value: any) {
        this._mediaObj = value;
    }

    public get images() {
        return this.mediaObj.image.files;
    }

    public get audio() {
        return this.mediaObj.audio.files;
    }

    public get ready(): boolean {
        return this._ready;
    }

    private set ready(value: boolean) {
        this._ready = value;
    }

    public get fetching(): boolean {
        return this._fetching;
    }

    private set fetching(value: boolean) {
        this._fetching = value;
    }

    public get bb(): any {
        return this._bb;
    }
    public set bb(value: any) {
        this._bb = value;
    }

    private getMediaArray() {
        const s = this;
        let head = {
            language: s.language
        };

        s.ready = false;
        s.fetching = true;

        if(s.bb === undefined) {
            
            s.bb = bootbox.dialog({
                message: Bs.jQEl("i", "fa-solid fa-pulse fa-spinner co"),
                onShown: (e) => {
                    
                    setInterval(() => {
                        if(s.fetching === false) {
                            s.bb.modal('hide')
                        }
                    }, 200);
                },
                closeButton: false
            });
        }
        else {
            s.bb.modal('show')
        }

        $.ajax({
            url: "https://media.fcsprint2.nl/api/media/fileList",
            type: "post",
            data: head,
            dataType: 'json'
        }).done(function (response) {
            s.fetching = false;
            s.mediaObj = response;
            s.ready = true;

            s.callback();

        }).fail(function (jqXHR, textStatus, errorThrown) {
            s.fetching = false;
            // console.log(jqXHR);
            // console.log(textStatus);
            // console.log(errorThrown);
        });
    }

    
    /**
     *
     * @param value
     * @return {string}
     */
    public findImage(value: string) {
        const s = this;
        if (!/\.jpg$/.test(value)) {
            value += '.jpg';
        }

        let foundIndex: number = s.mediaObj.image.lowercase.indexOf(value.toLowerCase());
        if (foundIndex === -1 && value.indexOf('_') > -1) {

            foundIndex = s.mediaObj.image.lowercase.indexOf(value.toLowerCase().replace('_', ' '));
        }

        if (foundIndex > -1) {

            return s.mediaObj.image.files[foundIndex];
        }

        return '';
    }
    
    /**
     *
     * @param value
     * @return {string}
    */
    public findAudio(value: string) {
        const s = this;

        if (!/\.mp3$/.test(value)) {
            value += '.mp3';
        }

        let foundIndex: number = s.mediaObj.audio.lowercase.indexOf(value.toLowerCase());

        if (foundIndex === -1 && value.indexOf('_') > -1) {

            foundIndex = s.mediaObj.audio.lowercase.indexOf(value.toLowerCase().replace('_', ' '));
        }
        
        if (foundIndex > -1) {
            
            return s.mediaObj.audio.files[foundIndex];
        }
        
        return '';
    }
    
   /**
     * @param {JQuery<HTMLElement>} elmt
     * @param {(event, ui) => void} select
     * @return {JQuery<HTMLElement>}
     */
    public autocompleteAudio(elmt: JQuery<HTMLElement>, select = (event, ui): void => { console.log(event); console.log(ui)}): JQuery<HTMLElement> {
        const s = this;

        (<any>$(elmt)).autocomplete({
            source: function (request, response) {
                let filteredArray = $.map(s.audio, function (item) {

                    if (item.toLowerCase().trim().startsWith(request.term.toLowerCase()) || item.toLowerCase().startsWith(request.term.toLowerCase())) {
                        return item;
                    }
                    else {
                        return null;
                    }
                });
                let results = (<any>$).ui.autocomplete.filter(filteredArray, request.term);
                response(results.slice(0, 10));
            },
            select: select,
            minLength: 2
        });

        return elmt;
    }

    /**
     * @param {JQuery<HTMLElement>} elmt
     * @param {(event, ui) => void} select
     * @return {JQuery<HTMLElement>}
     */
    public autocompleteImage(elmt: JQuery<HTMLElement>, select = (event, ui): void => { console.log(event); console.log(ui)}): JQuery<HTMLElement> {
        const s = this;

        (<any>$(elmt)).autocomplete({
            source: function (request, response) {
                let filteredArray = $.map(s.images, function (item) {

                    if (item.toLowerCase().trim().startsWith(request.term.toLowerCase()) || item.toLowerCase().startsWith(request.term.toLowerCase())) {
                        return item;
                    }
                    else {
                        return null;
                    }
                });
                let results = (<any>$).ui.autocomplete.filter(filteredArray, request.term);
                response(results.slice(0, 10));
            },
            select: select,
            minLength: 2
        });

        return elmt;
    }

    public select(): JQuery<HTMLElement> {
        const s = this;
        let select: JQuery<HTMLElement> = Bs.select();

        $.ajax({
            url: "/api/languages/unique",
            method: "GET",
            dataType: 'json'
        }).done((response): void => {

            response.forEach((lang): void => {

                let option: JQuery<HTMLElement> = Bs.option('', {
                    value: lang.isoCode
                }).html(`${lang.fullname} (${lang.isoCode})`)

                if(lang.isoCode === s.language) {
                    option.attr({
                        selected: 'selected'
                    });
                }

                select.append(option);
            });

            select.on('change', (): void => {
                s.changeLanguage(select.val().toString());
            });
        }).fail((jqXHR: JQuery.jqXHR<any>, textStatus: JQuery.Ajax.ErrorTextStatus, errorThrown: string): void =>
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });

        return select;
        
    }

    

    
}