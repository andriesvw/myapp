/**
 *  let t = new TIMER([ID]/[CLASS]);
 *  (object) target is the element on which the timer will be displayed
 */
import {Progress} from "../progress/Progress";
import $ from "jquery";
import { BsFa6 as BS } from "./bootstrap/BsFa6";
import { FontAwesome as Fa } from "./FontAwesome";

export class Timer {


    private _target: JQuery;
    private _progress: Progress = null;
    private _timer;
    private _sec: number;
    private _isStopped: boolean = true;
    private _isMobile: boolean = false;
    private pad;
    private _header: boolean = true;
    private _options: {header?: boolean};
    /**
     *
     * @param {jQuery} target
     * @param {Progress} progress
     */
    constructor(target: string|JQuery<HTMLElement> = null, progress: Progress = null, options: {header?: boolean} = {}) {

        this.options = options;
        this.target = target;
        this.progress = progress;
        this.timer = false;
        this.sec = 0;
        this.isstopped = true;
        this.isMobile = false;
        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
            this.isMobile = true;
        }

        this.pad = (val: number, bool: boolean)  => {

            return val > 9 ? val.toString() : '0' + val.toString();
        };

        this.init();
    }

    public get options(): { header?: boolean } {
        return this._options;
    }

    public set options(value: { header?: boolean }) {
        this._options = value;
    }

    /**
     *
     * @return {boolean}
     */
    public get header(): boolean {

        if(this.options.header === undefined) {
            this.options.header = true;
        }
        return this.options.header;
    }

    /**
     *
     * @param {boolean} value
     */
    public set header(value: boolean) {
        this.options.header = value;
    }

    /**
     * @return {JQuery}
     */
    public get target(): JQuery {
        return this._target;
    }

    /**
     * @param {string|JQuery} target
     */
    public set target(target: string|JQuery) {
        if(typeof target === 'string') {
            this._target = $(target);
        }
        else {
            this._target = target;
        }
    }

    /**
     * @return {null|Progress}
     */
    public get progress(): Progress {
        return this._progress;
    }

    /**
     * @param {Progress} progress
     */
    public set progress(progress: Progress) {
        this._progress = progress;
    }

    public get timer() {
        return this._timer;
    }

    public set timer(timer) {
        this._timer = timer
    }

    /**
     *
     * @return {number}
     */
    get sec(): number {
        return this._sec;
    }

    /**
     *
     * @param {number} sec
     */
    set sec(sec: number) {
        this._sec = sec;
    }

    /**
     *
     * @return {boolean}
     */
    get isstopped(): boolean {
        return this._isStopped;
    }

    /**
     *
     * @param {boolean} stopped
     */
    set isstopped(stopped: boolean) {
        this._isStopped = stopped;
    }

    /**
     *
     * @return {boolean}
     */
    get isMobile(): boolean {
        return this._isMobile;
    }

    /**
     *
     * @param {boolean} isMobile
     */
    set isMobile(isMobile: boolean) {
        this._isMobile = isMobile;
    }

    init(){

        let s = this;

        s.sec = 0;
        if(s.target !== null) {
            $(s.target).html('');
            let minutes = $('<div/>', {
                id: 'minutes',
                class: `d-inline-block ${((s.header) ? 'h4' : '')}`
            }).html(s.pad(parseInt((s.sec / 60).toString()), true).toString());

            let split = $('<span/>', {
                class: `pl-1 pr-1 ${((s.header) ? 'h4' : '')}`
            }).html(':');

            let seconds = $('<div/>', {
                id: 'seconds',
                class: `d-inline-block ${((s.header) ? 'h4' : '')} me-2`
            }).html(s.pad((s.sec % 60).toString(), false));

            $(s.target).append(minutes, split, seconds);
        }
    }

    start() {

        const s = this;

        if (s.timer === false) {

            s.isstopped = false;

            s.timer = setInterval(() => {

                let sec = s.pad(parseInt((++s.sec % 60).toString()), false).toString();
                let min = s.pad(parseInt((s.sec / 60).toString()), true).toString();
                    $("#seconds").html(sec);
                    $("#minutes").html(min);

                    if(s.spanMinutes !== undefined && s.spanSeconds !== undefined) {
                        s.spanMinutes.html(min);
                        s.spanSeconds.html(sec);
                    }
                }, 1000);

            // Add start-event
            if(s.progress !== null) {

                s.progress.start();
            }
        }
    }

    stop() {

        let s = this;
        let lastM = s.pad(parseInt((s.sec / 60).toString()), true);
        let lastS = s.pad(parseInt((s.sec % 60).toString()), false);
        clearInterval(s.timer);

        s.timer = setInterval(function () {

            $("#seconds").html(lastS);
            $("#minutes").html(lastM);

            if(s.spanMinutes !== undefined && s.spanSeconds !== undefined) {

                s.spanMinutes = BS.span().html(lastM.toString());
                s.spanSeconds = BS.span().html(lastS.toString());
            }
        }, 2000);

        // 2022-03-08 removed. Should be invoked from micro-app
        // Consider invoke Progress.start also from micro-app?
        // // Add stop-event
        // if(s.progress !== null) {
        //     s.progress.complete();
        // }
        s.isstopped = true;
    }

    /**
     *  Stops TIMER and reinitiates it
     */
    reset() {

        let s = this;
        s.stop();
        s.init();
    };

    /**
     * returns (string) with current min and sec
     */
    getTime(): string {

        let s = this;

        return s.pad(parseInt((s.sec / 60).toString()),true).toString() + s.pad(parseInt((s.sec % 60).toString()),false).toString();
    }

    /*
     * New Methods Timer
     */
    private _spanMinutes: JQuery<HTMLElement>;
    private _spanSeconds: JQuery<HTMLElement>;

    public get spanMinutes(): JQuery<HTMLElement> {
        return this._spanMinutes;
    }

    public set spanMinutes(value: JQuery<HTMLElement>) {
        this._spanMinutes = value;
    }

    public get spanSeconds(): JQuery<HTMLElement> {
        return this._spanSeconds;
    }

    public set spanSeconds(value: JQuery<HTMLElement>) {
        this._spanSeconds = value;
    }

    public html() {

        const s = this;
        let div = BS.div();
        console.log(s.sec)
        s.spanMinutes = BS.span().html(s.pad(parseInt((s.sec / 60).toString()), true).toString());
        s.spanSeconds = BS.span().html(s.pad((s.sec % 60).toString(), false).toString());
        div.append(s.spanMinutes, ':', s.spanSeconds);

        return div;
    }
}
