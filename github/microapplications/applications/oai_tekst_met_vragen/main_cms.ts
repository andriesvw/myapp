/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 11-11-20 09:21
 * Filename: main_cms
 */

require('jquery-ui-dist/jquery-ui.theme.css');

import './css/cms.css';
import '../../bronnen_TS/main-bs5-fa6.scss'
const $ = require('jquery');
import 'jquery-ui';
import 'bootstrap5';
import 'fontawesome6/js/all';
import {OaiTekstMetVragenCms} from "./ts/OaiTekstMetVragenCms";

$((): void => {
    const app = new OaiTekstMetVragenCms();
});