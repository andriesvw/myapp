
import {v4 as uuidv4} from 'uuid';
import {OaiTekstMetVragen} from "./OaiTekstMetVragen";
// import {QuestionFrontend as Question} from "./QuestionFrontend";

import { BronnenShadowBox } from "../../../bronnen_TS/BronnenShadowBox";

import {Chat} from "../../../bronnen_TS/chat/Chat";
import {Message, messageDataType} from "../../../bronnen_TS/chat/Message";
import {BsFa6} from "../../../bronnen_TS/bootstrap/BsFa6";
import {FontAwesome as Fa} from "../../../bronnen_TS/FontAwesome";

import {StarRating} from "../../../bronnen_TS/StarRating";
import {User} from "../../../bronnen_TS/chat/User";
import {QuestionFrontend} from "./QuestionFrontend";
import $, {data} from "jquery";


export class OaiTekstMetVragenFrontend extends OaiTekstMetVragen {

    private _chat: Chat;
    private _appUser: User;
    private _index: number;
    private _verifyUrl: string = '/openai_v4/v4';
    private _starRating: StarRating;
    private _questFes: QuestionFrontend[] = [];

    constructor() {
        super();

        this.init();
    }

    public get index(): number {
        return this._index;
    }

    public set index(value: number) {
        this._index = value;
    }

    public get chat(): Chat {
        return this._chat;
    }

    public set chat(value: Chat) {
        this._chat = value;
    }

    public get appUser(): User {
        return this._appUser;
    }

    public set appUser(value: User) {
        this._appUser = value;
    }

    public get questFes(): QuestionFrontend[] {
        return this._questFes;
    }

    public set questFes(value: QuestionFrontend[]) {
        this._questFes = value;
    }

    protected init() {

        const s = this;
        let container: JQuery<HTMLElement> = BsFa6.container('container');
        $('body').append(container);

        s.chat = new Chat(container);
        let uuid = uuidv4();
        let appName = this.getCurrentApplication();
        this.appUser = new User({
            id: uuid,
            uuid: uuid,
            name: appName,
            email: appName,
            roles: ['ROLE_APPLICATION']
        }, s.chat);

        s.getJson((response) => {

            s.app = response;
            s.start();
        });
    }

    protected start() {

        const s = this;

        document.title = s.title;

        s.questions.forEach((question: string, i: number) => {
            let q: QuestionFrontend = new QuestionFrontend(s, i, question);
            s.questFes.push(q);
        })

        s.chat.addMessage(s.getTitleMessage());

        setTimeout(() => {

            s.chat.addMessage(s.getContentMessage());
        }, 200);

        setTimeout(() => {

            s.nextQuestion();

        }, 400);
    }

    public nextQuestion() {

        const s = this;
        if(s.index === undefined || s.index === null) {
            s.index = 0
        }
        else {
            s.index++;
        }

        if(s.questFes[s.index] !== undefined) {
            s.questFes[s.index].ask();
        }
        else {
            //finished
        }
    }


    protected getTitleMessage(): Message {

        const s = this;

        let data: messageDataType = {
            user: s.appUser,
            content: s.title,
            type: 'title'
        };
        let message: Message = new Message(data, {
            header: 'h3'
        });

        return message;
    }

    protected getContentMessage(): Message {

        const s = this;
        let data: messageDataType = {
            user: s.appUser,
            content: s.getMainText(),
            type: 'content'
        };

        let message: Message = new Message(data, {
                        anchor: 'mainAnchor',
                        size: '1.2rem',
                        icon: false,
                        width: 'col-xs-12 col-sm-12 col-md-12 col-lg-10'
                    });

        return message;
    }

    public getMainText(): JQuery<HTMLElement> {

        const s = this;
        let row: JQuery<HTMLElement> = BsFa6.row();
        let col: JQuery<HTMLElement> = BsFa6.col('col-xs-12 col-sm-12 col-md-8');
        let rowText: JQuery<HTMLElement> = BsFa6.row();

        s.getTextLines().forEach((line) => {
            let colLine: JQuery<HTMLElement> = BsFa6.col('col-xs-12 col-sm-12 col-md-12');
            colLine.append(line);
            rowText.append(colLine)
        })

        col.append(rowText);
        row.append(col);

        if(s.image !== '' || s.audio !== '') {
            let colMedia: JQuery<HTMLElement> = BsFa6.col('col-xs-12 col-sm-12 col-md-4 text-end');
            let box: BronnenShadowBox = new BronnenShadowBox(s);

            if(s.image !== '')
                box.addImage(`https://media.fcsprint2.nl/media/${s.language}/img/` + s.image);

            if(s.audio !== '')
                box.addAudio(`https://media.fcsprint2.nl/media/${s.language}/audio/` + s.audio);

            colMedia.append(box.card);
            row.append(colMedia);
        }

        return row;
    }

    public get starRating(): StarRating{
        return this._starRating;
    }
    public set starRating(value: StarRating) {
        this._starRating = value;
    }

    public get verifyUrl(): string {
        return this._verifyUrl;
    }

    public set verifyUrl(value: string) {
        this._verifyUrl = value;
    }

    // protected finish() {
    //
    //     const s = this;
    //     let row: JQuery<HTMLElement> = BsFa6.row()
    //     let colScore: JQuery<HTMLElement> = BsFa6.col('col-12 ps-3').append($(s.chat.score.target).clone().html())
    //     let colTime: JQuery<HTMLElement> = BsFa6.col('col-12').append(
    //         Fa.solid('fa-stopwatch me-1'),
    //         $(s.chat.timer.target).clone().html())
    //     row.append(colScore, colTime)
    //
    //     let message: Message = new Message({
    //         user: s.parent.assistant,
    //         content: row,
    //         type: 'finished'
    //     })
    //
    //     s.chat.addMessage(message);
    // }
    // /**
    //  *
    //  * @protected
    //  */
    // protected init(): void {
    //
    //     let s = this;
    //     s.setUserCallback();
    //
    //     document.title = s.title;
    //
    //     let time = 250;
    //     let add = 150;
    //
    //     setTimeout(() => {
    //
    //         let message: Message = new Message({
    //                 user: s.parent.assistant,
    //                 content: s.title
    //         }, {
    //             color: ' bg-primary',
    //             text: 'white',
    //             header: 'h2',
    //             icon: false,
    //             width: 'col-xs-12 col-sm-12 col-md-12 col-lg-10'
    //         })
    //         s.parent.chat.addMessage(message);
    //     }, time)
    //
    //     time += add;
    //     setTimeout(() => {
    //
    //         let message: Message = new Message({
    //             user: s.parent.assistant,
    //             content: s.getMainText()
    //         }, {
    //             anchor: 'maintextanchor',
    //             size: '1.2rem',
    //             icon: false,
    //             width: 'col-xs-12 col-sm-12 col-md-12 col-lg-10'
    //         })
    //         s.parent.chat.addMessage(message);
    //     }, time)
    //
    //     time += add;
    //     setTimeout(() => {
    //
    //         let message: Message = new Message({
    //             user: s.parent.assistant,
    //             type: 'question',
    //             content: s.questions[s.index]
    //         })
    //         s.parent.chat.addMessage(message);
    //         // clearInterval(int);
    //     }, time);
    //
    //     s.starRating = new StarRating(s.app, s.chat.timer, s.chat.score, s.questions.length);
    // }
    //
    //
    //
    //
    // protected get callback() {
    //     const s = this;
    //     return function(data){
    //
    //         $.ajax({
    //             url: s.verifyUrl,
    //             data: {
    //                 text: data.text,
    //                 question: data.question,
    //                 answer: data.answer
    //             },
    //             method: 'post',
    //             dataType: 'json'
    //         })
    //             .done((response): void => {
    //                 console.log(response);
    //
    //                 // s.handleResponse(response);
    //             })
    //             .fail((jqXHR: JQuery.jqXHR, textStatus: JQuery.Ajax.ErrorTextStatus, errorThrown: string): void => {
    //                 console.log(jqXHR)
    //                 console.log(textStatus)
    //                 console.log(errorThrown)
    //
    //                 // s.setAlert('primary');
    //             });
    //     }
    // }
    // /**
    //  * @protected
    //  */
    // protected checkAnswer(data) {
    //
    //     const s = this;
    //     return (data)=> {
    //
    //         $.ajax({
    //             url: s.verifyUrl,
    //             data: {
    //                 text: data.text,
    //                 question: data.question,
    //                 answer: data.answer
    //             },
    //             method: 'post',
    //             dataType: 'json'
    //         })
    //             .done((response): void => {
    //                 console.log(response);
    //
    //                 // s.handleResponse(response);
    //             })
    //             .fail((jqXHR: JQuery.jqXHR, textStatus: JQuery.Ajax.ErrorTextStatus, errorThrown: string): void => {
    //                 console.log(jqXHR)
    //                 console.log(textStatus)
    //                 console.log(errorThrown)
    //
    //                 // s.setAlert('primary');
    //             });
    //     }
    // }

}