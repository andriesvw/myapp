/*
 * FC-Sprint2 project => TekstLezen.js
 *
 * The project "diglin.eu" is property of FC-Sprint2
 *
 * Created at:
 * 23 sep. 2019 @ 09:08:24
 *
 * Created by:
 * Andries van Weeren
 * a.weeren@fcroc.nl
 */
/*
 * General dependencies
 */

import {AbstractApplication} from "../../../bronnen_TS/AbstractApplication";

export class OaiTekstMetVragen extends AbstractApplication {

    private _app: any;

    constructor() {
        super();

        const s = this;

        s.app = {
            id: 'new',
            exercisetitle: '',
            applicationname: s.getCurrentApplication(),
            language: 'nl_NL',
            main_object: {
                image: '',
                audio: '',
                text: '',
                questions: []
            }
        };
        s.id = s.getUrlParameter('id');
    }

    /**
     * @return {number | string}
     */
    public get id(): number | string {
        return this.app.id;
    }

    /**
     * @param {string | number} value
     */
    public set id(value: string | number) {
        this.app.id = value;
    }

    /**
     *
     * @return {string}
     * @protected
     */
    public get title(): string {
        return this.app.exercisetitle;
    }

    /**
     *
     * @param {string} value
     * @protected
     */
    public set title(value: string) {
        this.app.exercisetitle = value;
        document.title = this.title;
    }

    /**
     *
     * @return {string}
     * @protected
     */
    public get language(): string {
        return this.app.language;
    }

    /**
     *
     * @param {string} value
     * @protected
     */
    public set language(value: string) {
        this.app.language = value;
    }

    public get app(): any {
        return this._app;
    }

    public set app(value: any) {
        this._app = value;
    }

    public get image(): string {
        return this._app.main_object.image;
    }

    public set image(value: string) {
        this._app.main_object.image = value;
    }

    public get audio(): string {
        return this._app.main_object.audio;
    }

    public set audio(value: string) {
        this._app.main_object.audio = value;
    }

    public get questions(): string[] {
        return this.app.main_object.questions;
    }

    public set questions(value: string[]) {
        this.app.main_object.questions = value;
    }

    public get text(): string {
        return this.app.main_object.text;
    }

    public set text(value: string) {
        this.app.main_object.text = value;
    }

    public getTextLines(): string[] {

        return this.text.split(/\n/g);
    }
}