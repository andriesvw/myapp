/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 01/03/2024 13:10
 * Filename: Question
 */

import {OaiTekstMetVragenCms} from "./OaiTekstMetVragenCms";
import {BsFa6 as Bs} from "../../../bronnen_TS/bootstrap/BsFa6";

/**
 * @class QuestionsCms
 */
export class QuestionsCms {

    private _parent: OaiTekstMetVragenCms;
    private _questions: string[];
    private _col: JQuery<HTMLElement>;

    constructor(parent: OaiTekstMetVragenCms, questions: string[] = []) {

        this.parent = parent;
        this.questions = questions;

        this.init();
    }

    public get parent(): OaiTekstMetVragenCms {
        return this._parent;
    }

    public set parent(value: OaiTekstMetVragenCms) {
        this._parent = value;
    }

    public get questions(): string[] {
        return this._questions;
    }

    public set questions(value: string[]) {
        this._questions = value;
    }

    public get col(): JQuery<HTMLElement> {
        return this._col;
    }

    public set col(value: JQuery<HTMLElement>) {
        this._col = value;
    }

    public init(): void {

        const s = this;

        s.col = Bs.col('col-md-12 mb-2');

        s.col.append(s.textarea());
    }

    protected textarea(): JQuery<HTMLElement> {

        const s = this;

        let textarea: JQuery<HTMLElement> = Bs.textarea('form-control', {
            class: 'form-control',
            rows: '20'
        }).on('input', (): void => {

            s.questions = textarea.val().toString().split(/\n/g);
        }).text(s.questions.join("\n"));

        return textarea;
    }
}
