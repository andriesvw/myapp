/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 01/03/2024 13:10
 * Filename: Question
 */

import {OaiTekstMetVragenFrontend} from "./OaiTekstMetVragenFrontend";
import {Chat} from "../../../bronnen_TS/chat/Chat";
import {User} from "../../../bronnen_TS/chat/User";
import {Message, messageDataType} from "../../../bronnen_TS/chat/Message";
import $ from "jquery";

/**
 * @class QuestionFrontend
 */
export class QuestionFrontend {

    private _parent: OaiTekstMetVragenFrontend;
    private _index: number;
    private _question: any;
    private _answer: string;
    private _answers: string[] = [];
    private _counted: boolean = false;
    private _correct: boolean = false;
    private _onAnswer: any;

    constructor(parent: OaiTekstMetVragenFrontend, index: number, question: string) {

        this.parent = parent;
        this.index = index;
        this.question = question;
    }

    public getMessage(): Message {

        const s = this;
        let data: messageDataType = {
            user: s.chat.assistent,
            content: s.question,
            type: 'question'
        };

        return new Message(data);
    }

    public ask(): void {

        const s = this;

        s.chat.addMessage(s.getMessage());
        s.setUserCallback();
    }

    public get chat(): Chat {
        return this.parent.chat;
    }

    public get assistent(): User {
        return this.parent.chat.assistent;
    }

    public get parent(): OaiTekstMetVragenFrontend {
        return this._parent;
    }

    public set parent(value: OaiTekstMetVragenFrontend) {
        this._parent = value;
    }

    public get index(): number {
        return this._index;
    }

    public set index(value: number) {
        this._index = value;
    }

    public get question(): any {
        return this._question;
    }

    public set question(value: any) {
        this._question = value;
    }

    public get answer(): string {
        return this._answer;
    }

    public set answer(value: string) {
        this._answer = value;
    }

    public get answers(): string[] {
        return this._answers;
    }

    public set answers(value: string[]) {
        this._answers = value;
    }

    public get counted(): boolean {
        return this._counted;
    }

    public set counted(value: boolean) {
        this._counted = value;
    }

    public get correct(): boolean {
        return this._correct;
    }

    public set correct(value: boolean) {
        this._correct = value;
    }

    public get onAnswer(): any {
        return this._onAnswer;
    }

    public set onAnswer(value: any) {
        this._onAnswer = value;
    }



    public setUserCallback(): void {

        const s = this;
        s.chat.user.callback = (answer: Message): void => {

            $.ajax({
                url: s.parent.verifyUrl,
                data: {
                    text: s.parent.text,
                    question: s.question,
                    answer: answer.content
                },
                method: 'post',
                dataType: 'json'
            })
            .done((response): void => {
                console.log(response);
                if(typeof answer.content === 'string') {
                    s.answers.push(answer.content);
                }
                else {
                    s.answers.push(answer.content.text());
                }

                if(response.correct) {
                    answer.correct = true;
                    s.correct = true;
                    s.counted = true;
                    s.chat.score.addCorrect();

                    let time: number = 400;

                    if(response.alternative !== undefined && response.alternative !== '') {

                        setTimeout(() => {

                            let data: messageDataType = {
                                user: s.chat.assistent,
                                content: response.alternative,
                                type: 'alternative'
                            }
                            let mess: Message = new Message(data);
                            mess.color = 'success';
                            s.chat.addMessage(mess);
                        }, time);
                        time += 400;
                    }

                    setTimeout(() => {

                        s.parent.nextQuestion();
                    }, 800)
                }
                else {
                    answer.correct = false;
                    s.correct = false;

                    if(!s.counted) {
                        s.chat.score.addWrong();
                    }

                    s.counted = true;

                    if(s.answers.length > 1) {
                        console.log('GIVE ANSWER')
                        let data: messageDataType = {
                            user: s.chat.assistent,
                            content: ((response.alternative !== undefined && response.alternative !== '') ? response.alternative : response.line),
                            type: 'givenAnswer'
                        }
                        let mess: Message = new Message(data);
                        s.chat.addMessage(mess);

                        setTimeout(() => {

                            s.parent.nextQuestion();
                        }, 400)
                    }
                }
                console.log(s)
            })
            .fail((jqXHR: JQuery.jqXHR, textStatus: JQuery.Ajax.ErrorTextStatus, errorThrown: string): void => {
                console.log(jqXHR)
                console.log(textStatus)
                console.log(errorThrown)
            });
        }
    }
}