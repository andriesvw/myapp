/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 10/07/2023 12:39
 * Filename: BaseMicroAppBlock.ts
 */

import {AppBlock} from "../../../bronnen_TS/AppBlock";

/**
 * @class OpenAiTestBlock
 * @extends AppBlock
 */

export class OaiTekstMetVragenBlock extends AppBlock {

    private _card: JQuery;

    /**
     * @param {any} parent
     * @param {number} index
     * @param {any} data
     */
    public constructor(parent: any, index: number, data: any) {
        super(parent, index, data);

        this.init();
    }

    /**
     * @protected
     */
    protected init(){

        let s = this;
        s.card = s.htmlCard();
    }

    /**
     * @return {JQuery}
     */
    public get card(): JQuery {
        return this._card;
    }

    /**
     * @param {JQuery} value
     */
    public set card(value: JQuery) {
        this._card = value;
    }

    /**
     * @return {JQuery}
     * @protected
     */
    protected htmlCard(): JQuery {

       let s = this;
       let div = $('<div/>', {
           class: 'card card-block border border-secondary mb-3'
       }).on('click', (e) => {
           e.stopPropagation();

           s.parent.toggleActiveBlock(s.card);
       });

       div.append(s.htmlHeader(), s.htmlBody(), s.htmlFooter());

       return div;
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    protected htmlHeader(): JQuery {

        let s = this;
        let div = $('<div/>', {
            class: 'card-header'
        });

        div.append(s.index.toString());

        return div;
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    protected htmlBody(): JQuery {

        let s = this;
        let div = $('<div/>', {
            class: 'card-body'
        });

        div.append(
            s.contentInputGroup(),
            s.audioInputGroup(),
            s.imageInputGroup()
        );

        return div;
    }

    /**
     * @return {JQuery}
     * @protected
     */
    protected htmlFooter(): JQuery {

        let s = this;
        let div = $('<div/>', {
            class: 'card-footer text-end'
        });

        div.append(s.controls());

        return div;
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    protected controls(): JQuery{

        let s = this;
        let btnGroup = $('<div/>', {
            class: 'btn-group btn-group-sm'
        });

        let removeBtn = $('<button/>', {
            class: 'btn btn-outline-danger'
        }).on('click', () => {

            s.removeBlock();
        });

        let removeIcon = $('<i/>', {
            class: 'fa-solid fa-trash-can fa-fw'
        });

        removeBtn.append(removeIcon);
        btnGroup.append(removeBtn);

        return btnGroup;
    }

    /**
     * Removes current block-data and -html
     * @protected
     */
    protected removeBlock(): void {

        let s = this;
        s.index = s.card.parent().index();

        // Remove current card HTML.
        s.card.parent().remove();

        // Remove block-object from partent.blocks-array
        s.parent.removeBlock(s.index);
    }

    /**
     *
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected contentInputGroup(){

        let s = this;
        let div = $('<div/>', {
            class: 'input-group mb-2'
        });
        let input = $('<input/>', {
            type: 'text',
            class: 'form-control input-text',
            placeholder: 'Text',
            value: s.data.content
        }).on('input', () => {

            // TODO Event on Input input-text
        });

        // TODO if media-inputs is empty and image and or audio exists fill it in the corresponding .input-image and .input-audio

        let span = $('<span/>', {
            class: 'input-group-text'
        }).html('<i class="fa-solid fa-header fa-fw fa-t"></i>');

        div.append(input, span);

        return div;
    }

    /**
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected imageInputGroup(){

        let s = this;
        let div = $('<div/>', {
            class: 'input-group mb-2'
        });
        let input = $('<input/>', {
            type: 'text',
            class: 'form-control input-image',
            placeholder: 'Image',
            value: s.data.image
        }).on('input', () => {

            // TODO Initiate jquery-ui Autocomplete on current input
        });

        // TODO If media exists, button requires .btn-outline-primary + on click event should play/show media
        // TODO if media NOT exists/or empty -> disabled button

        let button = $('<span/>', {
            class: 'btn btn-outline-secondary btn-image'
        }).on('click', () => {

            // TODO Event on Click media-button
            console.log('Show image: ' + input.val());
        }).html('<i class="fa-solid fa-fw fa-camera"></i>');

        if(s.data.audio === '') {
            button
                .addClass('disabled')
                .attr({disabled: 'disabled'});
        }

        div.append(input, button);

        return div;
    }

    /**
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected audioInputGroup(){

        let s = this;
        let div = $('<div/>', {
            class: 'input-group mb-2'
        });
        let input = $('<input/>', {
            type: 'text',
            class: 'form-control input-audio',
            placeholder: 'Audio',
            value: s.data.audio
        }).on('input', () => {

            // TODO Initiate jquery-ui Autocomplete on current input
        });

        // TODO If media exists, button requires .btn-outline-primary + on click event should play/show media
        // TODO if media NOT exists/or empty -> disabled button

        let button = $('<span/>', {
            class: 'btn btn-outline-secondary btn-audio'
        }).on('click', () => {

            // TODO Event on Click media-button
            console.log('Play audio: ' + input.val());
        }).html('<i class="fa-solid fa-fw fa-volume-up"></i>');

        if(s.data.audio === '') {
            button
                .addClass('disabled')
                .attr({disabled: 'disabled'});
        }

        div.append(input, button);

        return div;
    }
}