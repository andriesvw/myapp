/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsprint2
 * Created: 04/03/2024 12:43
 * Filename: Answer
 */

import {OaiTekstMetVragenFrontend} from "./OaiTekstMetVragenFrontend";
import {QuestionFrontend} from "./QuestionFrontend";
import $ from "jquery";
import {BsFa6 as BS} from "../../../bronnen_TS/bootstrap/BsFa6";
import { FontAwesome as Fa} from "../../../bronnen_TS/FontAwesome";

export type data = { correct: boolean; line: string; spelling: boolean; words: string[], alternative: string };
/**
 * @class Answer
 */
export class Answer {

    // private _index: number;
    // private _question: QuestionFrontend;
    // private _answer: string;
    // private _data: data;
    // private _state: number = 0;
    // private _verifyUrl: string = '/openai/v4';
    //
    // private _textarea: JQuery<HTMLElement>;
    // private _alert: JQuery<HTMLElement>;
    // private _colAnswer: JQuery<HTMLElement>;
    // private _colState: JQuery<HTMLElement>;
    //
    // constructor(question: QuestionFrontend, index: number) {
    //     this.index = index;
    //     this.question = question;
    //
    //     this.init();
    // }
    //
    // public get index(): number {
    //     return this._index;
    // }
    //
    // public set index(value: number) {
    //     this._index = value;
    // }
    //
    // public get question(): QuestionFrontend {
    //     return this._question;
    // }
    //
    // public set question(value: QuestionFrontend) {
    //     this._question = value;
    // }
    //
    // public get frontend(): OaiTekstMetVragenFrontend {
    //     return this._question.parent;
    // }
    //
    // public get answer(): string {
    //     return this._answer;
    // }
    //
    // public set answer(value: string) {
    //     this._answer = value;
    // }
    //
    // public get data(): data {
    //     return this._data;
    // }
    //
    // public set data(value: data) {
    //     this._data = value;
    // }
    //
    // public get correct(): boolean {
    //     return this.data.correct;
    // }
    //
    // public set correct(value: boolean) {
    //     this.data.correct = value;
    // }
    //
    // public get line(): string {
    //
    //     if(this.data.line === undefined) {
    //         this.data.line = '';
    //     }
    //     return this.data.line;
    // }
    //
    // public set line(value: string) {
    //     this.data.line = value;
    // }
    //
    // public get spelling(): boolean {
    //     return this.data.spelling;
    // }
    //
    // public set spelling(value: boolean) {
    //
    //     if(this.data.spelling === undefined) {
    //         this.data.spelling = true;
    //     }
    //
    //     this.data.spelling = value;
    // }
    //
    // public get alternative(): string {
    //
    //     if(this.data.alternative === undefined) {
    //         this.data.alternative = '';
    //     }
    //     return this.data.alternative;
    // }
    //
    // public set alternative(value: string) {
    //     this.data.alternative = value;
    // }
    //
    // public get words(): string[] {
    //
    //     if(this.data.words === undefined) {
    //         this.data.words = [];
    //     }
    //     return this.data.words;
    // }
    //
    // public set words(value: string[]) {
    //
    //     this.data.words = value;
    // }
    //
    // public get state(): number {
    //     return this._state;
    // }
    //
    // public set state(value: number) {
    //     this._state = value;
    // }
    //
    // protected get verifyUrl(): string {
    //     return this._verifyUrl;
    // }
    //
    // protected set verifyUrl(value: string) {
    //     this._verifyUrl = value;
    // }
    //
    // public get textarea(): JQuery<HTMLElement> {
    //     return this._textarea;
    // }
    //
    // public set textarea(value: JQuery<HTMLElement>) {
    //     this._textarea = value;
    // }
    //
    // public get alert(): JQuery<HTMLElement> {
    //     return this._alert;
    // }
    //
    // public set alert(value: JQuery<HTMLElement>) {
    //     this._alert = value;
    // }
    //
    // public get colAnswer(): JQuery<HTMLElement> {
    //     return this._colAnswer;
    // }
    //
    // public set colAnswer(value: JQuery<HTMLElement>) {
    //     this._colAnswer = value;
    // }
    //
    // public get colState(): JQuery<HTMLElement> {
    //     return this._colState;
    // }
    //
    // public set colState(value: JQuery<HTMLElement>) {
    //     this._colState = value;
    // }
    //
    // protected init() {
    //
    //     const s = this;
    //
    //     s.textarea = $('<textarea/>', {
    //         class: 'form-control',
    //         rows: '2'
    //     }).on('keyup', (e: JQuery.KeyUpEvent<HTMLElement>): void => {
    //
    //         if(e.originalEvent.which === 13) {
    //
    //             e.preventDefault();
    //             e.currentTarget.value = e.currentTarget.value.replace(/\n/g, '');
    //
    //             s.answer = s.textarea.val().toString();
    //
    //             s.giveAnswer();
    //         }
    //     }).on('focus', () => {
    //         $('.row-text').find('.alert.alert-primary.text-black').removeClass('alert alert-primary text-black py-1 mb-0')
    //     });
    // }
    //
    // protected giveAnswer() {
    //
    //     const s = this;
    //     s.alert = $('<div/>', {
    //         class: 'alert alert-primary mb-2'
    //     });
    //
    //     let row: JQuery<HTMLElement> = BS.row();
    //     let colIcon = BS.col('col-md-1').append(Fa.solid('fa-fw fa-user'));
    //     s.colAnswer = BS.col('col-md-10').html(s.answer);
    //     s.colState = BS.col('col-md-1 text-end').append(Fa.solid('fa-spinner fa-fw fa-pulse'));
    //     row.append(colIcon, s.colAnswer, s.colState);
    //     s.alert.append(row);
    //
    //     let colParent: JQuery<HTMLElement> = s.textarea.parent();
    //     s.textarea.remove();
    //     colParent.append(s.alert);
    //
    //     s.checkAnswer();
    // }
    //
    // protected setAlert(style: string = 'primary'): void {
    //
    //     const s = this;
    //
    //     s.alert.removeClass('alert-primary alert-danger alert-warning alert-success').addClass(`alert-${style}`);
    //
    //     if(style === 'primary') {
    //         s.colState.empty().append(Fa.solid('fa-triangle-exclamation'));
    //     }
    //     else if(style === 'success') {
    //         s.colState.empty().append(Fa.solid('fa-check'));
    //     }
    //     else {
    //         s.colState.empty().append(Fa.solid('fa-times'));
    //     }
    //
    //     if(style !== 'success') {
    //
    //         s.question.newAnswer();
    //     }
    // }
    //
    // protected getHighLights() {
    //
    //     const s = this;
    //     let content = s.answer;
    //     s.words.forEach((word: string): void => {
    //
    //         content = content.replace(word, "<span class=\"alert alert-danger py-0 px-1 mb-0\">" + word + "</span>");
    //     });
    //
    //     return content;
    // }
    //
    // protected getAlternativeResponse(): JQuery<HTMLElement> {
    //
    //     const s = this;
    //
    //     let col: JQuery<HTMLElement> = BS.col('col-md-12 col-answer');
    //     let alert: JQuery<HTMLElement> = $('<div/>', {
    //         class: 'alert alert-primary mb-2'
    //     });
    //
    //     let row: JQuery<HTMLElement> = BS.row();
    //     let colIcon: JQuery<HTMLElement> = BS.col('col-md-1').append(Fa.regular('fa-fw fa-user'));
    //     let colAnswer: JQuery<HTMLElement> = BS.col('col-md-10').html(s.alternative);
    //     let colState: JQuery<HTMLElement> = BS.col('col-md-1 text-end').append(Fa.solid('fa-fw fa-star'));
    //     row.append(colIcon, colAnswer, colState);
    //     alert.append(row);
    //
    //     col.append(alert);
    //
    //     return col;
    // }
    //
    // /**
    //  * @param response
    //  * @protected
    //  */
    // protected handleResponse(response: any): void {
    //
    //     const s = this;
    //
    //     s.data = response;
    //
    //     if(s.correct && s.words.length === 0) {
    //         s.setAlert('success');
    //
    //         if(s.alternative !== '') {
    //
    //             s.question.row.append(s.getAlternativeResponse());
    //         }
    //
    //     }
    //     else if(s.correct && s.words.length > 0) {
    //         s.setAlert('warning');
    //         s.colAnswer.html(s.getHighLights());
    //     }
    //     else if(!s.correct && s.words.length > 0) {
    //         s.setAlert('danger');
    //         s.colAnswer.html(s.getHighLights());
    //     }
    //     else {
    //         s.setAlert('danger');
    //
    //         if(s.line !== '') {
    //
    //             let index: number = s.frontend.getTextLines().indexOf(s.line);
    //
    //             if(index > -1) {
    //                 s.frontend.lines[index].addClass('alert alert-primary text-black py-1 px-2 mb-0');
    //             }
    //         }
    //     }
    // }
    //
    // /**
    //  * @protected
    //  */
    // protected checkAnswer(): void {
    //
    //     const s = this;
    //
    //     $.ajax({
    //         url: s.verifyUrl,
    //         data: {
    //             text: s.frontend.text,
    //             question: s.question.question,
    //             answer: s.answer
    //         },
    //         method: 'post',
    //         dataType: 'json'
    //     })
    //         .done((response): void => {
    //             console.log(response);
    //
    //             s.handleResponse(response);
    //         })
    //         .fail((jqXHR: JQuery.jqXHR, textStatus: JQuery.Ajax.ErrorTextStatus, errorThrown: string): void => {
    //             console.log(jqXHR)
    //             console.log(textStatus)
    //             console.log(errorThrown)
    //
    //             s.setAlert('primary');
    //         });
    // }
}