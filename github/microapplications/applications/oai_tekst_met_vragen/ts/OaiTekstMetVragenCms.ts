
import {OaiTekstMetVragen} from "./OaiTekstMetVragen";
import {QuestionsCms as Questions} from "./QuestionsCms";
import {BsFa6 as Bs} from "../../../bronnen_TS/bootstrap/BsFa6";
import {FontAwesome as Fa} from "../../../bronnen_TS/FontAwesome";
import { BronnenMediaCms as BronnenMedia } from "../../../bronnen_TS/BronnenMediaCms";
import $ from "jquery";
import Popover from 'bootstrap5/js/src/popover';

export class OaiTekstMetVragenCms extends OaiTekstMetVragen {

    private _cmsQuestions: Questions;
    private _heading: JQuery<HTMLElement>;
    private _rowQuestions: JQuery<HTMLElement>;
    private _bmedia: BronnenMedia;
    private _mainAudio: HTMLAudioElement;
    private _mainImage: HTMLImageElement;

    constructor() {
        super();

        this.init();
    }

    private get heading(): JQuery<HTMLElement> {
        return this._heading;
    }

    private set heading(value: JQuery<HTMLElement>) {
        this._heading = value;
    }

    public get rowQuestions(): JQuery<HTMLElement> {
        return this._rowQuestions;
    }

    public set rowQuestions(value: JQuery<HTMLElement>) {
        this._rowQuestions = value;
    }

    public get bmedia(): BronnenMedia {
        return this._bmedia;
    }

    public set bmedia(value: BronnenMedia) {
        this._bmedia = value;
    }

    public get mainAudio(): HTMLAudioElement {
        return this._mainAudio;
    }

    public set mainAudio(value: HTMLAudioElement) {
        this._mainAudio = value;
    }

    public get mainImage(): HTMLImageElement {
        return this._mainImage;
    }

    public set mainImage(value: HTMLImageElement) {
        this._mainImage = value;
    }

    public get cmsQuestions(): Questions {
        return this._cmsQuestions;
    }

    public set cmsQuestions(value: Questions) {
        this._cmsQuestions = value;
    }

    /**
     *
     * @protected
     */
    protected init(): void {

        const s = this;
        s.bmedia = new BronnenMedia(s.language);

        if(s.getUrlParameter('id') !== 'new') {
            s.getJson((response): void => {

                s.app = response;

                s.start();
            });
        }
        else {

            s.start();
        }
    }

    protected start(): void {

        const s = this;
        document.title = s.getTitle();

        // Wait for bmedia ready
        let interval: NodeJS.Timer = setInterval(() => {

            if(s.bmedia.ready) {

                $('body').append(s.getBaseHtml());
                clearInterval(interval);
            }
        }, 100);
    }

    /**
     *
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected getBaseHtml(): JQuery<HTMLElement> {

        const s = this;
        let container: JQuery<HTMLElement> = Bs.container('container-fluid');
        let card: JQuery<HTMLElement> = Bs.card();
        card.append(s.getCardHeader(), s.getCardBody());
        container.append(card);

        return container;
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    protected getContainer(): JQuery<HTMLElement> {

        const s = this;
        return $('<div/>', {
            class: 'container',
            style: 'margin-bottom: 100px'
        });
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    protected getCard(): JQuery<HTMLElement> {

        return Bs.card('card border border-primary rounded-0');
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    protected getCardHeader(): JQuery<HTMLElement> {

        const s = this;

        s.heading = Bs.div('card-header bg-primary text-white h1 rounded-0').empty().append(s.getTitle());
        return s.heading;
    }

    private getTitle() {

        if(this.title === '') {
            return this.getCurrentApplication().split('_').map((str: string) => {
                return str[0].toUpperCase() + str.substr(1);
            }).join(' ');
        }

        return this.title;
    }

    /**
     *
     * @return {JQuery}
     * @protected
     */
    protected getCardBody(): JQuery<HTMLElement> {

        const s = this;
        let body: JQuery<HTMLElement> = $('<div/>', {
            class: 'card-body'
        }).append(
            s.getTopRow(),
            s.getRowWorkArea()
        );

        return body;
    }

    protected getRowWorkArea(): JQuery<HTMLElement> {

        const s = this;

        let row: JQuery<HTMLElement> = $('<div/>', {
            class: 'row row-workarea'
        });

        let colText: JQuery<HTMLElement> = s.getColText();
        let colQuestions: JQuery<HTMLElement> = s.getColQuestions();

        row.append(colText, colQuestions);

        return row;
    }

    protected getColText(): JQuery<HTMLElement> {

        const s = this;

        let colText: JQuery<HTMLElement> = Bs.col('col-md-7');
        let rowText: JQuery<HTMLElement> = Bs.row();
        let colInnerText: JQuery<HTMLElement> = Bs.col();
        let textarea: JQuery<HTMLElement> = Bs.textarea('form-control', {
            class: 'form-control',
            rows: '20'
        }).on('input', (): void => {

            s.text = textarea.val().toString();
        }).text(s.text);
        colInnerText.append(textarea);
        rowText.append(colInnerText);
        colText.append(rowText);

        return colText;
    }

    protected getColQuestions(): JQuery<HTMLElement> {

        const s = this;

        let colQuestions: JQuery<HTMLElement> = Bs.col('col-md-5');
        s.rowQuestions = Bs.row();

        s.cmsQuestions = new Questions(s, s.questions);
        s.rowQuestions.append(s.cmsQuestions.col);
        colQuestions.append(s.rowQuestions);

        return colQuestions;
    }

    /**
     *
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected getTopRow(): JQuery<HTMLElement> {

        const s = this;
        let row: JQuery<HTMLElement> = Bs.row('row mb-4');

        let titleCol: JQuery<HTMLElement> = s.getTitleCol();
        let languageCol: JQuery<HTMLElement> = s.getLanguageCol();
        let contolsCol: JQuery<HTMLElement> = s.getControlsCol();

        row.append(
            titleCol,
            languageCol,
            contolsCol,
            s.colMainAudio(),
            s.colMainImage()
        );

        return row;
    }

    /**
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected colMainAudio(): JQuery<HTMLElement> {

        const s = this;

        let col: JQuery<HTMLElement> = Bs.col('col-md-4 col-lg-4');
        let group: JQuery<HTMLElement> = Bs.inpGrp();
        let button: JQuery<HTMLElement> = Bs.btn('btn-outline-secondary disabled', {
            disabled: 'disabled'
        }).append(Fa.solid('fa-volume-up'));

        let input: JQuery<HTMLElement> = Bs.inp('form-control', {
            value: s.stripUrl(s.audio)
        }).on('blur', (): void => {

            s.audio = s.bmedia.findAudio(input.val().toString());
            s.btnMainAudio(group, button);
        });

        input = s.bmedia.autocompleteAudio(input);

        group.append(input);

        s.btnMainAudio(group, button);
        col.append(group);

        return col;
    }

    /**
     * @param {JQuery<HTMLElement>} group
     * @param {JQuery<HTMLElement>} button
     * @private
     */
    private btnMainAudio(group: JQuery<HTMLElement>, button: JQuery<HTMLElement>): void {

        const s = this;

        group.append(button);

        if(s.audio !== '') {
            button
                .removeClass('btn-outline-secondary disabled')
                .removeAttr('disabled')
                .addClass('btn-outline-primary')
                .on('click', (): void => {

                    if(s.mainAudio === undefined) {
                        s.mainAudio = new Audio();
                    }

                    s.mainAudio.src = s.bmedia.getAudioLink(s.audio);
                    s.mainAudio.play();
                });
        }
        else {
            button
                .addClass('btn-outline-secondary disabled')
                .attr('disabled', 'disabled')
                .removeClass('btn-outline-primary')
                .off('click', () => {

                });
        }
    }

    /**
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected colMainImage(): JQuery<HTMLElement> {

        const s = this;

        let col: JQuery<HTMLElement> = Bs.col('col-md-4 col-lg-4');
        let group: JQuery<HTMLElement> = Bs.inpGrp();
        let button: JQuery<HTMLElement> = Bs.btn('btn-outline-secondary disabled', {
            disabled: 'disabled'
        }).append(Fa.solid('fa-camera'));

        let input: JQuery<HTMLElement> = Bs.inp('form-control', {
            value: s.stripUrl(s.image)
        }).on('blur', (): void => {

            s.image = s.bmedia.findImage(input.val().toString());
            s.btnMainImage(group, button);
        });

        input = s.bmedia.autocompleteImage(input);

        group.append(input);

        s.btnMainImage(group, button);
        col.append(group);

        return col;
    }

    /**
     * @param {JQuery<HTMLElement>} group
     * @param {JQuery<HTMLElement>} button
     * @private
     */
    private btnMainImage(group: JQuery<HTMLElement>, button: JQuery<HTMLElement>): void {

        const s = this;

        let po = new Popover(button, {
            content: '',
            html: true,
            placement: 'bottom',
            trigger: 'hover'
        });

        group.append(button);

        if(s.image !== '') {
            button
                .removeClass('btn-outline-secondary disabled')
                .removeAttr('disabled')
                .addClass('btn-outline-primary');

            if(s.mainImage === undefined) {
                s.mainImage = new Image();
            }

            s.mainImage.src = s.bmedia.getImageLink(s.image);

            po = new Popover(button, {
                content: s.mainImage,
                html: true,
                placement: 'bottom',
                trigger: 'hover'
            });
        }
        else {

            if(po !== undefined && po !== null) {

                po.dispose();
            }

            button
                .addClass('btn-outline-secondary disabled')
                .attr('disabled', 'disabled')
                .removeClass('btn-outline-primary')
        }
    }

    protected getTitleCol(): JQuery<HTMLElement> {

        const s = this;
        let col: JQuery<HTMLElement> = Bs.col('col-xs-12 col-sm-6 col-md-4 mb-2');
        let group: JQuery<HTMLElement> = Bs.inpGrp();
        let span: JQuery<HTMLElement> = Bs.span('input-group-text').append(Fa.solid('fa-heading'));
        let input: JQuery<HTMLElement> = Bs.inp(
            'form-control',
            {
                value: s.title
            }, {
            event: 'input',
            callback: (e): void => {

                s.title = input.val().toString();
                document.title = s.getTitle();
                s.heading.empty().append(s.getTitle());
            }
            });
        group.append(input, span);
        col.append(group);

        return col;
    }

    /**
     * Dot NOT use this method as is. This should be fetched with an AJAX-call to load the available languages
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected getLanguageCol():JQuery<HTMLElement> {

        const s = this;
        let col: JQuery<HTMLElement> = Bs.col('col-xs-12 col-sm-6 col-md-3 col-lg-2 mb-2');
        let select: JQuery<HTMLElement> = s.bmedia.select();

        col.append(select);

        return col;
    }

    /**
     * Do NOT use this method, it's an example method
     * @return {JQuery<HTMLElement>}
     * @protected
     */
    protected getControlsCol():JQuery<HTMLElement> {

        const s = this;
        let col: JQuery<HTMLElement> = Bs.col('col-xs-12 col-sm-6 col-md-5 col-lg-6 offset-xs-0 offset-sm-6 offset-md-0 text-end mb-2');
        let btnGroup: JQuery<HTMLElement> = $('<div/>', {
            class: 'btn-group',
            value: 'nl_NL'
        });

        /*
         * Complete dummy data of buttons
         */
        let btns = {
            'save': {
                'class': 'btn-primary',
                'title': 'Save',
                'icons': ['fa-save'],
                'function': (): void => {

                    s.questions = s.cmsQuestions.questions;

                    s.sendData(s.app, true);
                }
            },
            'saveExit': {
                'class': 'btn-outline-primary',
                'title': 'Save & Exit',
                'icons': ['fa-save', 'fa-reply'],
                'function': (): void => {

                    s.questions = s.cmsQuestions.questions;

                    s.sendData(s.app, false);
                }
            }
        };
        let k: keyof typeof btns;
        for (k in btns) {

            let ob = btns[k];
            let btn: JQuery<HTMLElement> = $('<button/>', {
                class: 'btn ' + ob['class']
            })
                .on('click', () => {
                    ob['function']()
                })

            ob['icons'].forEach((icon: string): void => {

                btn.append(Fa.solid(icon));
            });

            btnGroup.append(btn);

            // Instantiating Bootstrap.popover
            (<any>$(btn)).popover({
                content: ob['title'],
                trigger: 'hover',
                placement: 'bottom'
            });
        }

        col.append(btnGroup);

        return col;
    }

    /**
     * Just for testing
     * @protected
     */
    protected prepare(): void{

        const s = this;
        s.app = {
            id: s.getUrlParameter('id'),
            exercisetitle: 'Amal - vragen maken (APIv4)',
            applicationname: s.getCurrentApplication(),
            language: 'nl_NL',
            main_object: {
                image: 'foto amal.jpg',
                audio: 'text_Amal.mp3',
                text:
                    'Haar naam is Amal.\n' +
                    'Ze is 29 jaar.\n' +
                    'Amal komt uit Syrië.\n' +
                    'Ze woont nu in Arnhem.\n' +
                    'Ze is getrouwd.\n' +
                    'De naam van haar man is Mohammad.\n' +
                    'Hij werkt in een fabriek.\n' +
                    'Hij werkt elke dag van 07.00 uur tot 16.00 uur.\n' +
                    'Mohammad fietst naar het werk.\n' +
                    'Amal en Mohammad hebben 5 kinderen.\n' +
                    'Twee kinderen gaan naar de voorschool, en drie kinderen gaan naar de basisschool.\n' +
                    'Amal brengt de kleine kinderen om 08.00 uur naar de voorschool.\n' +
                    'Ze haalt ze om 15.00 uur weer op.\n' +
                    'De grote kinderen fietsen naar school.\n' +
                    'Amal is een huisvrouw.\n' +
                    'Ze werkt thuis.\n' +
                    'Ze wast de kleren, maakt het huis schoon, doet de boodschappen en kookt elke dag.',
                questions: [
                    'Is Amal getrouwd?',
                    'Komt Amal uit Syrië?',
                    'Waar woont Amal nu?',
                    'Hoe heet de man van Amal?',
                    'Hebben Amal en Mohammad 6 kinderen?',
                    'Waar werkt Mohammad?',
                    'Fietst Mohammad naar zijn werk?',
                    'Waar werkt Amal?'
                ]
            }
        };
    }
}
