/*
 * Author: Andries van Weeren
 * E-mail: a.vanweeren@fcroc.nl
 * Project: fcsource4
 * Created: 22-09-20 14:57
 * Filename: fe.js
 */

const $ = require('jquery');

import {OaiTekstMetVragenFrontend} from "./ts/OaiTekstMetVragenFrontend";

$((): void => {

    let application: OaiTekstMetVragenFrontend = new OaiTekstMetVragenFrontend();
});
