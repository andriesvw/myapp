# BaseMicroApp information
Describe CMS + Frontend functionality.<br/>
Keep track of changes in the CHANGELOG.md file!


## URL-parameters
- ...
- ...

## CMS key-combinations [2022-06-15]
On word:<br/>
- b + mouseclick => **bold**<br/>
- i + mouseclick => *italic*<br/>
- h + mouseclick => header (h3)
<p>
    Only Youtube + Vimeo video allowed
</p>